#!/bin/bash

# Export the environment variable for the builddir of aliBuild.
# REQUIRED and must be set runner specific.
export ALIBUILD_WORK_DIR="/home/jonas/Programme/CERN/alice/sw"
eval "$(/usr/local/bin/alienv shell-helper)"

# Decide which version of ROOT must be loaded.
if [[ $# -ne 2 ]]; then
    echo "Provide either ROOT5 or ROOT6 as first argument and gcc or clang as second!"
    exit 1
else
    root_ver="$1"
    compiler="$2"
fi

echo "Loading ROOT..."
if [[ "$root_ver" == "ROOT5" ]]; then
    eval $(alienv printenv AliRoot/latest-aliroot5-user)
    /usr/local/bin/alienv list
elif [[ "$root_ver" == "ROOT6" ]]; then
    eval $(alienv printenv AliRoot/latest-alice-root6)
    /usr/local/bin/alienv list
else
    echo "Wrong Version name for ROOT provided. [ROOT5 | ROOT6]"
    exit 1
fi

mkdir -p "build_$compiler" && cd "build_$compiler"

echo
echo "Building in $(pwd)"
echo

if [[ "$compiler" == "clang" ]]; then
  export CC=clang
  export CXX=clang++
fi

cmake .. -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DROOTSYS=$ROOTSYS -DTESTING=ON
make -j4
RETURN_MAKE=$?
echo "Make returned $RETURN_MAKE"

ctest .
RETURN_TEST=$?
echo "CTest returned $RETURN_TEST"

if [[ $RETURN_MAKE -ne 0 ]] || [[ $RETURN_TEST -ne 0 ]]; then
  exit 1
fi
exit 0
