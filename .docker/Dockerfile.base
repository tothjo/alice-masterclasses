#
# A docker image to run ALICE Master Classes
#
# This is based on my image
#
#   gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base
#
# Which in turn is based on CERN CentOS7 base image, with
#
# - Kerbreos client
# - Utilitity to copy to EOS
# - TeXLive
# - Doxygen
# - ROOT
#
# This image needs to communicate to the host X server.
# One should therefor run the image as
#
#  xhost +local:root
#  docker run --interactive --env=DISPLAY \
#   --volume=/tmp/.X11-unix:/tmp/.X11-unix \
#   gitlab-registry.cern.ch/cholm/alice-masterclasses 
#
# The master class GUI needs to be able to use GL.  For graphics
# cards directly supported by Mesa, this should work seemlessly.
#
# For other host graphic cards - notably nVidia - this will _not_ work.
# These graphics cards are really strict about the direct access and
# often requires exact matches between the docker and host binaries.
# Since this image should be agnostic to the host, we do not try
# to facilitate these kinds of hosts and graphics cards. You may want
# to check out nvidia-docker for more on this. 
#
#
FROM gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base
LABEL maintainer="Christian.Holm.Christensen@cern.ch"

# RUN yum install -y xpdf

# Set-up target location
RUN mkdir -p /opt/AliceMasterClass/Base \
    	     /opt/AliceMasterClass/Raa	\
    	     /opt/AliceMasterClass/Strangeness

COPY Base/scripts		/opt/AliceMasterClass/Base/scripts
COPY Base/doc			/opt/AliceMasterClass/Base/doc
COPY Raa/*.C			/opt/AliceMasterClass/Raa/
COPY Raa/scripts		/opt/AliceMasterClass/Raa/scripts
COPY Raa/doc			/opt/AliceMasterClass/Raa/doc
COPY Strangeness/*.C      	/opt/AliceMasterClass/Strangeness/
COPY Strangeness/scripts  	/opt/AliceMasterClass/Strangeness/scripts
COPY Strangeness/doc      	/opt/AliceMasterClass/Strangeness/doc
COPY *.C			/opt/AliceMasterClass/

RUN  source /opt/root/bin/thisroot.sh && \
     cd /opt/AliceMasterClass && \
     /opt/root/bin/root -l -b -q Base/scripts/AliceLoad.C && \
     /opt/root/bin/root -l -b -q Raa/scripts/RaaLoad.C && \
     /opt/root/bin/root -l -b -q Strangeness/scripts/StrangeLoad.C


RUN groupadd exercise && \
    useradd --no-log-init -m -g exercise student  && \
    mkdir -p /home/student/export && \
    chown student:exercise /home/student/export && \
    chmod 775 /home/student/export

VOLUME /home/student/export

USER student

WORKDIR	/home/student

ENTRYPOINT /opt/root/bin/root -l /opt/AliceMasterClass/MasterClass.C


#
# EOF
#

