Changes with respect to previous version
========================================

Part1:
------

- General code clean-up in event display 
- Added increased checks for counting:
  - Checks for double counting
  - Checks if the track is primary, and if not notifies the user 
  - Explicit checks that we do not count (by hand) in Pb-Pb 
- Counter tool is now embedded in the main GUI.  This allows the user
  to full-screen that window. 
- Counter automatically resets on next event
- *Event analysed* button automatically go to the next event
- New main tab to calculate RAA automatically 
- New teacher mode that allows fast counting 
- New run-through mode that will do the fill analysis 
- Instructions are kept in separate HTML files 

- New script for the instructor `CollectRAAs.C` which allows the
  instructor to collect the `RAA` values and visualize the results. 
  
- Removed unused images etc. 

Part2:
------

- Complete rewrite of the per centrality and `RAA` scripts. 
  - The scripts are far simpler and should make it easier for the
    students to modify them in the second part. 
  - Both scripts (`AnalyseTree.C` and `BuildRAA.C`) use the service
    functions in `Utilities.C` to accomplish the simplification. 
  - Instead of popping up multiple canvases for each plot, these
    scripts now show one canvas with multiple pads. 
- New GUI in `masterclass.C` that allows for steering running the two
  analysis scripts `AnalyseTree.C` and `BuildRAA.C`	
- New script `ReadRAA.C` that allows the instruction or student to
  more accurately read off the `RAA` values for selected pT and
  centrality bins.  Note the GUI also allows for exporting the results
  to a *ROOT* file (this file can be directly imported by the script
  `CombineRAAs.C`)
- New moderator script `CombineRAAs.C` which allows for robust
  entering of the `RAA` values for each group.  Specific pT bins are
  specified, but the centrality bins are set on input. 
- The script `MakeGroups.C` creates some dummy files for testing the
  import feature of `CombineRAAs.C`. 
- The script `AnalyseTreeFull.C` runs over all centrality bins in one
  go. 
  
