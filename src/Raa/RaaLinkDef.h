#ifdef __CLING__
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version. (See cxx source for full Copyright notice)
 */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ struct Raa::TGUIEnglish+;
#pragma link C++ struct Raa::TGUIGerman+;

#pragma link C++ class Raa::Dataset+;
#pragma link C++ struct Raa::StudentValue+;
#pragma link C++ struct Raa::ValuesInput+;
#pragma link C++ struct Raa::PtBinGUI+;
#pragma link C++ struct Raa::CentBin+;
#pragma link C++ struct Raa::Input+;
#pragma link C++ struct Raa::CombineArgs+;

#pragma link C++ class Raa::Exercise+;
#pragma link C++ struct Raa::Control+;
#pragma link C++ struct Raa::Editor+;
#pragma link C++ struct Raa::Log+;
#pragma link C++ struct Raa::Guard+;
#pragma link C++ struct Raa::Row+;
#pragma link C++ class Raa::Values+;
#pragma link C++ struct Raa::InstructionsGUI+;

#pragma link C++ class Raa::EventDisplay+;
#pragma link C++ class Raa::TAnalysisWidget+;
#pragma link C++ class Raa::internal::TResultWidget+;
#pragma link C++ class Raa::TTrackCounter+;

#pragma link C++ class Raa::TNavigation+;
#pragma link C++ struct Raa::internal::TDisplay+;
#pragma link C++ struct Raa::internal::THelp+;
#pragma link C++ struct Raa::internal::TTasks+;

#endif
