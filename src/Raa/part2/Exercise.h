/**
 * @file   Exercise.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 12:46:46 2017
 *
 * @brief  2nd part of the RAA class
 *
 * @ingroup alice_masterclass_raa_part2
 */

#ifndef RAAEXERCISE_C
#define RAAEXERCISE_C
#include <RtypesCore.h>
#include <TInterpreter.h>
#include <TROOT.h>
#include <TRootBrowser.h>
#include <TString.h>

#include "Raa/ContentGUITranslation.h"
#include "Control.h"
#include "Editor.h"
#include "Log.h"
#include "Raa/part2/Read.h"
#include "Utility/Exercise.h"
#include "Utility/Utilities.h"

class TRootBrowser;

namespace Raa
{
struct Control;
struct Log;

/**
 * Manager of the nuclear modification factor exercise.
 * @ingroup alice_masterclass_raa_part2
 */
class Exercise : public Utility::Exercise
{
  Editor* fEditor = nullptr;
  Control* fControl = nullptr;
  Log* fLog = nullptr;
  TString fPath;

  const TGUIEnglish& fTranslation;

 public:
  Exercise();

  /**
   * Add our elements to the EVE browser
   *
   * @param browser Browser implementation to use
   * @param cheat   True if cheats are on
   */
  void Setup(TRootBrowser* browser, Bool_t cheat) override;

  /// The original file name
  const char* OrigFileName() const { return fPath.Data(); }

  /// Returns the path to the file containing the initial analysis code.
  TString FileName() const { return Utility::ResourcePath("macros/Raa/Analyse.C"); }

  /**
   * Compile the code
   * @param mode The mode
   */
  void Compile(Int_t mode = -1);

  /**
   * Run the script
   * @param mode The mode
   */
  void Run(Int_t mode = -1);

  /// When user presses the Go button
  void Go();

  /// Launch GUI to read off values
  void Values() { Read(); }

  /**
   * Toggle cheats
   * @param on if true, cheats are on
   */
  void ToggleCheat(Bool_t on) override { fEditor->Load(OrigFileName(), on); }

  const TString& Instructions() const override { return fTranslation.PathLargeStat(); }

  /**
   * We cannot save
   * @return false
   */
  Bool_t CanPrint() const override { return false; }

  /**
   * We cannot save
   * @return false
   */
  Bool_t CanSave() const override { return false; }

  TString DataDir() const override { return Utility::ResourcePath("Raa/data"); }
};
} // namespace Raa
#endif
