/**
 * @file   Raa/scripts/Part2.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 18:49:26 2017
 *
 * @brief  Second part of the master class on RAA
 *
 * @ingroup alice_masterclass_raa_part2
 */

#ifndef PART2_H_WBTPO9VA
#define PART2_H_WBTPO9VA

#include <RtypesCore.h>
#include <TROOT.h>
#include <TString.h>

#include "Utility/AbstractMasterClassContent.h"

namespace Raa
{
class TPeakBackground : public Utility::TAbstractExercise
{
 public:
  TPeakBackground()
    : TAbstractExercise("peak background")
  {
  }

  void RunExercise(Bool_t AllowAuto) override;
};
} // namespace Raa

#endif /* end of include guard: PART2_H_WBTPO9VA */
