#include "Part1.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TEveManager.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <iostream>
#include <memory>
#include <utility>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/EventDisplay.h"
#include "Raa/part1/TNavigation.h"
#include "Utility/EventDisplay.h"
#include "Utility/INavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

class TGPicture;
class TGWindow;

namespace Raa
{
Dataset::Dataset(const TGWindow* p, UInt_t w, UInt_t h, Bool_t allowAuto)
  : TGMainFrame(p, w, h)
  , fStart(nullptr)
  , fDataset(0)
  , fAllowAuto(allowAuto)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2);
  const TGPicture* al = Utility::Logo();
  AddFrame(new TGPictureButton(this, al), lh);

  auto* htmlView = new TGHtml(this, 450, 400);
  TString txt = fTranslation.DataSetInstruction();

  if (!txt.IsNull()) {
    htmlView->ParseText(const_cast<char*>(txt.Data()));
  }
  AddFrame(htmlView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2));

  auto* cb = new TGComboBox(this);
  cb->AddEntry(fTranslation.SelectDataSet(), 0);

  for (int i = 1; i < 11; ++i) {
    cb->AddEntry(Form("%s %d", fTranslation.DataSet().Data(), i), i);
  }

  cb->Resize(100, 20);
  cb->Select(0, kFALSE);
  cb->Connect("Selected(Int_t)", "Raa::Dataset", this, "Choice(Int_t)");
  AddFrame(cb, lh);

  fStart = new TGTextButton(this, fTranslation.StartButton());
  fStart->SetEnabled(false);
  AddFrame(fStart, lh);
  fStart->Connect("Clicked()", "Raa::Dataset", this, "Start()");

  auto* tb = new TGTextButton(this, fTranslation.ExitButton());
  AddFrame(tb, lh);
  tb->Connect("Clicked()", "TApplication", gApplication, "Terminate()");

  SetWindowName("MasterClass");
  MapSubwindows();

  Resize(GetDefaultSize());

  MapWindow();
}

void Dataset::Choice(Int_t id)
{
  std::cerr << "Data set was chosen to " << id << "\n";
  fDataset = id;
  fStart->SetEnabled(fDataset != 0);
}

void Dataset::Code(const TString& data, const TString& geom, Bool_t cheat)
{
  std::cerr << "Creating template code to setup masterclass\n";

  auto* r = new Utility::TEventAnalyseGUI(geom);
  std::unique_ptr<Utility::EventDisplay> Exercise = std_fix::make_unique<EventDisplay>();
  std::unique_ptr<Utility::INavigation> Navigation =
    std_fix::make_unique<TNavigation>(Exercise.get(), cheat);

  r->Setup(std::move(Navigation), std::move(Exercise), fTranslation.Title(), data, cheat);
  r->SetupSignalSlots();

  if (cheat)
    r->Auto();
}

void Dataset::Start()
{
  if (fDataset == 0) {
    new TGMsgBox(gClient->GetRoot(), this, fTranslation.InvalidTitle().Data(),
                 fTranslation.InvalidMessage().Data());
    return;
  }
  Utility::SetupStyleForClass();

  std::cerr << "Load all required scripts\n";

  TString df = Form("AliVSD_Masterclass_%d.root", fDataset);
  TString gf = Utility::ResourcePath("data/Utility/geometry.root");

  std::cerr << "Start program\n";
  Code(df, gf, fAllowAuto);
  UnmapWindow();
}
} // namespace Raa
