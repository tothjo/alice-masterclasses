#include "TTrackCounter.h"

#include <gsl/gsl>
#include "Raa/part1/TrackClassification.h"

namespace
{
Bool_t NotContained(const gsl::span<TEveTrack* const> Range, const TEveTrack* const NewValue)
{
  return std::find(std::begin(Range), std::end(Range), NewValue) == std::end(Range);
}
} // namespace
namespace Raa
{
void TTrackCounter::CountPrimary(TEveTrack* T)
{
  Expects(IsPrimary(T));

  fPrimaryTracks.emplace_back(T);

  if (fHighestPtTrack == nullptr || GetPt(T) > GetPt(fHighestPtTrack))
    fHighestPtTrack = T;

  Ensures(!fPrimaryTracks.empty());
  Ensures(fHighestPtTrack != nullptr);
}

void TTrackCounter::CountPrimaries(const gsl::span<TEveTrack* const> Primaries)
{
  for (auto* const T : Primaries) {
    Expects(NotContained(fPrimaryTracks, T));
    CountPrimary(T);
  }
}

void TTrackCounter::CountPrimaryPositive(TEveTrack* T)
{
  Expects(IsPrimaryPositive(T));
  fPrimaryTracksPositiveCharged.emplace_back(T);
  Ensures(!fPrimaryTracksPositiveCharged.empty());
}

void TTrackCounter::CountPrimariesPositive(const gsl::span<TEveTrack* const> PrimariesPos)
{
  for (auto* const T : PrimariesPos) {
    Expects(NotContained(fPrimaryTracksPositiveCharged, T));
    CountPrimaryPositive(T);
  }
}

void TTrackCounter::CountSecondary(TEveTrack* T)
{
  Expects(IsSecondary(T));
  fSecondaryTracks.emplace_back(T);
  Ensures(!fSecondaryTracks.empty());
}

void TTrackCounter::CountSecondaries(const gsl::span<TEveTrack* const> Secondaries)
{
  for (auto* const T : Secondaries) {
    Expects(NotContained(fSecondaryTracks, T));
    CountSecondary(T);
  }
}

void TTrackCounter::Clear() noexcept
{
  fPrimaryTracks.clear();
  fPrimaryTracksPositiveCharged.clear();
  fSecondaryTracks.clear();
  fHighestPtTrack = nullptr;

  Ensures(fPrimaryTracks.empty());
  Ensures(fPrimaryTracksPositiveCharged.empty());
  Ensures(fSecondaryTracks.empty());
  Ensures(fHighestPtTrack == nullptr);
}

} // namespace Raa
