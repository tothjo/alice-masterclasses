#ifndef TANALYSISWIDGET_H_GOEIFH5X
#define TANALYSISWIDGET_H_GOEIFH5X

#include <TGCanvas.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGWindow.h>
#include <TRootEmbeddedCanvas.h>
#include <TString.h>
#include <utility>

#include "Raa/part1/TRaaStatistics.h"

namespace Raa
{
struct RaaResult {
  Double_t Raa;
  Double_t dRaa;
};

namespace internal
{
/// Small helper to format the text for the Raa display.
TString CreateRaaText(const TString& Prefix, Double_t Raa, Double_t dRaa) noexcept;

class TResultWidget : public TGVerticalFrame
{
 private:
  TGGroupFrame* fRaaPeripheralGroup;
  TGLabel* fRaaPeripheralAuto;
  TGLabel* fRaaPeripheralManual;

  TGGroupFrame* fRaaSemiCentralGroup;
  TGLabel* fRaaSemiCentralAuto;
  TGLabel* fRaaSemiCentralManual;

  TGGroupFrame* fRaaCentralGroup;
  TGLabel* fRaaCentralAuto;
  TGLabel* fRaaCentralManual;

 public:
  TResultWidget(const TGCompositeFrame* Parent);

  void SetRaaPeripheralAuto(Double_t Raa, Double_t dRaa) noexcept;
  void SetRaaPeripheralManual(Double_t Raa, Double_t dRaa) noexcept;

  void SetRaaSemiCentralAuto(Double_t Raa, Double_t dRaa) noexcept;
  void SetRaaSemiCentralManual(Double_t Raa, Double_t dRaa) noexcept;

  void SetRaaCentralAuto(Double_t Raa, Double_t dRaa) noexcept;
  void SetRaaCentralManual(Double_t Raa, Double_t dRaa) noexcept;

  ClassDef(TResultWidget, 0);
};
} // namespace internal

class TAnalysisWidget : public TGMainFrame
{
 private:
  const TRaaStatistics& fAutomaticStats;
  const TPtDistribution& fPeripheralLeadPt;
  const TPtDistribution& fSemiCentralLeadPt;
  const TPtDistribution& fCentralLeadPt;

  /// Vertical Frame to hold 2 rows containing the analysis results.
  TGVerticalFrame* fVerticalFrame;
  /// First Hoziontal row containing Raa factors and peripheral Pt histogram.
  TGHorizontalFrame* fHorizontalRow1;
  /// Second Horizontal row containg semicentral and central Pt histograms.
  TGHorizontalFrame* fHorizontalRow2;

  /// Result pane displays the result of the Raa values.
  internal::TResultWidget* fRaaResults;
  /// Display Pt histograms for peripheral lead lead and PP
  TRootEmbeddedCanvas* fCanvasPeripheral;
  /// Display Pt histograms for semi central lead lead and PP
  TRootEmbeddedCanvas* fCanvasSemiCentral;
  /// Display Pt histograms for central lead lead and PP
  TRootEmbeddedCanvas* fCanvasCentral;

  void PlotLeadvsPP(TRootEmbeddedCanvas* canvas, const TPtDistribution& leadDist);

 public:
  TAnalysisWidget(const TGWindow* parent, const TRaaStatistics& autoPP,
                  const TPtDistribution& peripheralLead, const TPtDistribution& semicentralLead,
                  const TPtDistribution& centralLead);

  /// @{
  /// @name Triggers updates for the Analysis views
  void UpdateAutoRaaPeripheral(const RaaResult& Values);
  void UpdateManualRaaPeripheral(const RaaResult& Values);

  void UpdateAutoRaaSemiCentral(const RaaResult& Values);
  void UpdateManualRaaSemiCentral(const RaaResult& Values);

  void UpdateAutoRaaCentral(const RaaResult& Values);
  void UpdateManualRaaCentral(const RaaResult& Values);

  void UpdatePtPlots();
  /// @}

  ClassDef(TAnalysisWidget, 0);
};
} // namespace Raa

#endif /* end of include guard: TANALYSISWIDGET_H_GOEIFH5X */
