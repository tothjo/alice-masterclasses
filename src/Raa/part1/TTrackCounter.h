#ifndef TTRACKCOUNTER_CXX_2YDR8JV1
#define TTRACKCOUNTER_CXX_2YDR8JV1

#include <TEveTrack.h>
#include <gsl/gsl>
#include <vector>

namespace Raa
{
/// \c TTrackCounter is used as the first step in the analysis of the Raa
/// Masterclass.
///
/// \c TTrackCounter is the basis for both the manual and automatic analysis.
/// It must be called for every Track that is loaded/selected and insert
/// the track into right list of tracks.
/// After the analysis is finished for one event both the automatic analysis
/// and the manual analysis must result in the same number of Tracks for
/// the analysis task. The automatic analysis does have the information for
/// all tasks where as the manual analysis only counts one aspect (e.g. only
/// secondary tracks).
class TTrackCounter
{
 private:
  /// List of counted primary tracks in event
  std::vector<TEveTrack*> fPrimaryTracks;
  /// List of counted primary tracks with positive charge in event
  std::vector<TEveTrack*> fPrimaryTracksPositiveCharged;
  /// List of counted secondary tracks in event
  std::vector<TEveTrack*> fSecondaryTracks;
  /// Single Track that is both primary and has the highest Pt value
  /// of the event.
  TEveTrack* fHighestPtTrack{ nullptr };

 public:
  TTrackCounter() noexcept = default;
  ~TTrackCounter() noexcept = default;

  TTrackCounter(TTrackCounter&&) noexcept = default;
  TTrackCounter& operator=(TTrackCounter&&) noexcept = default;
  TTrackCounter(const TTrackCounter&) = delete;
  TTrackCounter& operator=(const TTrackCounter&) = delete;

  // clang-format off

  /// Return the number of counted primary tracks.
  std::size_t NPrimaries() const noexcept { return fPrimaryTracks.size(); }
  /// Return a view on the primaries
  gsl::span<TEveTrack* const> Primaries() const noexcept { return { fPrimaryTracks }; }
  /// Return the number of counted primary tracks with positive charge.
  std::size_t NPrimariesPositive() const noexcept { return fPrimaryTracksPositiveCharged.size(); }
  /// Return a view on the positive primaries.
  gsl::span<TEveTrack* const> PrimariesPositive() const noexcept { return { fPrimaryTracksPositiveCharged }; }
  /// Return the number of counted secondary tracks.
  std::size_t NSecondaries() const noexcept { return fSecondaryTracks.size(); }
  /// Return a view on the secondaries.
  gsl::span<TEveTrack* const> Secondaries() const noexcept { return { fSecondaryTracks }; }
  /// Return the track with the highest Pt of all registered primaries.
  TEveTrack* PrimaryHighestPt() const noexcept { return fHighestPtTrack; }

  // clang-format on

  /// Count and bookkeep primary track. Assumes correct classification!
  void CountPrimary(TEveTrack* T);
  /// Count the list of primaries using \c CountPrimary, but expects that there
  /// are no duplicates.
  /// NOLINTNEXTLINE (readability-avoid-const-params-in-decls)
  void CountPrimaries(const gsl::span<TEveTrack* const> Primaries);
  /// Count and bookkeep primary, positive charged track. Does not count as
  /// Primary! Assumes correct classification!
  void CountPrimaryPositive(TEveTrack* T);
  /// Count the list of primaries using \c CountPrimaryPositive, but expects
  /// that there are no duplicates.
  /// NOLINTNEXTLINE (readability-avoid-const-params-in-decls)
  void CountPrimariesPositive(const gsl::span<TEveTrack* const> PrimariesPos);
  /// Count and bookkeep secondary track. Assumes correct classification!
  void CountSecondary(TEveTrack* T);
  /// Count and bookkeep secondaries using \c CountSecondary, but expects
  /// that there are no duplicates.
  /// NOLINTNEXTLINE (readability-avoid-const-params-in-decls)
  void CountSecondaries(const gsl::span<TEveTrack* const> Secondaries);

  /// Reset the counter to zero and forget about all tracks that were counted.
  void Clear() noexcept;
};
} // namespace Raa

#endif /* end of include guard: TTRACKCOUNTER_CXX_2YDR8JV1 */
