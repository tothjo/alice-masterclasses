/**
 * @file   Histograms.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 09:35:39 2017
 *
 * @brief  A container of data
 * @ingroup alice_masterclass_raa_part1
 */
#ifndef RAAHISTOGRAMS_C
#define RAAHISTOGRAMS_C

#include <RtypesCore.h>
#include <TAxis.h>
#include <TEveTrack.h>
#include <TH1.h>
#include <gsl/gsl>

namespace Raa
{
struct TGUIEnglish;

/// Helper class to represent the Pt-distribution of an event or dataset.
class TPtDistribution
{
 private:
  const TGUIEnglish& fTranslation;
  TH1D fPtHist;

 public:
  /// Create the Pt Distribution for a system \p NameHistogram to do the
  /// Raa-Calculation. E.g. \p NameHistogram == "LeadLead"
  TPtDistribution(const TString& PostfixHistogram);

  /// Return the underlying data encoding the pt distribution.
  const TH1D& GetHistogram() const noexcept { return fPtHist; }
  /// Add the \p Primaries to the Pt-Distribution histogram.
  /// NOLINTNEXTLINE (readability-avoid-const-params-in-decls)
  void AddEventStatistics(const gsl::span<TEveTrack* const> Primaries);
  /// Reset the distribution histogram to have a clean slate again.
  void ClearStatistics() { fPtHist.Reset(); }
};

/// This class contains the relevant statistics for a full data set to calculate
/// the Raa factor. It is filled after each event is handled (track counting
/// and stuff).
/// Using this data it is possible to do all calculation at the end.
class TRaaStatistics
{
 private:
  // TODO Some histogram Titles and Axis are not translated yet.
  const TGUIEnglish& fTranslation;

  TH1D fMultiplicityHist;
  TH1D fMultHistMinPt;
  TH1D fMultHistSec;
  TH1D fPtHist;

  TH1D fChargeHist;
  TH1D fPhiDist;

 public:
  TRaaStatistics(const TString& PostfixHistogram);

  /// @{
  /// @name Histograms contain Track statistics for whole dataset

  /// Multiplicity of primary tracks.
  const TH1D& HistMultiplicity() const { return fMultiplicityHist; }
  /// Multiplicity of primary tracks with pt > 1GeV
  const TH1D& HistMultiplicityMinPt() const { return fMultHistMinPt; }
  /// Multiplicity of secondary tracks.
  const TH1D& HistMultiplicitySecondaries() const { return fMultHistSec; }
  /// Pt distribution of all primary tracks.
  const TH1D& HistPt() const { return fPtHist; }

  /// Histogram containing the charge distribution.
  const TH1D& HistCharge() const { return fChargeHist; }
  /// Histogram containing the phi-distribution.
  const TH1D& HistPhi() const { return fPhiDist; }

  /// @}

  /// @{
  /// @name Methods to manipulate the statistics

  /// Incorporate the \p Primaries into the statistics and distributions.
  /// NOLINTNEXTLINE (readability-avoid-const-params-in-decls)
  void AddEventStatistics(const gsl::span<TEveTrack* const> Primaries);
  /// Add the number of \p NumberSecondaries to the secondary tracks stats.
  void AddSecondaryMultiplicity(Int_t NumberSecondaries);
  /// Reset all histograms to have a clean slate again.
  void ClearStatistics();
  /// @}

  /// Export the statistical data to a .root file.
  void Export(gsl::not_null<TFile*> f);
};

/// Calculates Raa and returns {Raa, Uncertainty}.
std::pair<Double_t, Double_t> CalcRaa(Double_t MeanPbPb, Double_t MeanPP, Double_t Correction,
                                      Double_t PbPbRMS, Double_t PPRMS);

/// This helper method calculates the Raa between two distributions that
/// are collected within this master class. E.g. 'PbPb Peripheral' with
/// 'PP Automatic'.
/// @returns Pair of <Raa, Uncertainty>
std::pair<Double_t, Double_t> RaaFor(Double_t LeadMultiplicity, const TH1D& ProtonMultiplicity,
                                     Double_t CorrectionFactor) noexcept;

} // namespace Raa
#endif
