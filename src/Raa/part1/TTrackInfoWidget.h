#ifndef TTRACKINFOWIDGET_H_R1CQGYVJ
#define TTRACKINFOWIDGET_H_R1CQGYVJ

#include <TEveVector.h>
#include <TGFrame.h>

class TEveTrack;
class TGWindow;
class TGLabel;
class TGNumberEntryField;

namespace Raa
{
struct TGUIEnglish;
class TTrackInfoWidget : public TGMainFrame
{
 private:
  const TGUIEnglish& fTranslation;

  /// Contain both the headline and the information about the track.
  TGGroupFrame* fGroup;

  /// Organize the headlines showing which number represents what(e.g. charge).
  TGHorizontalFrame* fHeadline;
  TGLabel* fLabelPX;
  TGLabel* fLabelPY;
  TGLabel* fLabelPZ;
  TGLabel* fLabelPT;
  TGLabel* fLabelQ;

  /// Organize the actual Track information (e.gg charge).
  TGHorizontalFrame* fInfoLine;
  TGNumberEntryField* fNumberPX;
  TGNumberEntryField* fNumberPY;
  TGNumberEntryField* fNumberPZ;
  TGNumberEntryField* fNumberPT;
  TGNumberEntryField* fNumberQ;

  /// Set the numberfields to these values.
  void SetCounter(const TEveVectorD& p, Double_t pt, Int_t q);

 public:
  TTrackInfoWidget(const TGWindow* parent);

  /// Update the number fields with the new \p track information.
  void UpdateInformation(const TEveTrack* track);
  /// Reset the number fields to zero.
  void ResetInformation() { SetCounter({ 0., 0., 0. }, 0., 0); }
};
} // namespace Raa

#endif /* end of include guard: TTRACKINFOWIDGET_H_R1CQGYVJ */
