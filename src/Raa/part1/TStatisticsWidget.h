#ifndef TSTATISTICSWIDGET_H_O0JX7NPB
#define TSTATISTICSWIDGET_H_O0JX7NPB

#include <TGFrame.h>
#include <TGWindow.h>
#include <TRootEmbeddedCanvas.h>

#include "Raa/part1/TRaaStatistics.h"

namespace Raa
{
class TStatisticsWidget : public TGMainFrame
{
 private:
  const TRaaStatistics& fAutomaticStats;
  const TRaaStatistics& fManualStats;

  TRootEmbeddedCanvas* fCanvas;

 public:
  TStatisticsWidget(const TGWindow* parent, const TRaaStatistics& autoStats,
                    const TRaaStatistics& manualStats);

  void RedrawAll() noexcept;
};
} // namespace Raa

#endif /* end of include guard: TSTATISTICSWIDGET_H_O0JX7NPB */
