/**
 * @file   /scripts/Part1.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 18:49:26 2017
 *
 * @brief  First part of the master class on RAA
 * @ingroup alice_masterclass_raa_part1
 */

#ifndef PART1_H_R7VSWVUA
#define PART1_H_R7VSWVUA

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TApplication.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGMsgBox.h>
#include <TGTextEntry.h>
#include <TInterpreter.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>

#include "Raa/ContentGUITranslation.h"
#include "Utility/AbstractMasterClassContent.h"

class TBuffer;
class TClass;
class TGTextButton;
class TGWindow;
class TMemberInspector;

namespace Raa
{
struct TGUIEnglish;
} // namespace Raa

namespace Raa
{
/** This defines the interface to choose the data set to use
 * @image html Raa/doc/Part1Selector.png
 * @ingroup alice_masterclass_raa_part1
 */
class Dataset : public TGMainFrame
{
 private:
  TGTextButton* fStart; // Start Button
  Int_t fDataset;       // Number of the choose data set
  Bool_t fAllowAuto;    // allow auto analysis (pass through from first window?!)

  const TGUIEnglish& fTranslation;

 public:
  Dataset(const TGWindow* p, UInt_t w, UInt_t h, Bool_t allowAuto = false);
  ~Dataset() override { Cleanup(); }

  void Choice(Int_t id);
  void Code(const TString& data, const TString& geom, Bool_t cheat);
  void Start();

  ClassDefOverride(Dataset, 0);
};

class TInspectPP : public Utility::TAbstractExercise
{
 public:
  TInspectPP()
    : TAbstractExercise("inspect p-p")
  {
  }

  /// Starts the GUI for selection of the Raa-Dataset which will continue
  /// with the class itself.
  void RunExercise(Bool_t AllowAuto) override { new Dataset(gClient->GetRoot(), 0, 0, AllowAuto); }
};
} // namespace Raa

#endif /* end of include guard: PART1_H_R7VSWVUA */
