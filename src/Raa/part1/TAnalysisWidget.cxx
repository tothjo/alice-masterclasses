#include "TAnalysisWidget.h"
#include <TCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <iostream>

namespace Raa
{
namespace internal
{
TString CreateRaaText(const TString& Prefix, Double_t Raa, Double_t dRaa) noexcept
{
  return TString::Format("%s: %f +- %f", Prefix.Data(), Raa, dRaa);
}

TResultWidget::TResultWidget(const TGCompositeFrame* Parent)
  : TGVerticalFrame(Parent)
  , fRaaPeripheralGroup(new TGGroupFrame(this, "Peripheral"))
  , fRaaPeripheralAuto(new TGLabel(fRaaPeripheralGroup))
  , fRaaPeripheralManual(new TGLabel(fRaaPeripheralGroup))
  , fRaaSemiCentralGroup(new TGGroupFrame(this, "SemiCentral"))
  , fRaaSemiCentralAuto(new TGLabel(fRaaSemiCentralGroup))
  , fRaaSemiCentralManual(new TGLabel(fRaaSemiCentralGroup))
  , fRaaCentralGroup(new TGGroupFrame(this, "Central"))
  , fRaaCentralAuto(new TGLabel(fRaaCentralGroup))
  , fRaaCentralManual(new TGLabel(fRaaCentralGroup))
{
  auto* expandX = new TGLayoutHints(kLHintsExpandX);

  fRaaPeripheralGroup->AddFrame(fRaaPeripheralAuto, expandX);
  fRaaPeripheralGroup->AddFrame(fRaaPeripheralManual, expandX);
  this->AddFrame(fRaaPeripheralGroup, expandX);

  fRaaSemiCentralGroup->AddFrame(fRaaSemiCentralAuto, expandX);
  fRaaSemiCentralGroup->AddFrame(fRaaSemiCentralManual, expandX);
  this->AddFrame(fRaaSemiCentralGroup, expandX);

  fRaaCentralGroup->AddFrame(fRaaCentralAuto, expandX);
  fRaaCentralGroup->AddFrame(fRaaCentralManual, expandX);
  this->AddFrame(fRaaCentralGroup, expandX);

  SetRaaPeripheralAuto(0., 0.);
  SetRaaPeripheralManual(0., 0.);
  SetRaaSemiCentralAuto(0., 0.);
  SetRaaSemiCentralManual(0., 0.);
  SetRaaCentralAuto(0., 0.);
  SetRaaCentralManual(0., 0.);
}

void TResultWidget::SetRaaPeripheralAuto(Double_t Raa, Double_t dRaa) noexcept
{
  fRaaPeripheralAuto->SetText(CreateRaaText("Auto", Raa, dRaa));
}
void TResultWidget::SetRaaPeripheralManual(Double_t Raa, Double_t dRaa) noexcept
{
  fRaaPeripheralManual->SetText(CreateRaaText("Manual", Raa, dRaa));
}
void TResultWidget::SetRaaSemiCentralAuto(Double_t Raa, Double_t dRaa) noexcept
{
  fRaaSemiCentralAuto->SetText(CreateRaaText("Auto", Raa, dRaa));
}
void TResultWidget::SetRaaSemiCentralManual(Double_t Raa, Double_t dRaa) noexcept
{
  fRaaSemiCentralManual->SetText(CreateRaaText("Manual", Raa, dRaa));
}
void TResultWidget::SetRaaCentralAuto(Double_t Raa, Double_t dRaa) noexcept
{
  std::cout << "Cyka blyat\n";
  fRaaCentralAuto->SetText(CreateRaaText("Auto", Raa, dRaa));
}
void TResultWidget::SetRaaCentralManual(Double_t Raa, Double_t dRaa) noexcept
{
  fRaaCentralManual->SetText(CreateRaaText("Manual", Raa, dRaa));
}
} // namespace internal

void TAnalysisWidget::PlotLeadvsPP(TRootEmbeddedCanvas* canvas, const TPtDistribution& leadDist)
{
  TCanvas* c = canvas->GetCanvas();
  TVirtualPad* p = c->cd();
  leadDist.GetHistogram().DrawCopy();
  fAutomaticStats.HistPt().DrawCopy("same");
  p->Modified();
  c->Modified();
  c->Update();
  c->cd();
}

TAnalysisWidget::TAnalysisWidget(const TGWindow* parent, const TRaaStatistics& autoPP,
                                 const TPtDistribution& peripheralLead,
                                 const TPtDistribution& semicentralLead,
                                 const TPtDistribution& centralLead)
  : TGMainFrame(parent)
  , fAutomaticStats(autoPP)
  , fPeripheralLeadPt(peripheralLead)
  , fSemiCentralLeadPt(semicentralLead)
  , fCentralLeadPt(centralLead)
  , fVerticalFrame(new TGVerticalFrame(this))
  , fHorizontalRow1(new TGHorizontalFrame(fVerticalFrame))
  , fHorizontalRow2(new TGHorizontalFrame(fVerticalFrame))
  , fRaaResults(new internal::TResultWidget(fHorizontalRow1))
  , fCanvasPeripheral(new TRootEmbeddedCanvas("Pt Overview Peripheral", fHorizontalRow1))
  , fCanvasSemiCentral(new TRootEmbeddedCanvas("Pt Overview SemiCentral", fHorizontalRow2))
  , fCanvasCentral(new TRootEmbeddedCanvas("Pt Overview Central", fHorizontalRow2))
{
  auto* LHints = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);

  fHorizontalRow1->AddFrame(fRaaResults, LHints);
  fHorizontalRow1->AddFrame(fCanvasPeripheral, LHints);
  fVerticalFrame->AddFrame(fHorizontalRow1, LHints);

  fHorizontalRow2->AddFrame(fCanvasSemiCentral, LHints);
  fHorizontalRow2->AddFrame(fCanvasCentral, LHints);
  fVerticalFrame->AddFrame(fHorizontalRow2, LHints);
  this->AddFrame(fVerticalFrame, LHints);

  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}
void TAnalysisWidget::UpdateAutoRaaPeripheral(const RaaResult& Values)
{
  fRaaResults->SetRaaPeripheralAuto(Values.Raa, Values.dRaa);
}
void TAnalysisWidget::UpdateManualRaaPeripheral(const RaaResult& Values)
{
  fRaaResults->SetRaaPeripheralManual(Values.Raa, Values.dRaa);
}

void TAnalysisWidget::UpdateAutoRaaSemiCentral(const RaaResult& Values)
{
  fRaaResults->SetRaaSemiCentralAuto(Values.Raa, Values.dRaa);
}
void TAnalysisWidget::UpdateManualRaaSemiCentral(const RaaResult& Values)
{
  fRaaResults->SetRaaSemiCentralManual(Values.Raa, Values.dRaa);
}

void TAnalysisWidget::UpdateAutoRaaCentral(const RaaResult& Values)
{
  fRaaResults->SetRaaCentralAuto(Values.Raa, Values.dRaa);
}
void TAnalysisWidget::UpdateManualRaaCentral(const RaaResult& Values)
{
  fRaaResults->SetRaaCentralManual(Values.Raa, Values.dRaa);
}

void TAnalysisWidget::UpdatePtPlots()
{
  PlotLeadvsPP(fCanvasPeripheral, fPeripheralLeadPt);
  PlotLeadvsPP(fCanvasSemiCentral, fSemiCentralLeadPt);
  PlotLeadvsPP(fCanvasCentral, fCentralLeadPt);
}

} // namespace Raa
