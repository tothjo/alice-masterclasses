#include "Combine.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TArrayD.h>
#include <TCanvas.h>
#include <TCanvasImp.h>
#include <TClass.h>
#include <TCollection.h>
#include <TError.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TGTextEntry.h>
#include <TH1.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TList.h>
#include <TMathBase.h>
#include <TNamed.h>
#include <TObjArray.h>
#include <TObject.h>
#include <TRandom.h>
#include <TRootCanvas.h>
#include <TString.h>

class TDirectory;
class TGWindow;

namespace Raa
{
const TGWindow* CombineWindow(TCanvas* c)
{
  const TGWindow* f = gClient->GetRoot();
#ifndef __CINT__
  if (c->GetCanvasImp()->IsA()->InheritsFrom(TRootCanvas::Class())) {
    f = dynamic_cast<TRootCanvas*>(c->GetCanvasImp());
  }
#endif
  return f;
}

PtBinGUI::PtBinGUI(TGCompositeFrame* p, Double_t pt)
  : TGGroupFrame(p, Form("pT=%5.2f", pt))
  , fValue(nullptr)
  , fError(nullptr)
  , fPt(pt)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);
  auto* vf = new TGVerticalFrame(this);

  auto* hf = new TGHorizontalFrame(vf);
  hf->AddFrame(new TGLabel(hf, Form("%s:", fTranslation.Value().Data())), lh);
  hf->AddFrame(fValue = new TGNumberEntryField(hf, 0), lh);
  vf->AddFrame(hf, lh);

  hf = new TGHorizontalFrame(vf);
  hf->AddFrame(new TGLabel(hf, Form("%s:", fTranslation.Error().Data())), lh);
  hf->AddFrame(fError = new TGNumberEntryField(hf, 0), lh);
  vf->AddFrame(hf, lh);

  AddFrame(vf, new TGLayoutHints(kLHintsExpandY));
  p->AddFrame(this, new TGLayoutHints(kLHintsExpandY | kLHintsExpandX, 2, 2, 2, 2));
}

void PtBinGUI::Import(TDirectory* d, Int_t cBin)
{
  TString nme = EncodePtHistName(fPt);
  auto* hst = Get<TH1>(d, nme);
  if (hst == nullptr) {
    return;
  }
  fValue->SetNumber(hst->GetBinContent(cBin));
  fError->SetNumber(hst->GetBinError(cBin));
}

void PtBinGUI::Take(Int_t cbin, TH1* h)
{
  Double_t val = fValue->GetNumber();
  Double_t err = fError->GetNumber();
  h->SetBinContent(cbin, val);
  h->SetBinError(cbin, err);
  h->SetMinimum(TMath::Min(h->GetMinimum(), val - err));
  h->SetMaximum(TMath::Max(h->GetMaximum(), val + err));
}

void PtBinGUI::Reset(Double_t c)
{
  Double_t d = c / 100;
  Double_t v = c < 0 ? 0 : gRandom->Gaus(d, d / 10);
  Double_t e = c < 0 ? 0 : gRandom->Gaus(d / 20, d / 200);
  fValue->SetNumber(v);
  fError->SetNumber(e);
}

CentBin::CentBin(TGCompositeFrame* p, const TArrayD& ptBins)
  : TGGroupFrame(p, "", 0, 0)
  , fCBin(nullptr)
{
  auto* vf = new TGVerticalFrame(this);
  fCBin = new TGComboBox(vf);
  SetupBinComboBox(*fCBin);
  fCBin->Select(1, kFALSE);
  vf->AddFrame(fCBin);

  auto* hf = new TGHorizontalFrame(vf);
  for (Int_t i = 0; i < ptBins.GetSize(); i++) {
    fPtBins.Add(new PtBinGUI(hf, ptBins[i]));
  }
  vf->AddFrame(hf);
  AddFrame(vf);

  p->AddFrame(this, new TGLayoutHints(kLHintsExpandY | kLHintsExpandX, 2, 2, 2, 2));
}

void CentBin::Reset(Bool_t rndm)
{
  // If rndm is true, select random centrality bin
  fCBin->Select(rndm ? gRandom->Integer(9) + 1 : 1, kFALSE);
  Double_t c = -1;
  if (rndm) {
    Int_t cBin = fCBin->GetSelected();
    switch (cBin) {
      case 1:
        c = 2.5;
        break;
      case 2:
        c = 7.5;
        break;
      default:
        c = (cBin - 2) * 10 + 15;
        break;
    }
  }
  TIter next(&fPtBins);
  PtBinGUI* bin = nullptr;
  while ((bin = dynamic_cast<PtBinGUI*>(next())) != nullptr) {
    bin->Reset(c);
  }
}

void CentBin::Import(TDirectory* d)
{
  Int_t cBin = fCBin->GetSelected();
  TIter nextP(&fPtBins);
  PtBinGUI* ptBin = nullptr;
  while ((ptBin = dynamic_cast<PtBinGUI*>(nextP())) != nullptr) {
    ptBin->Import(d, cBin);
  }
}

void CentBin::Take(TList* l)
{
  Int_t cBin = fCBin->GetSelected();
  TIter nextP(&fPtBins);
  TIter nextH1(l);
  PtBinGUI* ptBin = nullptr;
  TH1* h = nullptr;
  while (((ptBin = dynamic_cast<PtBinGUI*>(nextP())) != nullptr) &&
         ((h = dynamic_cast<TH1*>(nextH1())) != nullptr)) {
    ptBin->Take(cBin, h);
  }
}

Input::Input(Int_t nCent, TArrayD& ptBins, TCanvas* c)
  : TGTransientFrame(gClient->GetRoot(), CombineWindow(c), 0, 0)
  , fWho(nullptr)
  , fColor(0)
  , fPtBins(ptBins)
  , fStack(nullptr)
  , fLegend(nullptr)
  , fRet(nullptr)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* vf = new TGVerticalFrame(this);
  auto* gf = new TGGroupFrame(vf, fTranslation.Who().Data());
  auto* hf = new TGHorizontalFrame(gf);
  hf->AddFrame(fWho = new TGTextEntry(hf, ""), new TGLayoutHints(kLHintsExpandX));
  auto* im = new TGTextButton(hf, Form("%s ...", fTranslation.Import().Data()));
  hf->AddFrame(im);
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));
  vf->AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));
  im->Connect("Clicked()", "Raa::Input", this, "Import()");

  for (Int_t i = 0; i < nCent; i++) {
    auto* bin = new CentBin(vf, ptBins);
    fCentBins.Add(bin);
  }
  TGButtonGroup* bg = new TGHButtonGroup(vf);
  auto* ca = new TGTextButton(bg, fTranslation.ButtonCancel().Data());
  auto* ok = new TGTextButton(bg, "OK");
  ca->Connect("Clicked()", "TGTransientFrame", this, "UnmapWindow()");
  ok->Connect("Clicked()", "Raa::Input", this, "Take()");
  vf->AddFrame(bg, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));
  AddFrame(vf, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));
  MapSubwindows();
  Resize(GetDefaultSize());
}

void Input::Popup(Color_t c, TList* stack, TLegend* leg, Bool_t* ret, Bool_t rndm)
{
  fRet = ret;
  *fRet = false;
  fColor = c;
  fStack = stack;
  fLegend = leg;
  TString who("");
  if (rndm) {
    who.Form("%c", Char_t(gRandom->Integer(25) + 'A'));
  }
  fWho->SetText(who.Data());
  TIter next(&fCentBins);
  CentBin* bin = nullptr;
  while ((bin = dynamic_cast<CentBin*>(next())) != nullptr) {
    bin->Reset(rndm);
  }
  MapRaised();
  gClient->WaitForUnmap(this);
}

void Input::Take()
{
  // Figure out which one we got, and form the name
  TString who(fWho->GetText());
  TString nme(who);
  TString rpl(" /-@:;.,");
  for (Int_t i = 0; i < rpl.Length(); i++) {
    nme.ReplaceAll(rpl(i, 1), "_");
  }
  // Make legend entry
  TLegendEntry* e = fLegend->AddEntry("dummy", who, "f");
  e->SetFillColor(fColor);
  e->SetFillStyle(1001);
  // Loop over the define pt bins and make a histogram for each pT
  // bin for this group.
  // TList* l = new TList;
  for (Int_t i = 0; i < fPtBins.GetSize(); i++) {
    fStack->Add(MakeCentHistogram(fColor, fPtBins[i], who));
  }
  TIter nextB(&fCentBins);
  CentBin* bin = nullptr;
  while ((bin = dynamic_cast<CentBin*>(nextB())) != nullptr) {
    bin->Take(fStack);
  }
  *fRet = true;
  UnmapWindow();
}

void Input::Import()
{
  TGFileInfo info;
  info.SetMultipleSelection(false);
  new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDOpen, &info);
  if ((info.fFilename == nullptr) || info.fFilename[0] == '\0') {
    return;
  }

  TString fname(info.fFilename);
  // Printf("Will import from %s", fname.Data());
  TFile* file = TFile::Open(fname, "READ");
  auto* owho = dynamic_cast<TNamed*>(file->Get("who"));
  if (owho != nullptr) {
    fWho->SetText(owho->GetTitle());
  }
  auto* cbins = dynamic_cast<TObjArray*>(file->Get("cbins"));
  TIter nextB(&fCentBins);
  CentBin* bin = nullptr;
  Int_t cnt = 0;
  while ((bin = dynamic_cast<CentBin*>(nextB())) != nullptr) {
    if ((cbins != nullptr) && cbins->GetEntries() > cnt) {
      Int_t ic = cbins->At(cnt)->GetUniqueID();
      // Printf("Selecting centrality bin %d", ic);
      bin->fCBin->Select(ic, false);
    }
    cnt++;
    bin->Import(file);
  }
  gClient->NeedRedraw(this);
  gClient->WaitForUnmap(this);
}

Bool_t CombineMore(TCanvas* c)
{
  Int_t ret = 0;
  TGUIEnglish& Translation = Utility::TranslationFromEnv<ContentLanguage>();
  new TGMsgBox(gClient->GetRoot(), CombineWindow(c), Translation.DiagMoreDataTitle().Data(),
               Translation.DiagMoreDataTextGroup().Data(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  return ret == kMBYes;
}

void Combine(Int_t nCent, Int_t nPt, Double_t* pts, Bool_t test)
{
  TArrayD ptBins(nPt);
  TArrayD ptAxis = PtBins();
  for (Int_t i = 0; i < nPt; i++) {
    Int_t bin = PtBin(pts[i]);
    Double_t pt = (ptAxis[bin - 1] + ptAxis[bin]) / 2;
    ptBins[i] = pt;
  }

  auto* stack = new THStack("stack", "");
  auto* canvas = new TCanvas("c", "c", 800, 800);
  canvas->SetTopMargin(0.01);
  canvas->SetRightMargin(0.03);
  auto* groups = new TLegend(canvas->GetLeftMargin() + 0.01, 1 - canvas->GetTopMargin() - 0.4,
                             canvas->GetLeftMargin() + 0.4, 1 - canvas->GetTopMargin() - 0.01);
  groups->SetBorderSize(0);
  groups->SetFillStyle(0);
  groups->SetNColumns(2);
  auto* bins = new TLegend(1 - canvas->GetRightMargin() - 0.4, canvas->GetBottomMargin() + 0.01,
                           1 - canvas->GetRightMargin() - 0.01, canvas->GetBottomMargin() + 0.2);
  bins->SetBorderSize(0);
  bins->SetFillStyle(0);
  for (Int_t i = 0; i < ptBins.GetSize(); i++) {
    Style_t s = PtStyle(i);
    TString t;
    t.Form("#it{p}_{T}=%5.2f GeV/c", ptBins[i]);
    TLegendEntry* le = bins->AddEntry("dummy", t, "p");
    le->SetMarkerStyle(s);
    le->SetMarkerSize(2);
  }

  auto* input = new Input(nCent, ptBins, canvas);
  Int_t count = 0;
  do {
    Color_t c = ChooseColor(count);
    Bool_t ret = false;
    TList l;
    input->Popup(c, &l, groups, &ret, test);

    if (!ret) {
      continue;
    }

    count++;
    TIter nextHH(&l);
    TH1* hh = nullptr;
    while ((hh = dynamic_cast<TH1*>(nextHH())) != nullptr) {
      stack->Add(hh);
      // stack->GetHists()->Print();
    }
    stack->Modified();

    // canvas->Clear();
    stack->Draw("nostack");
    groups->Draw();
    bins->Draw();
    if (stack == nullptr) {
      Warning("CombineRAAs", "Stack disappeared!");
      break;
    }
    if (stack->GetHistogram() == nullptr) {
      Warning("CombineRAAs", "No histogram, even though we drew");
      break;
    }
    const TGUIEnglish& Translation = Utility::TranslationFromEnv<ContentLanguage>();
    stack->GetHistogram()->SetXTitle(Form("%s (%%)", Translation.Centrality().Data()));
    stack->GetHistogram()->SetYTitle("#it{R}_{AA}(#it{p}_{T},#it{c})");

    canvas->Modified();
    canvas->Update();
    canvas->cd();
  } while (CombineMore(canvas));
}

CombineArgs::CombineArgs()
  : TGTransientFrame(gClient->GetRoot(), nullptr, 0, 0, kVerticalFrame)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  Double_t vals[] = { 2, 1.5, 5.25, 0, 0 };
  for (Int_t i = 0; i < 5; i++) {
    AddInput(i == 0 ? Form("# %s", fTranslation.Centralities().Data()) : Form("pT %d", i), vals[i],
             i == 0);
  }

  auto* hf = new TGHorizontalFrame(this);
  auto* b = new TGTextButton(hf, fTranslation.ButtonCancel().Data());
  hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX));
  b->Connect("Clicked()", "TApplication", gApplication, "Terminate()");

  b = new TGTextButton(hf, "OK");
  hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX));
  b->Connect("Clicked()", "Raa::CombineArgs", this, "Take()");
  AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 1, 2));

  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
  gClient->WaitForUnmap(this);
}

void CombineArgs::AddInput(const char* name, Double_t val, Bool_t isInt)
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 1, 1);
  AddFrame(new TGLabel(this, name), lh);
  TGNumberFormat::EStyle sty = TGNumberFormat::kNESRealFour;
  if (isInt) {
    sty = TGNumberFormat::kNESInteger;
  }
  TGNumberFormat::EAttribute att = TGNumberFormat::kNEAPositive;
  TGNumberFormat::ELimit lim = TGNumberFormat::kNELLimitMinMax;
  auto* nf =
    new TGNumberEntry(this, val, isInt ? 0 : 3, -1, sty, att, lim, isInt ? 1 : 0, isInt ? 9 : 15);
  AddFrame(nf, lh);
  fVals.Add(nf);
}

Double_t CombineArgs::Get(Int_t i)
{
  auto* e = dynamic_cast<TGNumberEntry*>(fVals.At(i));
  if (e == nullptr) {
    return 0;
  }
  if (e->GetNumber() <= 0) {
    return 0;
  }
  return e->GetNumber();
}

void CombineArgs::Take()
{
  fNCent = Get(0);
  fPts.Set(20);
  fPts.Reset(0);
  Double_t v;
  Int_t i = 1;
  for (i = 1; (v = Get(i)) > 0; i++) {
    Printf("Got value %f", v);
    fPts[i - 1] = v;
  }
  Printf("Got %d values", i);
  fPts.Set(i - 1);
  UnmapWindow();
}

void TCombineResults::RunExercise(Bool_t test)
{
  auto* a = new CombineArgs;
  Combine(a->fNCent, a->fPts.GetSize(), a->fPts.GetArray(), test);
}
} // namespace Raa
