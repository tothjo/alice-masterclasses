/**
 * @file   Combine.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar  9 20:31:35 2017
 *
 * @brief Combine results in various pT and centrality bins from the
 * various groups.
 *
 * This is to be run by the Vidyo conference moderator
 * @ingroup alice_masterclass_raa_part2
 */
#ifndef RAACOMBINE_H_D2PUTNFB
#define RAACOMBINE_H_D2PUTNFB

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TArrayD.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TGraph.h>
#include <TH1.h>
#include <TLatex.h>
#include <TLegendEntry.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TRandom.h>
#include <TRootCanvas.h>
#include <TString.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/Utilities.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"

class TBuffer;
class TCanvas;
class TClass;
class TDirectory;
class TGComboBox;
class TGNumberEntryField;
class TGTextEntry;
class TGWindow;
class TH1;
class TLegend;
class TMemberInspector;

namespace Raa
{
struct TGUIEnglish;
/**
 * Get the window descriptor corresponding to  a TCanvas.
 *
 * @param c Canvas
 *
 * @return Window discriptor
 *
 * @ingroup alice_masterclass_raa_part2
 */
const TGWindow* CombineWindow(TCanvas* c);

/**
 * A pT bin value. The bin is fixed, the value is not.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct PtBinGUI : TGGroupFrame {
  TGNumberEntryField* fValue;
  TGNumberEntryField* fError;
  Double_t fPt;

  const TGUIEnglish& fTranslation;

  /**
   * Constructor
   *
   * @param p   Parent container frame
   * @param pt  pT bin value
   */
  PtBinGUI(TGCompositeFrame* p, Double_t pt);

  /**
   * Import values from a file
   *
   * @param d    Directory
   * @param cBin Centrality bin
   */
  void Import(TDirectory* d, Int_t cBin);

  /**
   * Take the input values, make a histogram, and return
   *
   * @param cbin Centrality bin to fill
   * @param h    Histogram to fill into
   */
  void Take(Int_t cbin, TH1* h);

  /**
   * Reset the input
   *
   * @param c If greater than zero, set some bogus value
   */
  void Reset(Double_t c = -1);
  ClassDef(PtBinGUI, 0);
};

/**
 * A set of centrality bin values.  The centrality bin is selected,
 * the pT bins are fixed, but values input
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct CentBin : TGGroupFrame {
  TList fPtBins;
  TGComboBox* fCBin;

  CentBin(TGCompositeFrame* p, const TArrayD& ptBins);
  /**
   * Reset the input
   *
   * @param rndm If true, select random centrality bin and pT values.
   */
  void Reset(Bool_t rndm);

  /**
   * Import values from a file
   *
   * @param d    Directory
   */
  void Import(TDirectory* d);

  /**
   * Fill each pT specific histogram in the stack with values.
   *
   * @param l List of histograms to fill
   */
  void Take(TList* l);
  ClassDef(CentBin, 0);
};

/**
 * A dialog to input the data.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Input : TGTransientFrame {
  TGTextEntry* fWho;
  Color_t fColor;
  TList fCentBins;
  TArrayD fPtBins;
  TList* fStack;
  TLegend* fLegend;
  Bool_t* fRet;

  const TGUIEnglish& fTranslation;

  /**
   * Constructor
   *
   * @param nCent  Number of centrality bins per group
   * @param ptBins The pT bins
   * @param c      Canvas to show messages on top of
   */
  Input(Int_t nCent, TArrayD& ptBins, TCanvas* c);

  /**
   * Show the dialog
   *
   * @param c     Colour for this group
   * @param stack Stack to add to
   * @param leg   Legend to add to
   * @param ret   Pointer to return value
   * @param rndm  If true, reset to random numbers, otherwise zero things
   */
  void Popup(Color_t c, TList* stack, TLegend* leg, Bool_t* ret, Bool_t rndm);
  void Take();
  void Import();

  ClassDef(Input, 0);
};

/**
 * Check with operator if we need more input.
 *
 * @return true if we need more
 *
 * @ingroup alice_masterclass_raa_part2
 */
Bool_t CombineMore(TCanvas* c);

/**
 * Combine RAA measurements.
 *
 * @param nCent Number of centralities
 * @param nPt   Number of pT bins
 * @param pts   Array of pT's
 * @param test  Run in test mode if true
 *
 * @ingroup alice_masterclass_raa_part2
 */
void Combine(Int_t nCent, Int_t nPt, Double_t* pts, Bool_t test = false);

/**
 * Prompt for arguments.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct CombineArgs : TGTransientFrame {
  TList fVals;
  Int_t fNCent{ 0 };
  TArrayD fPts;
  const TGUIEnglish& fTranslation;

  CombineArgs();

  void AddInput(const char* name, Double_t val, Bool_t isInt);
  Double_t Get(Int_t i);
  void Take();

  ClassDef(CombineArgs, 0)
};

class TCombineResults : public Utility::TAbstractExercise
{
 public:
  TCombineResults()
    : TAbstractExercise("combine results")
  {
  }
  void RunExercise(Bool_t test = false) override;
};
} // namespace Raa

#endif /* end of include guard: RAACOMBINE_H_D2PUTNFB */
