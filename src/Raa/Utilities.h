/**
 * @file   Utilities.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar  2 20:20:33 2017
 *
 * @brief  Various utilities used by AnalyseTree.C and BuildRAA.C
 */
#ifndef RAAUTILITIES_C
#define RAAUTILITIES_C
#include <RtypesCore.h>
#include <TArrayD.h>
#include <TCanvas.h>
#include <TClass.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLegend.h>
#include <TMath.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <iostream>

#include "ContentGUITranslation.h"
#include "Utility/Utilities.h"

class TClass;
class TDirectory;
class TFile;
class TGComboBox;
class TH1;
class TH2;
class THStack;
class TObject;
class TVirtualPad;

namespace Raa
{
/// Sets a combobox to contain all bins for the data.
void SetupBinComboBox(TGComboBox& Box);

/// Choose a color based on an integer property (for graphs).
Color_t ChooseColor(int ID);

/**
 * Set the style options used
 */
void StyleSettings();

/**
 * Function to improve style of histograms for plotting
 *
 * Good marker styles are
 *
 * - 20 = filled circle
 * - 21 = filled square
 * - 22 = filled triangle up
 * - 23 = filled triangle down
 * - 24 = open circle
 * - 25 = open square
 * - 26 = open triangle up
 * - 27 = open diamond
 * - 28 = open cross
 * - 29 = filled star
 * - 30 = open star
 *
 * The @a markerSize should be between 0.8 and 2.5
 *
 * Good colours are
 *
 * - kBlack, kGray +1 , kGray+2, kGray+3,
 * - kBlue, kBlue+4,  kBlue-4, kBlue-9
 * - kGreen, kGreen+4, kGreen-2, kGreen-9
 * - kRed, kRed+2, kRed-4, kRed-9
 * - kCyan, kCyan+3, kCyan-3, kCyan -9
 * - kYellow
 * - kOrange, kOrange+10, kOrange-3, kOrange+4
 * - kMagenta, kMagenta+3, kMagenta-7
 * - kViolet, kViolet-3, kViolet+6, kViolet+1
 *
 * @param histo1      The histogram to be modified
 * @param markerStyle The marker style
 * @param markerSize  The marker size
 * @param markerColor The marker colour
 * @param lineColor   The line colour
 */
void SetAttributes(TH1* histo1, Style_t markerStyle, Size_t markerSize, Color_t markerColor,
                   Color_t lineColor);

/**
 * Open our input file
 *
 * @param filename File name
 *
 * @return Pointer to file, or null if not found
 */
TFile* OpenInput(const char* filename = "");

/**
 * Open the output file
 *
 * @param read     If true, open for reading
 * @param filename File name
 *
 * @return Pointer to the file, or null
 */
TFile* OpenOutput(Bool_t read = false, const char* filename = "RaaOutput.root");

/**
 * Open the pp file
 *
 * @param filename File name
 *
 * @return
 */
TFile* OpenPP(const char* filename = "");

/**
 * Open the output file
 *
 * @param read     If true, open for reading
 * @param filename File name
 *
 * @return Pointer to the file, or null
 */
TFile* OpenResult(Bool_t read = false, const char* filename = "RaaResult.root");

/**
 * Get a pointer to an object from a directory.  Optionally, if @a cls
 * is non-null, we check that the object has the expected type.
 *
 * @param dir   Directory to read from
 * @param name  Name of the object
 * @param cls   Option class to check against
 *
 * @return Pointer to the object if found and (optionally) is of the
 * right type.
 */
TObject* GetObject(TDirectory* dir, const char* name, TClass* cls);

template <class CastTarget>
CastTarget* Get(TDirectory* Directory, const char* Name)
{
  return static_cast<CastTarget*>(GetObject(Directory, Name, TTree::Class()));
}

/**
 * Get the event tree
 *
 * @param dir         Directory to read from
 * @param centrality  Pointer to variable to store centrality in
 * @param mult        Pointer to variable to store multiplicity in
 *
 * @return Pointer to the tree, or null if not found
 */
TTree* GetEventTree(TDirectory* dir, Float_t* centrality, Int_t* mult);

/**
 * Get the track tree
 *
 * @param dir         Directory to read from
 * @param centrality  Pointer to variable to store centrality in
 * @param pt          Pointer to variable to store transverse momentum in
 *
 * @return Pointer to the tree, or null if not found
 */
TTree* GetTrackTree(TDirectory* dir, Float_t* centrality, Double_t* pt);

/**
 * Get the maximum number of entries to analyse
 *
 * @param t    Tree
 * @param test If true, restrict to 1000 entries
 *
 * @return The number of entries to analyse
 */
ULong64_t MaxEntries(TTree* t, Bool_t test = false);

/**
 * Format a histogram name
 *
 * @param prefix        Prefix
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Temporary string
 */
TString HistogramName(const char* prefix, Int_t minCentrality, Int_t maxCentrality);

/**
 * Format a histogram name
 *
 * @param prefix        Prefix
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 * @param postfix       Possible postfix
 *
 * @return Temporary string
 */
TString HistogramTitle(const char* prefix, Int_t minCentrality, Int_t maxCentrality,
                       const char* postfix = "");

/**
 * Get the colour corresponding to a centrality range
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return
 */
Color_t CentralityColor(Int_t minCentrality, Int_t maxCentrality);

/**
 * Get an array of pT bins
 *
 * @return Array of pT bins
 */
TArrayD PtBins();

/**
 * Find a bin number corresponding to a pT value
 *
 * @param pt pT value
 *
 * @return bin number
 */
Int_t PtBin(Double_t pt);

/**
 * Deduce the pT style
 *
 * @param i pT bin number
 *
 * @return Marker style, alternating between filled and hollow
 */
Style_t PtStyle(Int_t i);

/**
 * Make a pT distribution histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly allocated histogram
 */
TH1* MakePtHistogram(Int_t minCentrality, Int_t maxCentrality);

/**
 * Get pT histogram from a directory.  If @a minCentrality is smaller
 * than @a maxCentrality, assume we're getting a pp distribution.
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH1* GetPtHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality);

/**
 * Make a multiplicity histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly created histogram
 */
TH1* MakeMultHistogram(Int_t minCentrality, Int_t maxCentrality);

/**
 * Get multiplicity histogram from a directory. If @a minCentrality is
 * smaller than @a maxCentrality, assume we're getting a pp
 * distribution.
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH1* GetMultHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality);

/**
 * Make the multiplicity versus centrality correlation histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly allocated histogram
 */
TH2* MakeMultCentHistogram(Int_t minCentrality, Int_t maxCentrality);

/**
 * Get multiplicity histogram from a directory
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH2* GetMultCentHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality);

/**
 * Replace all special charaters in passed string
 *
 * @param who String to escape
 *
 * @return The escaped string
 */
TString EscapeWho(const char* who);

/**
 * Encode pT specific centrality histogram name
 *
 * @param pt     pT value
 * @param prefix Possible prefix to use
 *
 * @return The name
 */
TString EncodePtHistName(Double_t pt, const char* prefix = "cent");

/**
 * Encode pT specific centrality histogram name
 *
 * @param ptBin  pT bin
 * @return The name
 */
TString EncodePtHistName(Int_t ptBin);

/**
 * Make a centrality histogram
 *
 * @param color Color to use
 * @param pt    pT bin
 * @param who   Who did this
 *
 * @return Pointer to the histogram
 */
TH1* MakeCentHistogram(Color_t color, Double_t pt, const char* who);

/**
 * Read an entry from a tree
 *
 * @param t     Tree to read from
 * @param entry Entry offset
 * @param max   Maximum number of entries to get
 *
 * @return true on success, false otherwise
 */
Bool_t ReadEntry(TTree* t, ULong64_t entry, ULong64_t max);

/**
 * Check if the centrality is within (@a min, @a max)
 *
 * @param centrality Read centrality
 * @param min        Least centrality
 * @param max        Largest centrality
 *
 * @return true if centrality is within the specified range
 */
Bool_t CheckCentrality(Float_t centrality, Int_t min, Int_t max);

/**
 * Scale a histogram by number of binary collisions
 *
 * @param h
 * @param scale
 *
 * @return
 */
TH1* Scale(TH1* h, Double_t scale);

/**
 * Make a copy of @a in, remove the last element (presumably pp or
 * most peripheral event class), and divide all histograms in the copy
 * by the removed element.
 *
 * @param in   Input stack of histograms
 * @param name Name for new stack
 * @param rcp  If true, mark as @f$ R_{\mathrm{CP}}@f$
 *
 * @return Newly allocated stack
 */
THStack* Ratio(THStack* in, const char* name, Bool_t rcp = false);

/**
 * Draw a stack of histograms
 *
 * @param stack  Stack to draw
 * @param p      Pad to draw in
 * @param logy   If true, use logarithmic Y scale
 * @param leg    If true, make a legend
 */
void DrawStack(THStack* stack, TVirtualPad* p, Bool_t logy = false, Int_t leg = 0);

/**
 * Draw a 2D histogram
 *
 * @param h     Histogram
 * @param p     Pad
 * @param logy  If true, use logarithmic Z scale
 * @param tbase Base size for text
 */
void DrawH1(TH1* h, TVirtualPad* p, Bool_t logy, Double_t tbase = .02);

/**
 * Draw a 2D histogram
 *
 * @param h     Histogram
 * @param p     Pad
 * @param logz  If true, use logarithmic Z scale
 * @param tbase Base size for text
 */
void DrawH2(TH2* h, TVirtualPad* p, Bool_t logz, Double_t tbase = .02);

/**
 * Print progress
 *
 * @param cur  Current entry
 * @param freq Frequency of update
 */
void Progress(Int_t cur, Int_t freq);

} // namespace Raa
#endif
