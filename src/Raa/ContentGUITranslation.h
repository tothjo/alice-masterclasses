#ifndef CONTENTGUITRANSLATION_C_YYNAT9XW
#define CONTENTGUITRANSLATION_C_YYNAT9XW

#include <TString.h>
#include <iostream>
#include <memory>
#include <stdexcept>
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep
#include "Utility/Utilities.h"

namespace Raa
{
struct TGUIEnglish : Utility::TLanguageProvider {
  TGUIEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TLanguageProvider(lang)
  {
    std::cerr << "Registering English Raa Content\n";
#include "Raa/keys_gui_trans.txt.en"

    RegisterText("DataSetInstruction",
#include "Raa/Dataset.en.html"
    );
    RegisterText("PathCounter",
#include "Raa/Counter.en.html"
    );
    RegisterText("PathEventDisplay",
#include "Raa/EventDisplay.en.html"
    );
    RegisterText("PathLargeStat",
#include "Raa/LargeStat.en.html"
    );
    RegisterText("PathReadValues",
#include "Raa/ReadValues.en.html"
    );
  }

  LANG_KEY(All)
  LANG_KEY(AnalyseCentralityBin)
  LANG_KEY(Analysis)
  LANG_KEY(CalculateRAA)
  LANG_KEY(Central)
  LANG_KEY(Centrality)
  LANG_KEY(Centralities)
  LANG_KEY(Charge)
  LANG_KEY(ChargeDistribution)
  LANG_KEY(Control)
  LANG_KEY(Counter)
  LANG_KEY(Counts)
  LANG_KEY(DataSet)
  LANG_KEY(DataSetInstruction)
  LANG_KEY(Distribution)
  LANG_KEY(Error)
  LANG_KEY(Editor)
  LANG_KEY(ExitButton)
  LANG_KEY(EventCharacteristics)
  LANG_KEY(Import)
  LANG_KEY(InvalidTitle)
  LANG_KEY(InvalidMessage)
  LANG_KEY(ModeAndCentrality)
  LANG_KEY(Multiplicity)
  LANG_KEY(MultiplicityDist)
  LANG_KEY(Operations)
  LANG_KEY(Peripheral)
  LANG_KEY(ParticleMomentumCharge)
  LANG_KEY(Title)
  LANG_KEY(TitleGroup)
  LANG_KEY(Tracks)
  LANG_KEY(SelectMode)
  LANG_KEY(SelectBin)
  LANG_KEY(SelectpTBin)
  LANG_KEY(SemiCentral)
  LANG_KEY(SelectDataSet)
  LANG_KEY(StartButton)
  LANG_KEY(StatisticsCharge)
  LANG_KEY(StatisticsMult)
  LANG_KEY(StatisticsPT)
  LANG_KEY(StudentGroup)
  LANG_KEY(Value)
  LANG_KEY(Who)

  LANG_KEY(PathCounter)
  LANG_KEY(PathEventDisplay)
  LANG_KEY(PathLargeStat)
  LANG_KEY(PathReadValues)

  LANG_KEY(ButtonAutoCount)
  LANG_KEY(ButtonCalculate)
  LANG_KEY(ButtonCancel)
  LANG_KEY(ButtonClear)
  LANG_KEY(ButtonClose)
  LANG_KEY(ButtonFetch)
  LANG_KEY(ButtonPublish)
  LANG_KEY(ButtonSave)
  LANG_KEY(ButtonStart)
  LANG_KEY(ButtonReadValues)

  LANG_KEY(DiagAlreadyCounted)
  LANG_KEY(DiagMissingInputText)
  LANG_KEY(DiagMissingInputTitle)
  LANG_KEY(DiagMoreDataText)
  LANG_KEY(DiagMoreDataTitle)
  LANG_KEY(DiagMoreDataTextGroup)
  LANG_KEY(DiagNoBinText)
  LANG_KEY(DiagNoBinTitle)
  LANG_KEY(DiagNoGroupNameText)
  LANG_KEY(DiagNoGroupNameTitle)
  LANG_KEY(DiagNoHistogramText)
  LANG_KEY(DiagNoHistogramTitle)
  LANG_KEY(DiagNoModeText)
  LANG_KEY(DiagNoModeTitle)
  LANG_KEY(DiagNotPrimary)
  LANG_KEY(DiagSaveChanges)
  LANG_KEY(DiagSelectBinFirstText)
  LANG_KEY(DiagSelectBinFirstTitle)
  LANG_KEY(DiagErrorSaving)

  LANG_KEY(MenuSave)
  LANG_KEY(MenuCut)
  LANG_KEY(MenuCopy)
  LANG_KEY(MenuPaste)
  LANG_KEY(MenuDelete)
  LANG_KEY(MenuFind)
  LANG_KEY(MenuFindNext)
  LANG_KEY(MenuGotoLine)
  LANG_KEY(MenuCompileMacro)
  LANG_KEY(MenuExecuteMacro)
  LANG_KEY(MenuInterrupt)

  LANG_KEY(ToolBarFile)
  LANG_KEY(ToolBarEdit)
  LANG_KEY(ToolBarSearch)
  LANG_KEY(ToolBarTools)

  LANG_KEY(ToolBarDataSave)
  LANG_KEY(ToolBarDataCut)
  LANG_KEY(ToolBarDataCopy)
  LANG_KEY(ToolBarDataPaste)
  LANG_KEY(ToolBarDataDelete)
  LANG_KEY(ToolBarDataFind)
  LANG_KEY(ToolBarDataFindNext)
  LANG_KEY(ToolBarDataGoto)
  LANG_KEY(ToolBarDataCompile)
  LANG_KEY(ToolBarDataExecute)
  LANG_KEY(ToolBarDataInterrupt)
};

LANG_TRANSLATION(TGUI, German)
{
  std::cerr << "Registering German Raa Content\n";
#include "Raa/keys_gui_trans.txt.de"

  RegisterText("DataSetInstruction",
#include "Raa/Dataset.de.html"
  );
  RegisterText("PathCounter",
#include "Raa/Counter.de.html"
  );
  RegisterText("PathEventDisplay",
#include "Raa/EventDisplay.de.html"
  );
  RegisterText("PathLargeStat",
#include "Raa/LargeStat.de.html"
  );
  RegisterText("PathReadValues",
#include "Raa/ReadValues.de.html"
  );
}

struct ContentLanguage {
  using ReturnType = TGUIEnglish;

  static std::unique_ptr<TGUIEnglish> create(Utility::ESupportedLanguages lang)
  {
    switch (lang) {
      case Utility::English:
        return std_fix::make_unique<TGUIEnglish>();
      case Utility::German:
        return std_fix::make_unique<TGUIGerman>();
      case Utility::NLanguages:
        break;
    }
    throw std::runtime_error("Unsupported language requested for Raa Content!");
  }
};
} // namespace Raa

#endif /* end of include guard: CONTENTGUITRANSLATION_C_YYNAT9XW */
