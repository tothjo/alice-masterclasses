#ifndef TMULTIVIEW_H_ZOFUKCR8
#define TMULTIVIEW_H_ZOFUKCR8

#include <RtypesCore.h>
#include <TEveProjectionManager.h>
#include <TEveScene.h>

class TEveElement;
class TEveGeoShape;
class TEveViewer;

namespace Jpsi
{
struct TGUIEnglish;

struct TMultiView {
  //   TEveProjectionManager *f3DMgr;
  TEveProjectionManager* fRPhiMgr;
  TEveProjectionManager* fRhoZMgr;

  TEveViewer* f3DView;
  TEveViewer* fRPhiView;
  TEveViewer* fRhoZView;

  //   TEveScene             *f3DGeomScene;

  TEveScene* fRPhiGeomScene;
  TEveScene* fRhoZGeomScene;
  TEveScene* fRPhiEventScene;
  TEveScene* fRhoZEventScene;

  TEveGeoShape* fGeomGentle;     // Obvious meaning.
  TEveGeoShape* fGeomGentleRPhi; // Obvious meaning.
  TEveGeoShape* fGeomGentleRhoZ; // Obvious meaning.

  const TGUIEnglish& fTranslation;

  TMultiView();
  void SetDepth(Float_t d);

  void InitGeomGentle(TEveGeoShape* g3d, TEveGeoShape* grphi, TEveGeoShape* grhoz);

  void ImportGeomRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiGeomScene); }

  void ImportGeomRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZGeomScene); }

  void ImportEventRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiEventScene); }

  void ImportEventRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZEventScene); }

  void DestroyEventRPhi() { fRPhiEventScene->DestroyElements(); }

  void DestroyEventRhoZ() { fRhoZEventScene->DestroyElements(); }

  void DestroyGeomRPhi() { fRPhiGeomScene->DestroyElements(); }

  void DestroyGeomRhoZ() { fRhoZGeomScene->DestroyElements(); }

  void DestroyAllGeometries();
  void SetRPhiAxes();
  void SetRhoZAxes();

  TEveViewer* Get3DView() { return f3DView; }
  TEveViewer* GetRPhiView() { return fRPhiView; }
  TEveScene* GetRPhiEventScene() { return fRPhiEventScene; }
  TEveScene* GetRhoZEventScene() { return fRhoZEventScene; }
};

} // namespace Jpsi

#endif /* end of include guard: TMULTIVIEW_H_ZOFUKCR8 */
