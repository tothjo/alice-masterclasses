#ifndef FULLCLASS_H_ZQNNWRO8
#define FULLCLASS_H_ZQNNWRO8

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>

#include "Utility/AbstractMasterClassContent.h"

class TBuffer;
class TClass;
class TGComboBox;
class TGTextButton;
class TMemberInspector;

namespace Jpsi
{
void alice_vsd(Int_t dataset);

class MasterClassFrame : public TGMainFrame
{
 private:
  TGTextButton* fExample;
  TGTextButton* fStudent;
  TGTextButton* fTeacher;
  TGTextButton* fExit;
  TGComboBox* fDataset;

 public:
  MasterClassFrame();
  ~MasterClassFrame() override { Cleanup(); }
  void Start();
  ClassDefOverride(MasterClassFrame, 0);
};

class TJpsiClass : public Utility::TAbstractExercise
{
 public:
  TJpsiClass()
    : TAbstractExercise("Jpsi Analysis")
  {
  }

  void RunExercise(Bool_t test = false) override;
};
} // namespace Jpsi

#endif /* end of include guard: FULLCLASS_H_ZQNNWRO8 */
