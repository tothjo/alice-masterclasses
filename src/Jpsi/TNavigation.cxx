#include "Jpsi/TNavigation.h"

#include <GuiTypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>
#include <TSystem.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Utility
{
class EventDisplay;
} // namespace Utility

namespace Jpsi
{
namespace internal
{
TInstructions::TInstructions(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.EventNavigation())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))
  , fInstructionsButton(new TGTextButton(fMotherFrame, Translation.ButtonInstructions()))
{
  const TGFont* font = gClient->GetFont("-*-times-bold-r-*-*-16-*-*-*-*-*-*-*");
  FontStruct_t buttonFont = font->GetFontStruct();

  ULong_t buttonRedColor;
  gClient->GetColorByName("red", buttonRedColor);

  fInstructionsButton->SetTextColor(buttonRedColor);
  fInstructionsButton->SetFont(buttonFont);
  fMotherFrame->AddFrame(fInstructionsButton, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
}

TEventNavigation::TEventNavigation(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.EventNavigation())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))

  , fLabelFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fLabelPrevious(new TGLabel(fLabelFrame, Translation.ButtonPrevious()))
  , fLabelCurrent(new TGLabel(fLabelFrame, Translation.ButtonCurrent()))
  , fLabelNext(new TGLabel(fLabelFrame, Translation.ButtonNext()))

  , fButtonFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonPrevious(new TGPictureButton(fButtonFrame, gClient->GetPicture(Utility::ResourcePath("root/icons/GoBack.gif"))))
  , fLabelEventNumber(new TGLabel(fButtonFrame))
  , fButtonNext(new TGPictureButton(fButtonFrame, gClient->GetPicture(Utility::ResourcePath("root/icons/GoForward.gif"))))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);
  auto* HintExpandXY = new TGLayoutHints(kLHintsExpandX | kLHintsCenterY);

  fLabelFrame->AddFrame(fLabelPrevious, HintExpandX);
  fLabelFrame->AddFrame(fLabelCurrent, HintExpandX);
  fLabelFrame->AddFrame(fLabelNext, HintExpandX);
  fMotherFrame->AddFrame(fLabelFrame, HintExpandXY);

  fButtonFrame->AddFrame(fButtonPrevious, HintExpandX);
  fLabelEventNumber->SetText("1");
  fButtonFrame->AddFrame(fLabelEventNumber, HintExpandX);
  fButtonFrame->AddFrame(fButtonNext, HintExpandX);
  fMotherFrame->AddFrame(fButtonFrame, HintExpandXY);

  this->AddFrame(fMotherFrame, HintExpandXY);
}

TAnalysis::TAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.AnalysisTools())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))

  , fLoadTracksLine(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonLoadTracks(new TGTextButton(fLoadTracksLine, Translation.ButtonLoadTracks()))
  , fCheckboxLoadTracks(new TGCheckButton(fLoadTracksLine, "", 10))

  , fButtonFillPidHistos(new TGTextButton(fMotherFrame, Translation.ButtonFillPIDHistogram()))
  , fButtonDefineTrackCuts(new TGTextButton(fMotherFrame, Translation.ButtonDefineCuts()))

  , fApplyTrackCuts(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonApplyTrackCuts(new TGTextButton(fApplyTrackCuts, Translation.ButtonApplyCuts()))
  , fCheckboxApplyTrackCuts(new TGCheckButton(fApplyTrackCuts, "", 10))

  , fButtonFillInvMass(
      new TGTextButton(fMotherFrame, Translation.ButtonFillInvariantMassHistogram()))
  , fButtonExtractSignal(new TGTextButton(fMotherFrame, Translation.ButtonExtractSignal()))
{
  auto* KExpandX = new TGLayoutHints(kLHintsExpandX);
  auto* KHintX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);
  auto* KHintX2 = new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1);

  fLoadTracksLine->AddFrame(fButtonLoadTracks, KExpandX);
  fCheckboxLoadTracks->SetEnabled(kFALSE);
  fLoadTracksLine->AddFrame(fCheckboxLoadTracks);
  fMotherFrame->AddFrame(fLoadTracksLine, KHintX);

  fMotherFrame->AddFrame(fButtonFillPidHistos, KHintX2);
  fMotherFrame->AddFrame(fButtonDefineTrackCuts, KHintX);

  fButtonApplyTrackCuts->SetEnabled(kFALSE);
  fApplyTrackCuts->AddFrame(fButtonApplyTrackCuts, KExpandX);
  fCheckboxApplyTrackCuts->SetEnabled(kFALSE);
  fApplyTrackCuts->AddFrame(fCheckboxApplyTrackCuts);
  fMotherFrame->AddFrame(fApplyTrackCuts, KHintX);

  fMotherFrame->AddFrame(fButtonFillInvMass, KHintX2);
  fMotherFrame->AddFrame(fButtonExtractSignal, KHintX);
  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}

TSteering::TSteering(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.Steering())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))
  , fButtonShowDetector(new TGTextButton(fMotherFrame, Translation.ButtonShowDetector()))
  , fButtonToggleBackground(new TGTextButton(fMotherFrame, Translation.ButtonBackgroundColor()))
  , fButtonClearHistogram(new TGTextButton(fMotherFrame, Translation.ButtonClearHistograms()))
  , fButtonRestart(new TGTextButton(fMotherFrame, Translation.ButtonRestart()))
{
  auto* KExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);
  fMotherFrame->AddFrame(fButtonShowDetector, KExpandX);

  fMotherFrame->AddFrame(fButtonToggleBackground, KExpandX);

  fMotherFrame->AddFrame(fButtonClearHistogram, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  fMotherFrame->AddFrame(fButtonRestart, KExpandX);

  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}

TQuickAnalysis::TQuickAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.QuickAnalysis())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))

  , fQuickAnalysisFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonQuickAnalysis(new TGTextButton(fQuickAnalysisFrame, Translation.QuickAnalysis()))
  , fCheckBoxQuickAnalysis(new TGCheckButton(fQuickAnalysisFrame, "", 10))

  , fEventFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fLoopNumberLabel(new TGLabel(fEventFrame, " N      = "))
  , fNLoop(new TGNumberEntryField(fEventFrame))

  , fJumpFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonJumpEvents(new TGTextButton(fJumpFrame, Translation.ButtonJumpNEvents()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);

  fCheckBoxQuickAnalysis->SetEnabled(kFALSE);
  fQuickAnalysisFrame->AddFrame(fButtonQuickAnalysis, HintExpandX);
  fQuickAnalysisFrame->AddFrame(fCheckBoxQuickAnalysis);
  fMotherFrame->AddFrame(fQuickAnalysisFrame, HintExpandX);

  fEventFrame->AddFrame(fLoopNumberLabel, HintExpandX);
  fEventFrame->AddFrame(fNLoop, HintExpandX);
  fMotherFrame->AddFrame(fEventFrame, HintExpandX);

  fButtonJumpEvents->SetEnabled(kFALSE);
  fJumpFrame->AddFrame(fButtonJumpEvents, HintExpandX);
  fMotherFrame->AddFrame(fJumpFrame, HintExpandX);

  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}
} // namespace internal

TNavigation::TNavigation(TGCompositeFrame* Parent, Utility::EventDisplay*)
  // : INavigation(Exercise, false)
  : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
  , fInstructionsJpsiFrame(new internal::TInstructions(Parent, fTranslation))
  , fEventNavigationJpsiFrame(new internal::TEventNavigation(Parent, fTranslation))
  , fAnalysisJpsiFrame(new internal::TAnalysis(Parent, fTranslation))
  , fSteeringJpsiFrame(new internal::TSteering(Parent, fTranslation))
  , fQuickAnalysisJpsiFrame(new internal::TQuickAnalysis(Parent, fTranslation))
{
}

void TNavigation::SetupSignalSlotsTMP(TVSDReader* Reader)
{
  fInstructionsJpsiFrame->fInstructionsButton->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                       "Instructions()");

  fEventNavigationJpsiFrame->fButtonPrevious->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                      "PrevEvent()");
  fEventNavigationJpsiFrame->fButtonNext->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                  "NextEvent()");

  fAnalysisJpsiFrame->fButtonLoadTracks->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                 "LoadTracks()");
  fAnalysisJpsiFrame->fButtonFillPidHistos->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                                    "FillEnergyLossHisto()");
  fAnalysisJpsiFrame->fButtonDefineTrackCuts->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                                      "Selector()");
  fAnalysisJpsiFrame->fButtonApplyTrackCuts->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                     "SwapApplyTrackCuts()");
  fAnalysisJpsiFrame->fButtonFillInvMass->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                                  "FillInvariantMassHistos()");
  fAnalysisJpsiFrame->fButtonExtractSignal->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                                    "Extractor()");

  fSteeringJpsiFrame->fButtonShowDetector->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                   "ChangeGeometry()");
  fSteeringJpsiFrame->fButtonToggleBackground->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                       "ChangeBackgroundColor()");
  fSteeringJpsiFrame->fButtonClearHistogram->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                                     "ClearHisto()");
  fSteeringJpsiFrame->fButtonRestart->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                              "ClearHisto()");
  fSteeringJpsiFrame->fButtonRestart->Connect("Pressed()", "Jpsi::TVSDReader", Reader,
                                              "BeginAgain()");

  fQuickAnalysisJpsiFrame->fButtonQuickAnalysis->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                         "SwapQuickAnalysis()");
  fQuickAnalysisJpsiFrame->fButtonJumpEvents->Connect("Clicked()", "Jpsi::TVSDReader", Reader,
                                                      "EventsJump()");
}

void TNavigation::SetEventNumber(Int_t Num, Int_t Max)
{
  fEventNavigationJpsiFrame->fLabelEventNumber->SetText(TString::Format("%i / %i", Num, Max));
}
} // namespace Jpsi
