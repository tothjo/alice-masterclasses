#ifndef TVSDREADER_H_BAKZYE3A
#define TVSDREADER_H_BAKZYE3A

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TEveManager.h>
#include <TEveViewer.h>
#include <TGButton.h>

#include "Jpsi/TDetectorInfo.h"
#include "Jpsi/TExtractor.h"
#include "Jpsi/TNavigation.h"
#include "Jpsi/TSelector.h"

class TBuffer;
class TCanvas;
class TClass;
class TDirectory;
class TEveTrackList;
class TEveVSD;
class TFile;
class TGLabel;
class TGMainFrame;
class TGNumberEntryField;
class TH1F;
class TH2F;
class TLine;
class TMemberInspector;
class TObjArray;

namespace Jpsi
{
struct TMultiView;
} // namespace Jpsi

namespace Jpsi
{
struct TGUIEnglish;

class TVSDReader
{
 private:
  const TGUIEnglish& fTranslation;

 public:
  // File / Event Data
  TFile* fFile;
  TDirectory* fDirectory;
  TObjArray* fEvDirKeys;
  TEveVSD* fVSD;
  Int_t fMaxEv, fCurEv, fMaxR;
  Bool_t fLoadTracks, fGeometrySet, fApplyTrackCuts;

  // Main GUI
  TGMainFrame* fMainFrame;
  TNavigation fNavigation;

  // Reference to the Visualisation, signal slots, and managing destroying the geometry
  TMultiView* gMultiView;
  Bool_t fEnableQuickAnalysis;

  // Histograms that are used in the analysis
  TH1F* gMinvHist;
  TH1F* geeHist;
  TH1F* gppHist;
  TH1F* geHist;
  TH1F* gpHist;
  TH1F* gJpsiHist;
  TH2F* gEnergyLoss;

  Float_t gArrP[300];
  Float_t gArrdEdx[300];
  Float_t el[100][4];
  Float_t po[100][4];
  Int_t nPositrons;
  Int_t nElectrons;
  Int_t nSelectedTracks;
  Int_t nEvents;
  // Here are the Histograms plotted?!
  TCanvas* pad2;
  TCanvas* pad;

  // GUI again
  TDetectorInfo* gAliceDetectorInfo;
  TSelector* gAliceSelector;
  TExtractor* gAliceExtractor;

  // Analysis data
  Int_t numJpsi;
  Int_t bg;
  Float_t significance;
  Float_t sb;
  Float_t maxm, minm; // Comes from TExtractor

  // Lines that present the data
  TLine* l1;
  TLine* l2;
  TLine* l3;
  TLine* l4;
  TLine* l5;
  TLine* l6;

  /// State for the analysis
  Float_t p1, p2, de1, de2;

  // Event visualization structures
  TEveTrackList* fTrackList;

 public:
  TVSDReader(const char* file_name);
  virtual ~TVSDReader();

  void Exit();
  void Selector() { gAliceSelector = new TSelector(this, p1, p2, de1, de2); }
  void SetPIDCutValues();
  void PlotPIDLines();
  void SetMassLineValues();
  void PlotMassLines();
  void CalculateIntegral();
  void SetExtractorFieldValues();
  void Autosave();
  void Extractor() { gAliceExtractor = new TExtractor(this, minm, maxm); }
  void DetectorInfo() { gAliceDetectorInfo = new TDetectorInfo(); }
  void Instructions();
  void StudentSet();
  void DestroyGeometry();
  void GeometryDefault();
  void ChangeGeometry();
  void ChangeBackgroundColor() { gEve->GetViewers()->SwitchColorSet(); }
  void AttachEvent();
  void DropEvent();
  void BeginAgain();
  void NextEvent();
  void PrevEvent();
  Bool_t GotoEvent(Int_t ev);
  void SwapQuickAnalysis();
  void EventsJump();
  void LoadTracks();
  void EnableTrackCutsButton();
  void SwapApplyTrackCuts();
  void ApplyTrackCuts();
  void LoadTracksFromTree(Int_t maxR);
  void FillInvariantMassHistos();
  void ResetMatrices();
  void ResetDedxArray();
  void ClearHisto();
  void FillEnergyLossHisto();

  ClassDef(TVSDReader, 0);
};

} // namespace Jpsi

#endif /* end of include guard: TVSDREADER_H_BAKZYE3A */
