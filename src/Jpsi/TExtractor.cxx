#include "Jpsi/TExtractor.h"

#include <GuiTypes.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>
#include <TGWidget.h>
#include <TRootHelpDialog.h>
#include <TString.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Jpsi
{
// Signal Extraction Window
//______________________________________________________________________________
//______________________________________________________________________________

TExtractor::TExtractor(TVSDReader* parent, Float_t minm, Float_t maxm)
  : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
  , l5(nullptr)
  , l6(nullptr)
{
  auto* gf = new TGGroupFrame(this, "");

  const TGFont* font = gClient->GetFont("-*-times-bold-r-*-*-16-*-*-*-*-*-*-*");
  FontStruct_t buttonFont = font->GetFontStruct();
  ULong_t buttonRedColor;
  gClient->GetColorByName("red", buttonRedColor);

  auto* b = new TGTextButton(gf, fTranslation.ButtonInstructions());
  b->SetTextColor(buttonRedColor);
  b->SetFont(buttonFont);
  b->Connect("Clicked()", "Jpsi::TExtractor", this, "Instructions()");

  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));

  auto* hf = new TGHorizontalFrame(gf, 120, 20, kFixedWidth);

  val1 = new TGLabel(hf, "    ");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "min");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "max");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  hf = new TGHorizontalFrame(gf, 240, 20, kFixedWidth);

  val1 = new TGLabel(hf, fTranslation.MassRange());
  fm1 = new TGNumberEntryField(hf);
  fm1->SetNumber(minm);
  fm2 = new TGNumberEntryField(hf);
  fm2->SetNumber(maxm);

  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fm1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fm2, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  b = new TGTextButton(gf, fTranslation.ButtonExtractSignal());
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "CalculateIntegral()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "SetMassLineValues()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "PlotMassLines()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "SetExtractorFieldValues()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "Autosave()");
  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  hf = new TGHorizontalFrame(gf, 240, 20, kFixedWidth);
  val1 = new TGLabel(hf, fTranslation.NumberJpsis());
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  JpsiCount = new TGNumberEntryField(hf);
  JpsiCount->SetEnabled(kFALSE);
  hf->AddFrame(JpsiCount, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  hf = new TGHorizontalFrame(gf, 240, 20, kFixedWidth);
  val1 = new TGLabel(hf, fTranslation.SignalBackground());
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  SonB = new TGNumberEntryField(hf);
  SonB->SetEnabled(kFALSE);
  hf->AddFrame(SonB, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  hf = new TGHorizontalFrame(gf, 240, 20, kFixedWidth);
  val1 = new TGLabel(hf, fTranslation.Significance());
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  Sigma = new TGNumberEntryField(hf);
  Sigma->SetEnabled(kFALSE);
  hf->AddFrame(Sigma, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  b = new TGTextButton(gf, fTranslation.ButtonClose());
  b->Connect("Pressed()", "Jpsi::TExtractor", this, "UnmapWindow()");
  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
  AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  SetWindowName(fTranslation.SignalExtraction());

  // Set a name to the main frame
  MapSubwindows();

  // Initialize the layout algorithm via Resize()
  Resize(GetDefaultSize());
  // Map main frame
  MapWindow();
}

void TExtractor::Instructions()
{
  auto* instructions =
    new TRootHelpDialog(gClient->GetRoot(), fTranslation.SelectorInstructionsTitle(), 700, 400);

  instructions->SetText(fTranslation.SignalExtractionInstructions());

  instructions->Popup();
}

} // namespace Jpsi
