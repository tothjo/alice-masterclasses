#include "Jpsi/TVSDReader.h"

#include <GuiTypes.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TDirectory.h>
#include <TEveBrowser.h>
#include <TEveEventManager.h>
#include <TEveGeoShape.h>
#include <TEveManager.h>
#include <TEveScene.h>
#include <TEveTrack.h>
#include <TEveTrackPropagator.h>
#include <TEveVSD.h>
#include <TEveVector.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>
#include <TGTab.h>
#include <TGTextView.h>
#include <TH1.h>
#include <TH2.h>
#include <TKey.h>
#include <TLine.h>
#include <TList.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TObject.h>
#include <TPRegexp.h>
#include <TRootBrowser.h>
#include <TRootHelpDialog.h>
#include <TString.h>
#include <TSystem.h>
#include <TTree.h>
#include <TVirtualPad.h>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <list>
#include <stdexcept>

#include "Jpsi/GUITranslation.h"
#include "Jpsi/TMultiView.h"
#include "Utility/LanguageProvider.h"

class TEveElement;
class TEveGeoShapeExtract;

namespace Jpsi
{
// Main class that does all the work
//______________________________________________________________________________
//______________________________________________________________________________

TVSDReader::TVSDReader(const char* file_name)
  : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
  , fFile(nullptr)
  , fDirectory(nullptr)
  , fEvDirKeys(nullptr)
  , fVSD(nullptr)
  , fMaxEv(-1)
  , fCurEv(-1)
  , fMaxR(600)
  , fLoadTracks(kFALSE)
  , fGeometrySet(kTRUE)
  , fApplyTrackCuts(kFALSE)
  , fMainFrame(new TGMainFrame(gClient->GetRoot(), 1000, 600))
  , fNavigation(fMainFrame, nullptr)
  , fEnableQuickAnalysis(kFALSE)
  , l1(nullptr)
  , l2(nullptr)
  , l3(nullptr)
  , l4(nullptr)
  , l5(nullptr)
  , l6(nullptr)
  , p1(0.)
  , p2(0.)
  , de1(0.)
  , de2(0.)
  , fTrackList(nullptr)
{
  // Loading the Data file begin
  fFile = TFile::Open(file_name);

  if (fFile == nullptr) {
    throw std::invalid_argument(Form("VSD_Reader : Can not open file '%s'!", file_name));
  }

  gMultiView = new TMultiView();
  fEvDirKeys = new TObjArray;
  TPMERegexp name_re("Event\\d+");
  TObject* obj = nullptr;

  TList* keys = fFile->GetListOfKeys();
  Int_t i = 0;
  while ((obj = keys->At(i)) != nullptr) {
    if (name_re.Match(obj->GetName()) != 0) {
      fEvDirKeys->Add(obj);
    }
    i++;
  }

  fMaxEv = fEvDirKeys->GetEntriesFast();
  if (fMaxEv == 0) {
    printf("VSD_Reader: No events to show ... terminating.");
    gSystem->Exit(1);
  }
  // Configure the Navigation
  fNavigation.SetupSignalSlotsTMP(this);

  // Loading the data file end
  fVSD = new TEveVSD;

  // Creating the histogram for tab 4
  gJpsiHist = new TH1F(fTranslation.StatisticsJpsi(), fTranslation.DifferenceEPmEEpPP(), 60, 0., 6);
  gJpsiHist->SetLineColor(6);
  gJpsiHist->SetMarkerColor(6);
  gJpsiHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
  gJpsiHist->GetYaxis()->SetTitle(fTranslation.Counts());
  gJpsiHist->Sumw2();

  gMinvHist = new TH1F(fTranslation.StatisticsElectronPositron(),
                       fTranslation.InvariantMassElectronPositron(), 60, 0., 6);
  gMinvHist->SetLineColor(2);
  gMinvHist->SetMarkerColor(2);
  gMinvHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
  gMinvHist->GetYaxis()->SetTitle(fTranslation.Counts());
  gMinvHist->Sumw2();

  geeHist = new TH1F(fTranslation.StatisticsElectronElectron(),
                     fTranslation.InvariantMassElectronElectron(), 60, 0., 6);
  geeHist->SetLineColor(4);
  geeHist->SetMarkerColor(4);
  geeHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
  geeHist->GetYaxis()->SetTitle(fTranslation.Counts());
  geeHist->Sumw2();

  gppHist = new TH1F(fTranslation.StatisticsPositronPositron(),
                     fTranslation.InvariantMassPositronPositron(), 60, 0., 6);
  gppHist->SetLineColor(416);
  gppHist->SetMarkerColor(416);
  gppHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
  gppHist->GetYaxis()->SetTitle(fTranslation.Counts());
  gppHist->Sumw2();

  geHist = new TH1F(fTranslation.CopyElectronElectron(),
                    fTranslation.ElectronElectronDistribution(), 60, 0., 6);
  geHist->Sumw2();

  gpHist = new TH1F(fTranslation.CopyPositronPositron(),
                    fTranslation.PositronPositronDistribution(), 60, 0., 6);
  gpHist->Sumw2();
  // Creating histograms end

  // Creating histogram for tab 3
  Int_t nbinsX = 200;
  Int_t nbinsY = 120;
  auto* binLimitsX = new Float_t[nbinsX + 1];
  auto* binLimitsY = new Float_t[nbinsX + 1];
  Float_t firstX = 0.1;
  Float_t lastX = 20.;
  Float_t firstY = 20.;
  Float_t lastY = 140.;
  Float_t expMax = TMath::Log(lastX / firstX);
  for (Int_t i = 0; i < nbinsX + 1; ++i) {
    binLimitsX[i] = firstX * TMath::Exp(expMax / nbinsX * (Float_t)i);
  }
  for (Int_t i = 0; i < nbinsY + 1; ++i) {
    binLimitsY[i] = firstY + i * (lastY - firstY) / nbinsY;
  }
  gEnergyLoss = new TH2F(fTranslation.SpecificEnergyLoss(), fTranslation.SpecificEnergyLoss(),
                         nbinsX, binLimitsX, nbinsY, binLimitsY);
  gEnergyLoss->GetXaxis()->SetTitle(fTranslation.MomentumGeVc());
  gEnergyLoss->GetXaxis()->SetTitleOffset(1.5);
  gEnergyLoss->GetYaxis()->SetTitle("dE/dx in TPC (arb. units)");
  gEnergyLoss->SetStats(false);
  // Creating histogram end
}

TVSDReader::~TVSDReader()
{
  // Destructor.

  DropEvent();
  delete fVSD;
  delete fEvDirKeys;

  fFile->Close();
  delete fFile;
}

void TVSDReader::Exit()
{
  gEve->GetBrowser()->UnmapWindow();
  gSystem->CleanCompiledMacros();
}

namespace
{
void SetupLine(TLine* l, Float_t x1, Float_t x2, Float_t y1, Float_t y2, Int_t LineColor = 2)
{
  l->SetX1(x1);
  l->SetX2(x2);
  l->SetY1(y1);
  l->SetY2(y2);
  l->SetLineColor(LineColor);
  l->SetLineStyle(5);
}
} // namespace
void TVSDReader::SetPIDCutValues()
{
  p1 = gAliceSelector->fp1->GetNumber();
  p2 = gAliceSelector->fp2->GetNumber();
  de1 = gAliceSelector->fde1->GetNumber();
  de2 = gAliceSelector->fde2->GetNumber();

  delete l1;
  delete l2;
  delete l3;
  delete l4;

  l1 = new TLine;
  l2 = new TLine;
  l3 = new TLine;
  l4 = new TLine;

  SetupLine(l1, p1, p1, de1, de2);
  SetupLine(l2, p2, p2, de1, de2);
  SetupLine(l3, p1, p2, de1, de1);
  SetupLine(l4, p1, p2, de2, de2);
}

void TVSDReader::PlotPIDLines()
{
  pad2->cd();
  gEnergyLoss->Draw("colz");
  l1->Draw();
  l2->Draw();
  l3->Draw();
  l4->Draw();
  pad2->cd()->Update();
}

void TVSDReader::SetMassLineValues()
{
  delete l5;
  delete l6;

  l5 = new TLine;
  l6 = new TLine;

  SetupLine(l5, minm, minm, -100, 100, 1);
  SetupLine(l6, maxm, maxm, -100, 100, 1);
}

void TVSDReader::PlotMassLines()
{
  pad->cd(2);
  l5->Draw();
  l6->Draw();
  pad->cd(2)->Update();
}

void TVSDReader::CalculateIntegral()
{
  minm = gAliceExtractor->fm1->GetNumber();
  maxm = gAliceExtractor->fm2->GetNumber();

  Float_t epsilon = 0.01;
  Int_t massBin1 = gJpsiHist->FindBin(minm + epsilon);
  Int_t massBin2 = gJpsiHist->FindBin(maxm - epsilon);

  numJpsi = gJpsiHist->Integral(massBin1, massBin2);
  bg = (geeHist->Integral(massBin1, massBin2)) + (gppHist->Integral(massBin1, massBin2));

  if (numJpsi + bg > 0.) {
    significance = numJpsi / (TMath::Sqrt(numJpsi + bg));
  }
  if (bg > 0.0) {
    sb = 1. * numJpsi / bg;
  } else if (numJpsi > 0.0) {
    sb = 9999.;
  }
}

void TVSDReader::SetExtractorFieldValues()
{
  gAliceExtractor->JpsiCount->SetNumber(numJpsi);
  gAliceExtractor->Sigma->SetNumber(significance);
  gAliceExtractor->SonB->SetNumber(sb);
}

void TVSDReader::Autosave()
{
  std::ofstream myresult;
  myresult.open("masterclass.save", std::ofstream::out | std::ofstream::app);
  myresult << "Momentum region: " << p1 << " < p < " << p2 << std::endl;
  myresult << "Specific energy loss: " << de1 << " < dE/dx < " << de2 << std::endl;
  myresult << "Invariant mass window: " << minm << "< m <" << maxm << std::endl;
  myresult << "Number of events = " << nEvents << std::endl;
  myresult << "Number of Jpsi's: " << numJpsi << std::endl;
  myresult << "Signal/Background: " << sb << std::endl;
  myresult << "Significance: " << significance << std::endl;
  myresult << std::endl;
  myresult.close();
}

void TVSDReader::Instructions()
{
  auto* instructions = new TGMainFrame();

  instructions->SetWindowName(fTranslation.InstructionsJpsi());
  auto* gf = new TGGroupFrame(instructions, "");

  auto* hf = new TGHorizontalFrame(gf, 750, 350, kFixedWidth);
  auto* helpText = new TGTextView(hf, 760, 400);
  helpText->LoadBuffer(fTranslation.InstructionsJpsiText());

  hf->AddFrame(helpText, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  hf = new TGHorizontalFrame(gf, 100, 400, kFixedWidth);
  auto* b = new TGTextButton(hf, fTranslation.ParticleIdentificationPlot());
  b->Connect("Clicked()", "Jpsi::TVSDReader", this, "DetectorInfo()");
  hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  b = new TGTextButton(hf, fTranslation.ButtonClose());
  b->Connect("Clicked()", "TGMainFrame", instructions, "UnmapWindow()");
  hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  instructions->AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  instructions->MapSubwindows();

  instructions->Resize(instructions->GetDefaultSize());
  instructions->MapWindow();
}

void TVSDReader::StudentSet()
{
  Instructions();

  // Start TEventVisualization Constructor
  TEveBrowser* browser = gEve->GetBrowser();
  browser->GetMainFrame()->DontCallClose();
  browser->GetMainFrame()->SetWindowName(fTranslation.MasterclassTitle());

  if (browser->GetTabRight()->GetTabTab(3) != nullptr)
    browser->GetTabRight()->RemoveTab(3);

  if (browser->GetTabRight()->GetTabTab(2) != nullptr)
    browser->GetTabRight()->RemoveTab(2);

  if (browser->GetTabLeft()->GetTabTab(0) != nullptr)
    browser->GetTabLeft()->RemoveTab(0);

  if (browser->GetTabBottom()->GetTabTab(0) != nullptr)
    browser->GetTabBottom()->RemoveTab(0);
  // End TEventVisualization Constructor

  browser->StartEmbedding(TRootBrowser::kLeft);

  auto* LeftTab = browser->GetTabLeft();
  fMainFrame->ReparentWindow(LeftTab);
  LeftTab->AddTab("Navigation", fMainFrame);
  LeftTab->RemoveTab(0);
  LeftTab->MapSubwindows();
  fMainFrame->SetCleanup(kDeepCleanup);

  // TODO Replace with Utility::internal::TInstructionsFrame
  fMainFrame->AddFrame(fNavigation.fInstructionsJpsiFrame);
  // TODO Replace with internal::TEventsFrame
  fMainFrame->AddFrame(fNavigation.fEventNavigationJpsiFrame);

  fMainFrame->AddFrame(fNavigation.fAnalysisJpsiFrame);
  fMainFrame->AddFrame(fNavigation.fSteeringJpsiFrame);
  fMainFrame->AddFrame(fNavigation.fQuickAnalysisJpsiFrame);

  fMainFrame->MapSubwindows();
  fMainFrame->Resize();
  fMainFrame->MapWindow();

  browser->StopEmbedding(fTranslation.Tools());
  browser->StartEmbedding(TRootBrowser::kBottom);

  fMainFrame = new TGMainFrame(gClient->GetRoot(), 1000, 600, kHorizontalFrame);
  fMainFrame->SetWindowName("XX GUI");
  fMainFrame->SetCleanup(kDeepCleanup);

  const TGFont* font = gClient->GetFont("-*-helvetica-bold-r-normal-*-20-*-*-*-*-*-*-*");
  FontStruct_t buttonFont = font->GetFontStruct();

  ULong_t buttonRedColor;
  gClient->GetColorByName("red", buttonRedColor);
  auto* b = new TGTextButton(fMainFrame, fTranslation.ButtonExit());
  b->SetTextColor(buttonRedColor);
  b->SetFont(buttonFont);
  b->Connect("Clicked()", "TApplication", gApplication, "Terminate()");

  fMainFrame->AddFrame(b, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 1, 1, 1, 1));
  fMainFrame->MapSubwindows();
  fMainFrame->Resize();
  fMainFrame->MapWindow();

  browser->StopEmbedding(" ");
  browser->StartEmbedding(TRootBrowser::kRight);

  // END OF GUI Setup

  // Third Tab is created here
  pad2 = new TCanvas();
  gPad->SetLogx(1);
  gEnergyLoss->Draw();

  browser->StopEmbedding(fTranslation.ParticleIdentification());
  //  GotoEvent(0);
  browser->GetTabRight()->SetTab(1);

  browser->StopEmbedding(" ");
  browser->StartEmbedding(TRootBrowser::kRight);
  // Third tab done

  // 4th tab is created here
  pad = new TCanvas();
  pad->Divide(2, 2);

  pad->cd(1);
  gMinvHist->Draw("ep");

  pad->cd(2);
  gJpsiHist->Draw("ep");

  pad->cd(3);
  geeHist->Draw("ep");

  pad->cd(4);
  gppHist->Draw("ep");

  browser->StopEmbedding(fTranslation.InvariantMass());
  //  GotoEvent(0);
  browser->GetTabRight()->SetTab(1);
  // 4th tab is done
}

namespace
{
void CleanUpScene()
{
  TEveSceneList* sceneList1 = gEve->GetScenes();
  auto i = sceneList1->BeginChildren();
  auto* sceneList2 = (TEveSceneList*)*i;
  sceneList2->RemoveElements();
  i++;
  i++;
  auto* sceneList3 = (TEveSceneList*)*i;
  sceneList3->RemoveElements();
  i++;
  auto* sceneList4 = (TEveSceneList*)*i;
  sceneList4->RemoveElements();
}
} // namespace

void TVSDReader::DestroyGeometry()
{
  CleanUpScene();

  gMultiView->SetDepth(-10);
  gMultiView->DestroyGeomRPhi();
  gMultiView->DestroyGeomRhoZ();
  gMultiView->SetDepth(0);
  gMultiView->SetRPhiAxes();
  gMultiView->SetRhoZAxes();
}

void TVSDReader::GeometryDefault()
{
  // TODO This is done in TEventVisualization::LoadDetectorGeometry
  CleanUpScene();

  TEveGeoShape* gentle_geom = nullptr;

  TFile* geom = TFile::Open(Utility::ResourcePath("data/Utility/geometry.root"));
  if (geom == nullptr)
    return;
  auto* gse = (TEveGeoShapeExtract*)geom->Get("Gentle");
  gentle_geom = TEveGeoShape::ImportShapeExtract(gse, nullptr);
  geom->Close();
  delete geom;

  gentle_geom->SetRnrSelf(kFALSE);

  // ITS
  auto i = gentle_geom->BeginChildren();

  // TPC
  i++;

  // TRD+TOF
  i++;

  auto* lvl1 = static_cast<TEveGeoShape*>(*i);
  lvl1->SetRnrSelf(kFALSE);
  auto j = lvl1->BeginChildren();

  // Number of elements that are modified with the transparency.
  // Probably all detector elements?
  const auto ModificationCount = 19;
  for (int i = 0; i < ModificationCount; ++i) {
    auto* trd1 = static_cast<TEveGeoShape*>(*++j);
    trd1->SetRnrSelf(kTRUE);
    trd1->SetMainTransparency(80);
  }

  gEve->AddGlobalElement(gentle_geom);

  gMultiView->SetDepth(-10);
  gMultiView->DestroyGeomRPhi();
  gMultiView->ImportGeomRPhi(gentle_geom);
  gMultiView->DestroyGeomRhoZ();
  gMultiView->ImportGeomRhoZ(gentle_geom);
  gMultiView->SetDepth(0);
  gMultiView->SetRPhiAxes();
  gMultiView->SetRhoZAxes();
}

void TVSDReader::ChangeGeometry()
{
  if (fGeometrySet) {
    DestroyGeometry();
    fGeometrySet = kFALSE;
  } else {
    GeometryDefault();
    fGeometrySet = kTRUE;
  }
}

void TVSDReader::AttachEvent()
{
  // Attach event data from current directory.
  fVSD->LoadTrees();
  fVSD->SetBranchAddresses();
}

void TVSDReader::DropEvent()
{
  // Drop currently held event data, release current directory.

  // Drop old visualization structures.
  gEve->GetViewers()->DeleteAnnotations();
  gEve->GetCurrentEvent()->DestroyElements();

  if (fTrackList != nullptr)
    // fTrackList->DestroyElements();
    // delete fTrackList;
    fTrackList = nullptr;

  // Drop old event-data.
  fVSD->DeleteTrees();
  delete fDirectory;
  fDirectory = nullptr;
}

// Event navigation
void TVSDReader::BeginAgain()
{
  p1 = 0.;
  p2 = 0.;
  de1 = 0.;
  de2 = 0.;

  fLoadTracks = kFALSE;
  fApplyTrackCuts = kFALSE;
  fEnableQuickAnalysis = kFALSE;

  fNavigation.fAnalysisJpsiFrame->fButtonLoadTracks->SetEnabled(kTRUE);
  fNavigation.fAnalysisJpsiFrame->fButtonFillPidHistos->SetEnabled(kTRUE);
  fNavigation.fAnalysisJpsiFrame->fButtonDefineTrackCuts->SetEnabled(kTRUE);
  fNavigation.fAnalysisJpsiFrame->fButtonApplyTrackCuts->SetEnabled(kTRUE);
  fNavigation.fAnalysisJpsiFrame->fButtonFillInvMass->SetEnabled(kTRUE);
  fNavigation.fAnalysisJpsiFrame->fCheckboxApplyTrackCuts->SetDisabledAndSelected(kFALSE);
  fNavigation.fAnalysisJpsiFrame->fCheckboxApplyTrackCuts->SetEnabled(kFALSE);
  fNavigation.fAnalysisJpsiFrame->fCheckboxLoadTracks->SetDisabledAndSelected(kFALSE);

  fNavigation.fQuickAnalysisJpsiFrame->fCheckBoxQuickAnalysis->SetDisabledAndSelected(kFALSE);
  fNavigation.fQuickAnalysisJpsiFrame->fButtonJumpEvents->SetEnabled(kFALSE);

  GotoEvent(0);
}

// TODO This is done in TEventAnalyseGUI
void TVSDReader::NextEvent()
{
  if (fCurEv < fMaxEv - 1) {
    ResetMatrices();
    GotoEvent(fCurEv + 1);
  }
}

void TVSDReader::PrevEvent()
{
  if (fCurEv != 0) {
    ResetMatrices();
    GotoEvent(fCurEv - 1);
  }
}

Bool_t TVSDReader::GotoEvent(Int_t ev)
{
  if (ev < 0 || ev >= fMaxEv) {
    printf("GotoEvent: fMaxEv = fEvDirKeys->GetEntriesFast();Invalid event id %d.", ev);
    return kFALSE;
  }

  DropEvent();

  fCurEv = ev;
  fDirectory = (TDirectory*)((TKey*)fEvDirKeys->At(fCurEv))->ReadObj();
  fVSD->SetDirectory(fDirectory);
  AttachEvent();
  fNavigation.SetEventNumber(fCurEv + 1, fMaxEv);

  // Load event data into visualization structures.

  if (fLoadTracks) {
    LoadTracksFromTree(fMaxR);
  }

  if (fEnableQuickAnalysis) {
    FillInvariantMassHistos();
    FillEnergyLossHisto();
  } else {
    TEveElement* top = gEve->GetCurrentEvent();
    if (gMultiView != nullptr) {
      gMultiView->DestroyEventRPhi();
      gMultiView->ImportEventRPhi(top);
      gMultiView->DestroyEventRhoZ();
      gMultiView->ImportEventRhoZ(top);
      gEve->Redraw3D(kFALSE, kTRUE);
    }
  }
  return kTRUE;
}
// END TEventAnalyseGUI choose event

void TVSDReader::SwapQuickAnalysis()
{
  Bool_t enable = !fEnableQuickAnalysis;

  fNavigation.fAnalysisJpsiFrame->fButtonLoadTracks->SetEnabled(!enable);
  fNavigation.fAnalysisJpsiFrame->fButtonFillPidHistos->SetEnabled(!enable);
  fNavigation.fAnalysisJpsiFrame->fButtonDefineTrackCuts->SetEnabled(!enable);
  fNavigation.fAnalysisJpsiFrame->fButtonApplyTrackCuts->SetEnabled(!enable);
  fNavigation.fAnalysisJpsiFrame->fButtonFillInvMass->SetEnabled(!enable);

  fEnableQuickAnalysis = enable;
  fNavigation.fQuickAnalysisJpsiFrame->fCheckBoxQuickAnalysis->SetDisabledAndSelected(enable);
  fNavigation.fQuickAnalysisJpsiFrame->fButtonJumpEvents->SetEnabled(enable);

  if (enable) {
    fLoadTracks = kTRUE;
    fNavigation.fAnalysisJpsiFrame->fCheckboxApplyTrackCuts->SetEnabled(kFALSE);
    fNavigation.fAnalysisJpsiFrame->fCheckboxLoadTracks->SetDisabledAndSelected(kTRUE);
  }
}

void TVSDReader::EventsJump()
{
  if (fEnableQuickAnalysis) {
    Int_t nLoop;
    if (fNavigation.fQuickAnalysisJpsiFrame->fNLoop->GetNumber() + fCurEv < fMaxEv) {
      nLoop = fNavigation.fQuickAnalysisJpsiFrame->fNLoop->GetNumber();
    } else {
      auto* startMessage = new TRootHelpDialog(gClient->GetRoot(), "warning", 250, 100);
      startMessage->SetText(fTranslation.WarningTooManyEvents());
      startMessage->Popup();
      nLoop = 0;
    }
    for (int i = 0; i < nLoop; i++) {
      NextEvent();
    }
    pad2->cd()->Update();
    pad->cd(1)->Update();
    pad->cd(2)->Update();
    pad->cd(3)->Update();
    pad->cd(4)->Update();
  } else {
    auto* startMessage = new TRootHelpDialog(gClient->GetRoot(), "warning", 250, 100);
    startMessage->SetText(fTranslation.WarningQuickAnalysis());
    startMessage->Popup();
  }
}

void TVSDReader::LoadTracks()
{
  if (fLoadTracks) {
    fLoadTracks = kFALSE;
    GotoEvent(fCurEv);
    if (fNavigation.fAnalysisJpsiFrame->fCheckboxLoadTracks != nullptr)
      fNavigation.fAnalysisJpsiFrame->fCheckboxLoadTracks->SetDisabledAndSelected(kFALSE);
  } else {
    fLoadTracks = kTRUE;
    GotoEvent(fCurEv);
    fNavigation.fAnalysisJpsiFrame->fCheckboxLoadTracks->SetDisabledAndSelected(kTRUE);
  }
}

void TVSDReader::EnableTrackCutsButton()
{
  fNavigation.fAnalysisJpsiFrame->fButtonApplyTrackCuts->SetEnabled(kTRUE);
}

void TVSDReader::SwapApplyTrackCuts()
{
  if (fApplyTrackCuts) {
    fApplyTrackCuts = kFALSE;
    GotoEvent(fCurEv);
    fNavigation.fAnalysisJpsiFrame->fCheckboxApplyTrackCuts->SetDisabledAndSelected(kFALSE);
    pad2->cd();
    gEnergyLoss->Draw("colz");
    pad2->cd()->Update();
  } else {
    ApplyTrackCuts();
  }
}

void TVSDReader::ApplyTrackCuts()
{
  fApplyTrackCuts = kTRUE;
  GotoEvent(fCurEv);
  fNavigation.fAnalysisJpsiFrame->fCheckboxApplyTrackCuts->SetDisabledAndSelected(kTRUE);
  if (gAliceSelector != nullptr) {
    pad2->cd();
    PlotPIDLines();
    pad2->cd()->Update();
  }
}

// Track loading
// TODO In Utility/VSDReader.cxx
void TVSDReader::LoadTracksFromTree(Int_t maxR)
{
  if (fTrackList == nullptr) {
    fTrackList = new TEveTrackList("ESD Tracks");
    fTrackList->SetMainColor(kBlue);
    // fTrackList->IncDenyDestroy();
  } else {
    fTrackList->DestroyElements();
  }

  TEveTrackPropagator* trkProp = fTrackList->GetPropagator();
  trkProp->SetMaxR(maxR);

  TTree* fTreeR = fVSD->fTreeR;
  if (fTreeR != nullptr) {
    Int_t nTracks = fTreeR->GetEntries();

    ResetMatrices();
    ResetDedxArray();

    for (Int_t i = 0; i < nTracks; ++i) {
      fTreeR->GetEntry(i);
      auto* track = new TEveTrack(&fVSD->fR, trkProp);

      Float_t px = track->GetMomentum().fX;
      Float_t py = track->GetMomentum().fY;
      Float_t pz = track->GetMomentum().fZ;
      Float_t dedx = track->GetStatus();
      Int_t charge = track->GetCharge();
      Float_t pSquared = px * px + py * py + pz * pz;

      if (fApplyTrackCuts &&
          (dedx >= de2 || dedx < de1 || pSquared >= p2 * p2 || pSquared <= p1 * p1)) {
        track->Destroy();
        continue;
      }
      nSelectedTracks++;
      // Fill arrays for energy loss histogram
      gArrP[i] = TMath::Sqrt(px * px + py * py + pz * pz);
      gArrdEdx[i] = dedx;

      // Fill arrays for invariant mass calculation

      // if energy loss below threshold, assume particle is a pion, otherwise assume it is an
      // electron
      Float_t massSquared = dedx > 62 ? 0. : 0.019;
      Float_t e = TMath::Sqrt(pSquared + massSquared);
      if (charge > 0) {
        po[nPositrons][0] = e;
        po[nPositrons][1] = px;
        po[nPositrons][2] = py;
        po[nPositrons][3] = pz;
        nPositrons++;
      } else {
        el[nElectrons][0] = e;
        el[nElectrons][1] = px;
        el[nElectrons][2] = py;
        el[nElectrons][3] = pz;
        nElectrons++;
      }
      track->SetAttLineAttMarker(fTrackList);
      fTrackList->AddElement(track);
    }
  }
  fTrackList->MakeTracks();
  gEve->AddElement(fTrackList);
  gEve->Redraw3D();
}

namespace
{
template <typename H1, typename H2>
void FillHistogram(Int_t n1, Int_t n2, Float_t Particle[100][4], H1* Hist1, H2* Hist2)
{
  for (int i = 0; i < n1 - 1; i++) {
    for (int j = i + 1; j < n2; j++) {
      Float_t mass =
        TMath::Sqrt((Particle[i][0] + Particle[j][0]) * (Particle[i][0] + Particle[j][0]) -
                    (Particle[i][1] + Particle[j][1]) * (Particle[i][1] + Particle[j][1]) -
                    (Particle[i][2] + Particle[j][2]) * (Particle[i][2] + Particle[j][2]) -
                    (Particle[i][3] + Particle[j][3]) * (Particle[i][3] + Particle[j][3]));
      Hist1->Fill(mass);
      Hist2->Fill(mass);
    }
  }
}
} // namespace

void TVSDReader::FillInvariantMassHistos()
{
  if (fLoadTracks) {
    nEvents++;
    FillHistogram(nElectrons, nElectrons, el, geeHist, geHist);
    FillHistogram(nPositrons, nPositrons, po, gppHist, gpHist);
    FillHistogram(nElectrons, nPositrons, el, gMinvHist, gJpsiHist);

    pad->cd(1);
    Float_t yMax = gMinvHist->GetBinContent(gMinvHist->GetMaximumBin());
    yMax += sqrt(yMax);
    yMax *= 1.05;
    gMinvHist->GetYaxis()->SetRangeUser(0, yMax);
    gMinvHist->Draw();

    pad->cd(2);
    gJpsiHist->Draw();

    pad->cd(3);
    geeHist->GetYaxis()->SetRangeUser(0, yMax);
    geeHist->Draw();

    pad->cd(4);
    gppHist->GetYaxis()->SetRangeUser(0, yMax);
    gppHist->Draw();

    pad->cd(1)->Update();
    pad->cd(2)->Update();
    pad->cd(3)->Update();
    pad->cd(4)->Update();

    gJpsiHist->Add(geHist, -1);
    gJpsiHist->Add(gpHist, -1);

    ResetMatrices();
  } else {
    auto* startMessage = new TRootHelpDialog(gClient->GetRoot(), "warning", 400, 100);
    startMessage->SetText(fTranslation.WarningLoadBeforeFilling());
    startMessage->Popup();
  }
}

void TVSDReader::ResetMatrices()
{
  for (int i = 0; i < 100; i++) {
    for (int j = 0; j < 4; j++) {
      el[i][j] = 0;
      po[i][j] = 0;
    }
  }
  nPositrons = 0;
  nElectrons = 0;
  geHist->Reset();
  gpHist->Reset();
}

void TVSDReader::ResetDedxArray()
{
  for (int i = 0; i < nSelectedTracks; i++) {
    gArrP[i] = 0;
    gArrdEdx[i] = 0;
  }
  nSelectedTracks = 0;
}

namespace
{
template <typename HistogramType>
void ClearHistogram(TCanvas* pad, HistogramType* gMinvHist, Int_t Idx = -1)
{
  if (Idx != -1)
    pad->cd(Idx);

  gMinvHist->Reset();
  gMinvHist->Draw();

  if (Idx != -1)
    pad->cd(Idx)->Update();
}
} // namespace

void TVSDReader::ClearHisto()
{
  ClearHistogram(pad, gMinvHist, 1);
  ClearHistogram(pad2, gEnergyLoss);
  ClearHistogram(pad, geeHist, 3);
  ClearHistogram(pad, gppHist, 4);
  ClearHistogram(pad, gJpsiHist, 2);
}

void TVSDReader::FillEnergyLossHisto()
{
  if (fLoadTracks) {
    if (nSelectedTracks > 0) {
      for (int i = 0; i < nSelectedTracks; i++) {
        gEnergyLoss->Fill(gArrP[i], gArrdEdx[i]);
      }
      pad2->cd();
      gEnergyLoss->Draw("colz");
      if (fApplyTrackCuts && (gAliceSelector != nullptr)) {
        PlotPIDLines();
      }
      pad2->cd()->Update();
      ResetDedxArray();
    }
  } else {
    auto* startMessage = new TRootHelpDialog(gClient->GetRoot(), "warning", 400, 100);
    startMessage->SetText(fTranslation.WarningLoadBforeEnergy());
    startMessage->Popup();
  }
}

} // namespace Jpsi
