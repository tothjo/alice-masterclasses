#ifndef TEXTRACTOR_H_JQ3GV7X4
#define TEXTRACTOR_H_JQ3GV7X4

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>

class TBuffer;
class TClass;
class TGLabel;
class TGNumberEntryField;
class TGTextButton;
class TLine;
class TMemberInspector;

namespace Jpsi
{
class TVSDReader;
struct TGUIEnglish;

class TExtractor : public TGMainFrame
{
 private:
  const TGUIEnglish& fTranslation;

 public:
  TVSDReader* parent;
  TGTextButton* b;
  TGNumberEntryField* fm1;
  TGNumberEntryField* fm2;
  TGNumberEntryField* JpsiCount;
  TGNumberEntryField* Sigma;
  TGNumberEntryField* SonB;
  TLine* l5;
  TLine* l6;
  TGLabel* val1;
  Float_t p1, p2, de1, de2;

  TExtractor(TVSDReader* parent, Float_t minm, Float_t maxm);
  void Instructions();

  ClassDefOverride(TExtractor, 0);
};

} // namespace Jpsi

#endif /* end of include guard: TEXTRACTOR_H_JQ3GV7X4 */
