#include "Jpsi/TSelector.h"

#include <GuiTypes.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>
#include <TGWidget.h>
#include <TRootHelpDialog.h>
#include <TString.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Jpsi
{
// Track Selection Window
//______________________________________________________________________________
//______________________________________________________________________________

TSelector::TSelector(TVSDReader* parent, Float_t currentP1, Float_t currentP2, Float_t currentDe1,
                     Float_t currentDe2)
  : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
  , p1(currentP1)
  , p2(currentP2)
  , de1(currentDe1)
  , de2(currentDe2)
{
  auto* gf = new TGGroupFrame(this, "");

  const TGFont* font = gClient->GetFont("-*-times-bold-r-*-*-16-*-*-*-*-*-*-*");
  FontStruct_t buttonFont = font->GetFontStruct();
  ULong_t buttonRedColor;
  gClient->GetColorByName("red", buttonRedColor);
  auto* b = new TGTextButton(gf, fTranslation.ButtonInstructions());
  b->SetTextColor(buttonRedColor);
  b->SetFont(buttonFont);
  b->Connect("Clicked()", "Jpsi::TSelector", this, "Instructions()");

  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));

  auto* hf = new TGHorizontalFrame(gf, 200, 20, kFixedWidth);

  val1 = new TGLabel(hf, "    ");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "min");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "max");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  hf = new TGHorizontalFrame(gf, 200, 20, kFixedWidth);

  val1 = new TGLabel(hf, "p");
  fp1 = new TGNumberEntryField(hf);
  fp1->SetNumber(p1);
  fp2 = new TGNumberEntryField(hf);
  fp2->SetNumber(p2);

  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fp1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fp2, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));

  hf = new TGHorizontalFrame(gf, 200, 20, kFixedWidth);

  val1 = new TGLabel(hf, "dE/dx");
  fde1 = new TGNumberEntryField(hf);
  fde1->SetNumber(de1);
  fde2 = new TGNumberEntryField(hf);
  fde2->SetNumber(de2);

  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fde1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fde2, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));

  b = new TGTextButton(gf, fTranslation.ButtonEngage());
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "SetPIDCutValues()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "PlotPIDLines()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "EnableTrackCutsButton()");
  b->Connect("Pressed()", "Jpsi::TVSDReader", parent, "ApplyTrackCuts()");
  b->Connect("Pressed()", "Jpsi::TSelector", this, "UnmapWindow()");
  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));

  b = new TGTextButton(gf, fTranslation.ButtonClose());
  b->Connect("Pressed()", "Jpsi::TSelector", this, "UnmapWindow()");
  gf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));
  AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));

  SetWindowName(fTranslation.SelectorTitle());

  MapSubwindows();

  // Initialize the layout algorithm via Resize()
  Resize(GetDefaultSize());
  // Map main frame
  MapWindow();
}

void TSelector::Instructions()
{
  auto* instructions =
    new TRootHelpDialog(gClient->GetRoot(), fTranslation.SelectorInstructionsTitle(), 700, 400);
  instructions->SetText(fTranslation.SelectorInstructionsText());

  instructions->Popup();
}

} // namespace Jpsi
