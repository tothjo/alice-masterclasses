#ifndef TSELECTOR_H_BAJ5PFYC
#define TSELECTOR_H_BAJ5PFYC

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>

class TBuffer;
class TClass;
class TGLabel;
class TGNumberEntryField;
class TGTextButton;
class TMemberInspector;

namespace Jpsi
{
class TVSDReader;
struct TGUIEnglish;

class TSelector : public TGMainFrame
{
 private:
  const TGUIEnglish& fTranslation;

 public:
  TVSDReader* parent;
  TGTextButton* b;
  TGNumberEntryField* fp1;
  TGNumberEntryField* fp2;

  TGNumberEntryField* fde1;
  TGNumberEntryField* fde2;

  TGLabel* val1;

  Float_t p1, p2, de1, de2;

  TSelector(TVSDReader* parent, Float_t currentP1, Float_t currentP2, Float_t currentDe1,
            Float_t currentDe2);
  void Instructions();

  ClassDefOverride(TSelector, 0);
};

} // namespace Jpsi

#endif /* end of include guard: TSELECTOR_H_BAJ5PFYC */
