#ifndef TDETECTORINFO_H_AHSISZ2B
#define TDETECTORINFO_H_AHSISZ2B

#include <Rtypes.h>
#include <TGFrame.h>

class TBuffer;
class TClass;
class TGPictureButton;
class TMemberInspector;

namespace Jpsi
{
class TDetectorInfo : public TGMainFrame
{
 protected:
  TGPictureButton* b1;

 public:
  TDetectorInfo();

  ClassDef(TDetectorInfo, 0);
};

} // namespace Jpsi

#endif /* end of include guard: TDETECTORINFO_H_AHSISZ2B */
