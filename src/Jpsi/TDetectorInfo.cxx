#include "Jpsi/TDetectorInfo.h"

#include <TGButton.h>
#include <TGClient.h>
#include <TGLayout.h>
#include <TString.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

namespace Jpsi
{
// Detector Information Window
//______________________________________________________________________________
//______________________________________________________________________________
TDetectorInfo::TDetectorInfo()
{
  const TGUIEnglish& Translation = Utility::TranslationFromEnv<TJPsiLanguage>();
  b1 = nullptr;
  TGTextButton* b2 = nullptr;
  auto* gf = new TGGroupFrame(this, Translation.ALICEDetector());

  auto* hf = new TGHorizontalFrame(gf, 250, 250);
  b1 = new TGPictureButton(hf, gClient->GetPicture(Utility::ResourcePath("doc/Jpsi/dEdx.png")));

  hf->AddFrame(b1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  gf->AddFrame(hf);

  hf = new TGHorizontalFrame(gf);
  {
    b2 = new TGTextButton(hf, Translation.ButtonClose());
    b2->Connect("Clicked()", "Jpsi::TDetectorInfo", this, "UnmapWindow()");
    hf->AddFrame(b2, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  }

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));

  AddFrame(gf);

  SetWindowName(Translation.DetectorInfoTitle());
  MapSubwindows();

  // Initialize the layout algorithm via Resize()
  Resize(GetDefaultSize());

  // Map main frame
  MapWindow();
}

} // namespace Jpsi
