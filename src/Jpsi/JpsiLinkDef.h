#ifdef __CLING__
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version. (See cxx source for full Copyright notice)
 */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Jpsi::TNavigation+;
#pragma link C++ struct Jpsi::internal::TAnalysis+;
#pragma link C++ struct Jpsi::internal::TSteering+;
#pragma link C++ struct Jpsi::internal::TQuickAnalysis+;

#pragma link C++ struct Jpsi::TMultiView+;
#pragma link C++ class Jpsi::TVSDReader+;
#pragma link C++ class Jpsi::TDetectorInfo+;
#pragma link C++ class Jpsi::TSelector+;
#pragma link C++ class Jpsi::TExtractor+;
#pragma link C++ class Jpsi::MasterClassFrame+;

#endif
