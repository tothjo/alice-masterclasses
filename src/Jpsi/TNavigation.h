#ifndef TNAVIGATION_H_MPCOYTDS
#define TNAVIGATION_H_MPCOYTDS

#include <Rtypes.h>
#include <TGFrame.h>
#include <TString.h>

#include "Utility/INavigation.h"

class TBuffer;
class TClass;
class TGCheckButton;
class TGCompositeFrame;
class TGHorizontalFrame;
class TGLabel;
class TGNumberEntryField;
class TGPictureButton;
class TGTextButton;
class TGVerticalFrame;
class TMemberInspector;

namespace Utility
{
class EventDisplay;
} // namespace Utility

namespace Jpsi
{
class TVSDReader;
struct TGUIEnglish;

namespace internal
{
struct TInstructions : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fInstructionsButton;

  TInstructions(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
};

struct TEventNavigation : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;

  TGHorizontalFrame* fLabelFrame;
  TGLabel* fLabelPrevious;
  TGLabel* fLabelCurrent;
  TGLabel* fLabelNext;

  TString fIcondir;
  TGHorizontalFrame* fButtonFrame;
  TGPictureButton* fButtonPrevious;
  TGLabel* fLabelEventNumber;
  TGPictureButton* fButtonNext;

  TEventNavigation(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
};

struct TAnalysis : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;

  TGHorizontalFrame* fLoadTracksLine;
  TGTextButton* fButtonLoadTracks;
  TGCheckButton* fCheckboxLoadTracks;

  TGTextButton* fButtonFillPidHistos;
  TGTextButton* fButtonDefineTrackCuts;

  TGHorizontalFrame* fApplyTrackCuts;
  TGTextButton* fButtonApplyTrackCuts;
  TGCheckButton* fCheckboxApplyTrackCuts;

  TGTextButton* fButtonFillInvMass;
  TGTextButton* fButtonExtractSignal;

  TAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TAnalysis, 0);
};
struct TSteering : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fButtonShowDetector;
  TGTextButton* fButtonToggleBackground;
  TGTextButton* fButtonClearHistogram;
  TGTextButton* fButtonRestart;

  TSteering(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TSteering, 0);
};
struct TQuickAnalysis : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;

  TGHorizontalFrame* fQuickAnalysisFrame;
  TGTextButton* fButtonQuickAnalysis;
  TGCheckButton* fCheckBoxQuickAnalysis;

  TGHorizontalFrame* fEventFrame;
  TGLabel* fLoopNumberLabel;
  TGNumberEntryField* fNLoop;

  TGHorizontalFrame* fJumpFrame;
  TGTextButton* fButtonJumpEvents;

  TQuickAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TQuickAnalysis, 0);
};
} // namespace internal

class TNavigation // : public Utility::INavigation
{
 private:
  friend class TVSDReader;
  const TGUIEnglish& fTranslation;

  internal::TInstructions* fInstructionsJpsiFrame;
  internal::TEventNavigation* fEventNavigationJpsiFrame;
  internal::TAnalysis* fAnalysisJpsiFrame;
  internal::TSteering* fSteeringJpsiFrame;
  internal::TQuickAnalysis* fQuickAnalysisJpsiFrame;

 public:
  TNavigation(TGCompositeFrame* Parent, Utility::EventDisplay* Exercise);
  virtual ~TNavigation() = default;

  /// Dead for now!
  // void SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI,
  // Utility::VSDReader* Reader) override {}

  /// Used for internal Jpsi setup
  /// should be removed and this class should integrate well with the rest of
  /// the framework.
  void SetupSignalSlotsTMP(TVSDReader* Reader);

  void SetEventNumber(Int_t Num, Int_t Max);

  ClassDef(TNavigation, 0);
};
} // namespace Jpsi

#endif /* end of include guard: TNAVIGATION_H_MPCOYTDS */
