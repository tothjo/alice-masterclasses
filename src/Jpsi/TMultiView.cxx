#include "Jpsi/TMultiView.h"

#include <Rtypes.h>
#include <TEveBrowser.h>
#include <TEveGeoShape.h>
#include <TEveManager.h>
#include <TEveProjectionAxes.h>
#include <TEveProjectionManager.h>
#include <TEveProjections.h>
#include <TEveViewer.h>
#include <TEveWindow.h>
#include <TGLViewer.h>
#include <TString.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Jpsi
{
// TMultiView
//
// Structure encapsulating standard views: 3D, r-phi and rho-z.
// Includes scenes and projection managers.
//
// Should be used in compiled mode.
TMultiView::TMultiView()
  : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
{
  // Constructor --- creates required scenes, projection managers
  // and GL viewers.

  // Scenes
  //========

  //      f3DGeomScene = (dynamic_cast<TEveScene*>(gEve->GetScenes()->FirstChild()));

  fRPhiGeomScene = gEve->SpawnNewScene(fTranslation.RPhiGeom(), fTranslation.RPhiGeomDesc());
  fRhoZGeomScene = gEve->SpawnNewScene(fTranslation.RhoZGeom(), fTranslation.RhoZGeomDesc());
  fRPhiEventScene = gEve->SpawnNewScene(fTranslation.RPhiEvent(), fTranslation.RPhiEventDesc());
  fRhoZEventScene = gEve->SpawnNewScene(fTranslation.RhoZEvent(), fTranslation.RhoZEventDesc());

  // Projection managers
  //=====================
  /*
        f3DMgr = new TEveProjectionManager(TEve3DProjection::kPT_3D);
        gEve->AddToListTree(f3DMgr, kFALSE);
        {
           TEveProjectionAxesGL* a = new TEveProjectionAxesGL(f3DMgr);
  //         a->SetMainColor(kWhite);
  //         a->SetTitle("3D");
  //         a->SetTitleSize(0.05);
  //         a->SetTitleFont(102);
  //         a->SetLabelSize(0.025);
  //         a->SetLabelFont(102);
           f3DGeomScene->AddElement(a);
        }

  //   TAxis3D* axis = new TAxis3D();
   //  axis->Paint();
  */
  fRPhiMgr = new TEveProjectionManager(TEveProjection::kPT_RPhi);
  gEve->AddToListTree(fRPhiMgr, kFALSE);
  {
    auto* a = new TEveProjectionAxes(fRPhiMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("R-Phi");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRPhiGeomScene->AddElement(a);
  }

  fRhoZMgr = new TEveProjectionManager(TEveProjection::kPT_RhoZ);
  gEve->AddToListTree(fRhoZMgr, kFALSE);
  {
    auto* a = new TEveProjectionAxes(fRhoZMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("Rho-Z");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRhoZGeomScene->AddElement(a);
  }

  // Viewers
  //=========

  TEveWindowSlot* slot = nullptr;
  TEveWindowPack* pack = nullptr;

  slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
  pack = slot->MakePack();
  pack->SetElementName(fTranslation.MultiView());
  pack->SetHorizontal();
  pack->SetShowTitleBar(kFALSE);
  pack->NewSlot()->MakeCurrent();
  f3DView = gEve->SpawnNewViewer(fTranslation.View3D(), "");

  //      gEve->GetGlobalScene()->AddElement(axis);
  f3DView->AddScene(gEve->GetGlobalScene());
  ///      f3DView->AddScene(f3DGeomScene);
  f3DView->AddScene(gEve->GetEventScene());
  //      f3DView->GetGLViewer()->SetPointScale(0);
  //      f3DView->AddElement(axis);

  pack = pack->NewSlot()->MakePack();
  pack->SetShowTitleBar(kFALSE);
  pack->NewSlot()->MakeCurrent();
  fRPhiView = gEve->SpawnNewViewer(fTranslation.RPhiView(), "");
  fRPhiView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
  fRPhiView->AddScene(fRPhiGeomScene);
  fRPhiView->AddScene(fRPhiEventScene);

  pack->NewSlot()->MakeCurrent();
  fRhoZView = gEve->SpawnNewViewer(fTranslation.RhoZView(), "");
  fRhoZView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
  fRhoZView->AddScene(fRhoZGeomScene);
  fRhoZView->AddScene(fRhoZEventScene);
}

void TMultiView::SetDepth(Float_t d)
{
  // Set current depth on all projection managers.

  fRPhiMgr->SetCurrentDepth(d);
  fRhoZMgr->SetCurrentDepth(d);
}

void TMultiView::InitGeomGentle(TEveGeoShape* g3d, TEveGeoShape* grphi, TEveGeoShape* grhoz)
{
  // Initialize gentle geometry.

  fGeomGentle = g3d;
  fGeomGentleRPhi = grphi;
  fGeomGentleRPhi->IncDenyDestroy();
  fGeomGentleRhoZ = grhoz;
  fGeomGentleRhoZ->IncDenyDestroy();

  ImportGeomRPhi(fGeomGentleRPhi);
  ImportGeomRhoZ(fGeomGentleRhoZ);
}

void TMultiView::DestroyAllGeometries()
{
  // Destroy 3d, r-phi and rho-z geometries.

  fGeomGentle->DestroyElements();
  fGeomGentleRPhi->DestroyElements();
  fGeomGentleRhoZ->DestroyElements();
}

void TMultiView::SetRPhiAxes()
{
  gEve->AddToListTree(fRPhiMgr, kFALSE);
  {
    auto* a = new TEveProjectionAxes(fRPhiMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("R-Phi");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRPhiGeomScene->AddElement(a);
  }
}

void TMultiView::SetRhoZAxes()
{
  gEve->AddToListTree(fRhoZMgr, kFALSE);
  {
    auto* a = new TEveProjectionAxes(fRhoZMgr);
    a->SetMainColor(kWhite);
    a->SetTitle("Rho-Z");
    a->SetTitleSize(0.05);
    a->SetTitleFont(102);
    a->SetLabelSize(0.025);
    a->SetLabelFont(102);
    fRhoZGeomScene->AddElement(a);
  }
}

} // namespace Jpsi
