#include "MasterClassRegistry.h"

#include <iostream>

#include <TString.h>
#include "Raa/ClassContent.h"
#include "Strangeness/ClassContent.h"
#include "Jpsi/ClassContent.h"

namespace EntryPoint
{
void TMasterClassRegistry::ConstructClassContents(Utility::ESupportedLanguages lang)
{
  std::cerr << "Changing language for all register master classes to "
            << Utility::GetLanguageName(lang) << "\n";
  fClasses.clear();
  fClasses.emplace_back(new Raa::TClassContent(lang));
  fClasses.emplace_back(new Strangeness::TClassContent(lang));
  fClasses.emplace_back(new Jpsi::TClassContent(lang));
}
} // namespace EntryPoint
