#ifndef MASTERCLASSREGISTRY_C_OLDOKGVE
#define MASTERCLASSREGISTRY_C_OLDOKGVE

#include <memory>
#include <vector>

#include "Utility/AbstractMasterClassContent.h"
#include "Utility/LanguageProvider.h"

namespace EntryPoint
{
class TMasterClassRegistry
{
 public:
  TMasterClassRegistry(Utility::ESupportedLanguages lang = Utility::English)
  {
    ConstructClassContents(lang);
  }
  void ChangeLanguage(Utility::ESupportedLanguages lang) { ConstructClassContents(lang); }

  const std::vector<std::unique_ptr<Utility::TAbstractMasterClassContent>>& GetClasses() const
  {
    return fClasses;
  }

 private:
  void ConstructClassContents(Utility::ESupportedLanguages lang);

  std::vector<std::unique_ptr<Utility::TAbstractMasterClassContent>> fClasses;
};
} // namespace EntryPoint

#endif /* end of include guard: MASTERCLASSREGISTRY_C_OLDOKGVE */
