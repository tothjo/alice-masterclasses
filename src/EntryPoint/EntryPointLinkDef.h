// Copied from https://github.com/mfasDa/ROOT6tools/blob/master/src/ROOT6toolsLinkDef.h

#ifdef __CLING__
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version. (See cxx source for full Copyright notice)
 */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

// GUIElement.h
#pragma link C++ struct EntryPoint::ITranslateable+;
#pragma link C++ struct EntryPoint::TGUILogo+;
#pragma link C++ struct EntryPoint::TGUISettings+;
#pragma link C++ struct EntryPoint::TGUIDescription+;
#pragma link C++ struct EntryPoint::TGUIClasses+;
#pragma link C++ struct EntryPoint::TGUIControlArea+;

// GUITranslation.h
#pragma link C++ struct EntryPoint::TGUIEnglish+;
#pragma link C++ struct EntryPoint::TGUIGerman+;

// MasterClassRegistry.h
#pragma link C++ class EntryPoint::TMasterClassRegistry+;

#endif
