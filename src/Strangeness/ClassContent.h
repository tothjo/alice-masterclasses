#ifndef CLASSCONTENT_C_LGNS80U5
#define CLASSCONTENT_C_LGNS80U5

#include <memory>
#include <stdexcept>

#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep

#include "Strangeness/instructors/Collect.h"
#include "Strangeness/instructors/Combine.h"
#include "Strangeness/part1/Part1.h"
#include "Strangeness/part2/Part2.h"

namespace Strangeness
{
struct TContentEnglish : Utility::TContentTranslation {
  TContentEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TContentTranslation(lang)
  {
    std::cerr << "Registering English Strangeness Class content\n";
#include "Strangeness/keys_class_trans.txt.en"

    RegisterText("Description",
#include "Strangeness/Description.en.html"
    );
  }
};

LANG_TRANSLATION(TContent, German)
{
  std::cerr << "Registering German Strangeness Class content\n";
#include "Strangeness/keys_class_trans.txt.de"

  RegisterText("Description",
#include "Strangeness/Description.de.html"
  );
}

inline std::unique_ptr<Utility::TContentTranslation> LanguageFactory(
  Utility::ESupportedLanguages lang)
{
  switch (lang) {
    case Utility::English:
      return std_fix::make_unique<TContentEnglish>();
    case Utility::German:
      return std_fix::make_unique<TContentGerman>();
    case Utility::NLanguages:
      break;
  }
  throw std::runtime_error("Requested Translation for Strangeness does not exist");
}

struct TClassContent : Utility::TAbstractMasterClassContent {
  TClassContent(Utility::ESupportedLanguages lang)
    : TAbstractMasterClassContent(LanguageFactory(lang))
  {
    AddExercise(std_fix::make_unique<TInspectPP>());
    AddExercise(std_fix::make_unique<TPeakBackground>());
    AddExercise(std_fix::make_unique<TCollectResults>());
    AddExercise(std_fix::make_unique<TCombine>());
  }
};
} // namespace Strangeness

#endif /* end of include guard: CLASSCONTENT_C_LGNS80U5 */
