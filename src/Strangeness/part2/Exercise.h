/**
 * @file   StrangeExercise.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:22:41 2017
 *
 * @brief  Stering of second part of exercise
 *
 * @ingroup alice_masterclass_str_part2
 */
#ifndef STRANGEEXERCISE_C
#define STRANGEEXERCISE_C

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TGListTree.h>
#include <TGMsgBox.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TNtuple.h>
#include <TPaveStats.h>
#include <TROOT.h>
#include <TRootBrowser.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part2/Fitter.h"
#include "Strangeness/part2/Picker.h"
#include "Utility/Exercise.h"

class TF1;
class TFile;
class TGListTreeItem;
class TGraphErrors;
class TH1;
class TMultiGraph;
class TNtuple;
class TRootBrowser;
class TVirtualPad;

namespace Strangeness
{
struct Fitter;
struct Picker;
/**
 * This defines the interface for exercises
 *
 * @ingroup alice_masterclass_str_part2
 */
class Exercise : public Utility::Exercise
{
  TFile* fFile;
  Picker* fPicker{ nullptr };
  Fitter* fFitter{ nullptr };
  TGListTreeItem* fItem{ nullptr };
  TH1* fHist{ nullptr };
  TCanvas* fPlot{ nullptr };
  TCanvas* fStats{ nullptr };
  TCanvas* fProp{ nullptr };
  TF1* fFit{ nullptr };
  TF1* fBg{ nullptr };
  Double_t fTotal{ 0 };
  Double_t fTotalE{ 0 };
  Double_t fBack{ 0 };
  Double_t fBackE{ 0 };
  TMultiGraph* fRaw{ nullptr };
  TMultiGraph* fYield{ nullptr };
  TMultiGraph* fEnhancement{ nullptr };
  TMultiGraph* fMass;
  TMultiGraph* fWidth;
  Bool_t fAutoRange{ false };
  TRootBrowser* fBrowser{ nullptr };
  const TGUIEnglish& fTranslation;

 public:
  Exercise();

  /**
   * Set-up the elements needed by the exercise - e.g., GUI elements
   * or the like
   *
   * @param browser Browser implementation to use
   * @param cheat   True if cheats are on
   */
  void Setup(TRootBrowser* browser, Bool_t cheat) override;

  /**
   * Toggle cheats
   *
   * @param on if true, cheats are on
   */
  void ToggleCheat(Bool_t on) override { fAutoRange = on; }

  const TString& Instructions() const override { return fTranslation.FileEventDisplay(); }

  void HistSelect(TGListTreeItem* i, Int_t /*unused*/) { HistSelect(i); }
  /**
   * Called when we choose a new element in the tree
   *
   * @param i     Element
   */
  void HistSelect(TGListTreeItem* i);

  /**
   * Called when the user presses the fit button.
   *
   * Here, we fit our model (pol2+gausn) to our histogram, calculate
   * the integrals (with errors), and cache the relevant information.
   *
   * We also display the reduced chi-square
   *
   * Note, the information isn't propagated to anything until we hit
   * the "Accept" button.
   */
  void Fit();

  /**
   * Called when the user hit the "Accept" (or "Clear") button.
   *
   * We mark the histogram as done in the tree, and propagate values
   * of the signal to the relevant graph.  We can also update our
   * strangeness enhancement factor.
   */
  void Accept();

  /**
   * Get a graph corresponding to the given particle type. This makes
   * sure that the graph exists.
   *
   * @param m    Multigraph
   * @param type Particle type
   *
   * @return The graph of the given particle type
   */
  TGraphErrors* Graph(TMultiGraph* m, Int_t type);

  /**
   * Set point on graph in a multigraph
   *
   * @param m     Multigraph
   * @param type  Particle type
   * @param i     Point number
   * @param nPart Number of participants
   * @param y     Value
   * @param e     Uncertainty
   */
  void SetPoint(TMultiGraph* m, Int_t type, Int_t i, Double_t nPart, Double_t y, Double_t e);

  /**
   * Set a Point on the graph.  This makes sure that we have all
   * points leading up to this point.
   *
   * @param g     Graph
   * @param i     Point number
   * @param nPart Number of participants
   * @param y     Value
   * @param e     Uncertainty
   */
  void SetPoint(TGraphErrors* g, Int_t i, Double_t nPart, Double_t y, Double_t e);

  /**
   * Summarize the result of the fit into graphs.  We first clear the
   * corresponding point.  If the fit is being rejected, then we do
   * nothing more.  Otherwise we calculate the yield and strangeness
   * enhancement.
   *
   * @param type Type of particle
   * @param c1   Least centrality
   * @param c2   Largest centrality
   * @param s    Signal
   * @param es   Error on signal
   * @param m    Mean mass
   * @param em   Uncertainty on mass
   * @param w    Width
   * @param ew   Uncertainty on width
   */
  void Summarize(Int_t type, Int_t c1, Int_t c2, Double_t s, Double_t es, Double_t m, Double_t em,
                 Double_t w, Double_t ew);

  /**
   * Draw a multigraph in a sub-pad
   *
   * @param mg   Multigraph
   * @param p    Parent pad
   * @param sub  Sub-pad number
   * @param txt  Y-title
   */
  void DrawMG(TMultiGraph* mg, TVirtualPad* p, Int_t sub, const char* txt);

  /**
   * Get our centrality "bin" number
   *
   * @param c1
   * @param c2
   *
   * @return Bin number (0-9)
   */
  Int_t CentBin(Int_t c1, Int_t c2) const;

  /**
   * Get the number of participants per centrality bin
   *
   * @param bin Centrality bin
   *
   * @return The number of participants
   */
  Double_t NPart(Int_t bin) const;

  /**
   * Get the number of events for a given centrality bin
   *
   * @param bin Centrality bin
   *
   * @return Number of events
   */
  Int_t NEvents(Int_t bin) const;

  /**
   * Get the efficiency for a given centrality bin and species
   *
   * @param bin  Centrality bin
   * @param type Species
   *
   * @return Efficiency
   */
  Double_t Efficiency(Int_t bin, Int_t type) const;

  /**
   * Get the pp yield.  If we determined the value a, we use that
   * value.
   *
   * @param type
   * @param e
   *
   * @return
   */
  Double_t PPYield(Int_t type, Double_t& e);

  /**
   * Get an nTuple of results
   *
   * @return Pointer to nTuple
   */
  TNtuple* NTuple();

  Bool_t CanSave() const override { return true; }
  Bool_t CanPrint() const override { return true; }

  /**
   * Export to PDF
   */
  void PrintPdf(const char* out) override { fStats->SaveAs(out); }

  /// Export to ROOT
  void Export(const TString& fn) override;

  /**
   * Automatically run through all histograms
   */
  void Auto() override;

  TString DataDir() const override { return Utility::ResourcePath("data/Strangeness"); }
};
} // namespace Strangeness

#endif
