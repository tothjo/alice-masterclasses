/**
 * @file   StrangeFitter.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 15:29:38 2017
 *
 * @brief  A frame for setting fit ranges and limts
 *
 * @ingroup alice_masterclass_str_part2
 */
#ifndef STRANGEFITTER_C
#define STRANGEFITTER_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TH1.h>
#include <TRandom.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part2/Range.h"

class TBuffer;
class TClass;
class TGTextButton;
class TH1;
class TMemberInspector;

namespace Strangeness
{
struct Range;
struct TGUIEnglish;
class Exercise;

/**
 * The GUI element that allows us to change the fit range and limits.
 *
 * @ingroup alice_masterclass_str_part2
 */
struct Fitter : public TGMainFrame {
  const TGUIEnglish& fTranslation;
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fFit;
  TGTextButton* fAccept;
  Range* fSignal;
  Range* fBackground;

  /**
   * Constructor
   *
   * @param b  Our browser
   */
  Fitter(Exercise* b);

  /**
   * Notify of histogram change
   *
   * @param h New histogram or null
   * @param autoRange if true, automatically adjust fit range and limits
   */
  void SetHistogram(TH1* h, Bool_t autoRange = false);

  ClassDef(Fitter, 0);
};
} // namespace Strangeness

#endif
