/**
 * @file   Raa/scripts/Part2.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 18:49:26 2017
 *
 * @brief  Second part of the master class on Strangeness
 *
 * @ingroup alice_masterclass_str_part2
 */
#ifndef PART2_H_ZVUOBHBG
#define PART2_H_ZVUOBHBG

#include <RtypesCore.h>
#include <TString.h>

#include "Utility/AbstractMasterClassContent.h"
#include "Utility/Utilities.h"

namespace Strangeness
{
struct TPeakBackground : Utility::TAbstractExercise {
  TPeakBackground()
    : TAbstractExercise("peak background")
  {
  }
  void RunExercise(Bool_t AllowAuto = false) override;
};
} // namespace Strangeness

#endif /* end of include guard: PART2_H_ZVUOBHBG */
