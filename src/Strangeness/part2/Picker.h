/**
 * @file   StrangePicker.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 15:18:10 2017
 *
 * @brief  Pick histograms etc.
 *
 * @ingroup alice_masterclass_str_part2
 */
#ifndef STRANGEPICKER_C
#define STRANGEPICKER_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TClass.h>
#include <TFile.h>
#include <TGFrame.h>
#include <TGListTree.h>
#include <TGPicture.h>
#include <TH1.h>
#include <TKey.h>

#include "Strangeness/GUITranslation.h"
#include "Utility/Exercise.h"
#include "Utility/LogoButtons.h"

class TBuffer;
class TClass;
class TFile;
class TGListTree;
class TGListTreeItem;
class TGPicture;
class TMemberInspector;
class TObject;

namespace Utility
{
class Exercise;
} // namespace Utility

namespace Strangeness
{
struct TGUIEnglish;

/**
 * A GUI element that allows us to pick a histogram from a tree
 *
 * @ingroup alice_masterclass_str_part2
 */
struct Picker : public TGMainFrame {
  TGListTree* fList;
  TGListTreeItem* fTop;
  const TGPicture* fPic;
  TGUIEnglish& fTranslation;
  /**
   * Constructor
   *
   * @param file  File to get histograms from
   * @param e     The exercise
   * @param cheat If cheats are on
   */
  Picker(TFile* file, Utility::Exercise* e, Bool_t cheat);

  /**
   * We undo the effect of the user clicking the check marks
   *
   * @param user   User data
   */
  void UndoCheck(TObject* user, Bool_t /*unused*/);
  ClassDef(Picker, 0);
};
} // namespace Strangeness

#endif
