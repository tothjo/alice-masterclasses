#ifdef __CLING__
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version. (See cxx source for full Copyright notice)
 */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ struct Strangeness::TGUIEnglish+;
#pragma link C++ struct Strangeness::TGUIGerman+;
#pragma link C++ struct Strangeness::ClassLanguage+;

#pragma link C++ class Strangeness::Dataset+;
#pragma link C++ struct Strangeness::Obs+;

#pragma link C++ class Strangeness::Particle+;
#pragma link C++ class Strangeness::Calculator+;
#pragma link C++ class Strangeness::EventDisplay+;
#pragma link C++ class Strangeness::Exercise+;
#pragma link C++ struct Strangeness::Fitter+;
#pragma link C++ struct Strangeness::Histograms+;
#pragma link C++ struct Strangeness::Picker+;
#pragma link C++ struct Strangeness::Range+;

#pragma link C++ class Strangeness::TNavigation+;
#pragma link C++ struct Strangeness::internal::TDisplay+;
#pragma link C++ struct Strangeness::internal::THelp+;

#endif
