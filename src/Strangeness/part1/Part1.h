/**
 * @file   Part1.C
 * @file   Part1.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:37:21 2017
 *
 * @brief  Run the first exercise
 *
 *
 * @ingroup alice_masterclass_str_part1
 *
 */
/**
 * @defgroup alice_masterclass_str_part1 1. Exercise in strangeness class
 * @ingroup alice_masterclass_str
 */
#ifndef PART1_H_RMGPSXGJ
#define PART1_H_RMGPSXGJ

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>
#include <fstream>
#include <iostream>

#include "Strangeness/GUITranslation.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/Utilities.h"

class TBuffer;
class TClass;
class TGTextButton;
class TMemberInspector;

namespace Strangeness
{
struct TGUIEnglish;
} // namespace Strangeness

namespace Strangeness
{
/** Data set selector
 * @image html Strangeness/doc/Part1Selector.png
 * @ingroup alice_masterclass_str_part1
 */
class Dataset : public TGMainFrame
{
 private:
  TGTextButton* fStart{ nullptr };
  Int_t fDataset{ 0 };
  Bool_t fAllowAuto;
  TGUIEnglish& fTranslation;

 public:
  Dataset(Bool_t allowAuto = false);
  ~Dataset() override;
  void Choice(Int_t id);
  void Code(const TString& data, const TString& geom, Bool_t cheat, Bool_t demo);
  void Start();

  ClassDefOverride(Dataset, 0)
};

struct TInspectPP : Utility::TAbstractExercise {
  TInspectPP()
    : TAbstractExercise("inspect p-p")
  {
  }
  void RunExercise(Bool_t AllowAuto = false) override;
};
} // namespace Strangeness

#endif /* end of include guard: PART1_H_RMGPSXGJ */
