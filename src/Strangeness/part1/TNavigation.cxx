#include "TNavigation.h"

#include <TGButton.h>
#include <TGClient.h>
#include <TGLayout.h>
#include <TString.h>
#include <vector>

#include "Utility/Info.h"

namespace Utility
{
class EventDisplay;
class TEventAnalyseGUI;
class VSDReader;
} // namespace Utility

namespace Strangeness
{
namespace internal
{
TDisplay::TDisplay(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Display(), kVerticalFrame)
  , fButtonTracks(new TGTextButton(this, Translation.Tracks()))
  , fFrameStrangeParticles(new TGHorizontalFrame(this))
  , fButtonV0(new TGTextButton(fFrameStrangeParticles, "V0s"))
  , fButtonCascade(new TGTextButton(fFrameStrangeParticles, "Cascades"))
  , fFrameGeometry(new TGHorizontalFrame(this))
  , fToggleDetector(new TGTextButton(fFrameGeometry, Translation.ButtonGeometry()))
  , fButtonAxes(new TGTextButton(fFrameGeometry, Translation.ButtonAxes()))
  , fButtonChangeBackground(new TGTextButton(this, Translation.ButtonBackground()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);

  Utility::SetupNavigationButton(fButtonTracks, Translation.TipToggleTracks(), /*Toggle=*/true);
  this->AddFrame(fButtonTracks, HintExpandX);

  Utility::SetupNavigationButton(fButtonV0, Translation.TipToggleV0(), /*Toggle=*/true);
  fFrameStrangeParticles->AddFrame(fButtonV0, HintExpandX);

  Utility::SetupNavigationButton(fButtonCascade, Translation.TipToggleCascades(), /*Toggle=*/true);
  fFrameStrangeParticles->AddFrame(fButtonCascade, HintExpandX);
  this->AddFrame(fFrameStrangeParticles, HintExpandX);

  Utility::SetupNavigationButton(fToggleDetector, Translation.TipToggleDetectorVolumes(),
                                 /*Toggle=*/true);
  fFrameGeometry->AddFrame(fToggleDetector, HintExpandX);

  Utility::SetupNavigationButton(fButtonAxes, Translation.TipToggleAxes());
  fFrameGeometry->AddFrame(fButtonAxes, HintExpandX);

  this->AddFrame(fFrameGeometry, HintExpandX);

  // --- Background
  Utility::SetupNavigationButton(fButtonChangeBackground, Translation.TipToggleBackground());
  this->AddFrame(fButtonChangeBackground, HintExpandX);
}

THelp::THelp(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Encyclopaedia(), kVerticalFrame)
  , fButtonAliceInfo(new TGTextButton(this, "ALICE"))
  , fButtonDecayPatterns(new TGTextButton(this, Translation.ButtonDecayPatterns()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);
  fButtonAliceInfo->SetToolTipText(Translation.TipShowInformation());
  this->AddFrame(fButtonAliceInfo, HintExpandX);

  fButtonDecayPatterns->SetToolTipText(Translation.TipShowDecays());
  this->AddFrame(fButtonDecayPatterns, HintExpandX);
}

} // namespace internal

void TNavigation::InitialLoad() {
  fDisplayStrangeFrame->fButtonTracks->Toggle(kTRUE);
  fDisplayStrangeFrame->fToggleDetector->Toggle(kTRUE);
}

TNavigation::TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat)
  : Utility::INavigation(Exercise, Cheat)
  , fDisplayStrangeFrame(new internal::TDisplay(this, fTranslation))
  , fHelpStrange(new internal::THelp(this, fTranslation))
{
  auto* LayoutExpandX = new TGLayoutHints(kLHintsExpandX);
  this->AddFrame(fDisplayStrangeFrame, LayoutExpandX);

  fHelpStrange->fButtonAliceInfo->Connect("Clicked()", "Utility::INavigation", this,
                                          "DetectorInfo()");
  fHelpStrange->fButtonDecayPatterns->Connect("Clicked()", "Strangeness::TNavigation", this,
                                              "PatternInfo()");
  this->AddFrame(fHelpStrange, LayoutExpandX);
  Resize();
}

void TNavigation::SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI,
                                   Utility::VSDReader* Reader)
{
  INavigation::SetupSignalSlots(AnalysisGUI, Reader);

  fDisplayStrangeFrame->fButtonAxes->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                             "ToggleRenderable(=Utility::ERenderables::kAxes)");
  fDisplayStrangeFrame->fButtonTracks->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                               AnalysisGUI,
                                               "ToggleRenderable(=Utility::ERenderables::kTracks)");
  fDisplayStrangeFrame->fButtonV0->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                           "ToggleRenderable(=Utility::ERenderables::kV0s)");
  fDisplayStrangeFrame->fButtonCascade->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kCascades)");
  fDisplayStrangeFrame->fToggleDetector->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kGeometry)");
  fDisplayStrangeFrame->fButtonChangeBackground->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                         AnalysisGUI, "ChangeBackgroundColor()");

  InitialLoad();
}

/// Show the decay pattern information
void TNavigation::PatternInfo()
{
  std::vector<TString> names = { "kaon", "lambda", "antilambda", "xi" };
  new Utility::InfoBox("Decay patterns", names, gClient->GetRoot(), 100, 100);
}
} // namespace Strangeness
