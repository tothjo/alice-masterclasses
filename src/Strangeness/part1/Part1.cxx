#include "Part1.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TEveManager.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <memory>
#include <utility>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/EventDisplay.h"
#include "Strangeness/part1/TNavigation.h"
#include "Utility/EventDisplay.h"
#include "Utility/INavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

class TGPicture;

namespace Strangeness
{
/** Data set selector
 * @image html Strangeness/doc/Part1Selector.png
 * @ingroup alice_masterclass_str_part1
 */
Dataset::Dataset(Bool_t allowAuto)
  : TGMainFrame(nullptr, 0, 0)
  , fAllowAuto(allowAuto)
  , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2);
  const TGPicture* al = Utility::Logo();
  AddFrame(new TGPictureButton(this, al), lh);

  auto* htmlView = new TGHtml(this, 450, 400);
  TString txt = fTranslation.DocDataSet();
  htmlView->ParseText(const_cast<char*>(txt.Data()));
  AddFrame(htmlView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2));

  auto* cb = new TGComboBox(this);
  cb->AddEntry(fTranslation.SelectDataSet(), 0);
  cb->AddEntry(fTranslation.DemoSet(), 1);

  for (Int_t i = 1; i <= 21; i++) {
    cb->AddEntry(Form("%s %2d", fTranslation.DataSet().Data(), i), i + 1);
  }

  cb->Resize(100, 20);
  cb->Select(0, kFALSE);
  cb->Connect("Selected(Int_t)", "Strangeness::Dataset", this, "Choice(Int_t)");
  AddFrame(cb, lh);

  fStart = new TGTextButton(this, fTranslation.Start());
  fStart->SetEnabled(false);
  AddFrame(fStart, lh);
  fStart->Connect("Clicked()", "Strangeness::Dataset", this, "Start()");

  auto* tb = new TGTextButton(this, fTranslation.Exit());
  AddFrame(tb, lh);
  tb->Connect("Clicked()", "TApplication", gApplication, "Terminate()");

  SetWindowName("MasterClass");
  MapSubwindows();

  Resize(GetDefaultSize());

  MapWindow();
}

Dataset::~Dataset() { Cleanup(); }

void Dataset::Choice(Int_t id)
{
  fDataset = id - 1;
  fStart->SetEnabled(fDataset != -1);
}

void Dataset::Code(const TString& data, const TString& geom, Bool_t cheat, Bool_t demo)
{
  // auto* r = new Utility::VSDReader(geom.Data());
  auto* r = new Utility::TEventAnalyseGUI(geom);
  std::unique_ptr<Utility::EventDisplay> Exercise = std_fix::make_unique<EventDisplay>(demo);
  std::unique_ptr<Utility::INavigation> Navigation =
    std_fix::make_unique<TNavigation>(Exercise.get(), cheat);
  r->Setup(std::move(Navigation), std::move(Exercise), "Strangeness", data, cheat);
  r->SetupSignalSlots();

  if (cheat)
    r->Auto();
}

void Dataset::Start()
{
  if (fDataset == -1) {
    new TGMsgBox(gClient->GetRoot(), this, fTranslation.DiagInvalidInputTitle(),
                 fTranslation.DiagInvalidInputText());
    return;
  }
  Utility::SetupStyleForClass();

  TString df;
  Int_t no = fDataset;
  if (no > 0) {
    df.Form("AliVSD_MasterClass_%d.root", no);
  } else {
    df = "AliVSD_example.root";
  }
  TString gf = Utility::ResourcePath("data/Utility/geometry.root");
  Code(df, gf, fAllowAuto, no == 0);
  UnmapWindow();
}

void TInspectPP::RunExercise(Bool_t AllowAuto) { new Dataset(AllowAuto); }

} // namespace Strangeness
