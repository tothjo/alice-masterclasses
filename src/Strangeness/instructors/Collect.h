/**
 * @file   StrangeCollect.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:39:58 2017
 *
 * @brief  Collect results from the students
 *
 *
 * @ingroup alice_masterclass_str_part1
 *
 */
#ifndef STRANGECOLLECT_H_P67MFBVU
#define STRANGECOLLECT_H_P67MFBVU

#include <RtypesCore.h>
#include <TClass.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TH1.h>
#include <TRootCanvas.h>
#include <TString.h>

#include "Utility/AbstractMasterClassContent.h"

class TCanvas;
class TDirectory;
class TFile;
class TGWindow;
class TH1;

namespace Strangeness
{
/**
 * @{
 * @name Collect results from the students
 *
 * @ingroup alice_masterclass_str_part1
 */
const TGWindow* CollectWindow(TCanvas* c);

/**
 * Prompt for a new file
 *
 * @return Pointer to file
 */
TFile* CollectOpen(TCanvas* c);

/**
 * Get a histogram
 *
 * @param d     Directory
 * @param name  Name of histogram
 *
 * @return Pointer to histogram or null
 */
TH1* GetH1(TDirectory* d, const char* name);

/**
 * Add a histogram to a histogram
 *
 * @param sum Histogram to add to
 * @param h   Histram to add
 */
TH1* CollectAdd(TH1* sum, TH1* h);

/**
 * Check if we need more data
 *
 *
 * @return
 */
Bool_t CollectMore(TCanvas* c);

struct TCollectResults : Utility::TAbstractExercise {
  TCollectResults()
    : TAbstractExercise("collect results")
  {
  }

  void RunExercise(Bool_t cheat = false) override;
};

} // namespace Strangeness
/*
 * @}
 */

#endif /* end of include guard: STRANGECOLLECT_H_P67MFBVU */
