#include "AbstractMasterClassContent.h"

#include <TString.h>
#include <iostream>
#include <utility>

#include "Utility/ContentTranslation.h"
#include "Utility/LanguageProvider.h"

namespace Utility
{
TAbstractExercise::TAbstractExercise(TString ExerciseName)
  : fExerciseName(std::move(ExerciseName))
{
}

TAbstractMasterClassContent::TAbstractMasterClassContent(std::unique_ptr<TContentTranslation> lang)
  : fTranslation(std::move(lang))
{
}

void TAbstractMasterClassContent::ChangeLanguage(std::unique_ptr<TContentTranslation> lang)
{
  fTranslation = std::move(lang);
}

/// Return a translated list of all exercises to display in the GUI.
std::vector<TString> TAbstractMasterClassContent::GetExerciseNames() const
{
  std::cerr << "Translate exercises\n";

  std::vector<TString> Result;
  for (auto& Exercise : fExercises) {
    std::cerr << "Exec-Name: " << Exercise->GetName() << "\n";
    std::cerr << "Translation: " << fTranslation->Exercise(Exercise->GetName()) << "\n";
    std::cerr << "Language: " << GetLanguageName(fTranslation->GetLanguage()) << "\n";

    Result.emplace_back(fTranslation->Exercise(Exercise->GetName()));
  }
  return Result;
}

void TAbstractMasterClassContent::AddExercise(std::unique_ptr<TAbstractExercise> Exercise)
{
  fExercises.emplace_back(std::move(Exercise));
}

} // namespace Utility
