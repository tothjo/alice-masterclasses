#include "EventDisplay.h"

#include <RtypesCore.h>
#include <TError.h>
#include <TEveElement.h>
#include <TEveTrack.h>
#include <TEveVector.h>
#include <TString.h>
#include <cstdio>
#include <list>

namespace Utility
{
void EventDisplay::FixRender(TEveElement* el, Bool_t visSelf, Int_t opacity)
{
  // Printf("Setting rendering of %p - %s to visself=%d opacity=%d",
  //        el, el->GetElementName(), visSelf, opacity);
  el->SetRnrSelf(visSelf);
  el->SetMainTransparency(opacity);
}

/// FIXME The approach is in principle super error prone. 
/// The assumption about the Elements seems weird. (Detector Geometry is
/// passed in)
void EventDisplay::FixGeometry(gsl::not_null<TEveElement*> el)
{
  // FIXME I think static is incorrect here and might be a reason for bugs?
  // FIXME This is not respected in the subclassed EventDisplays. BUG
  static Bool_t done = false;
  if (done) {
    return;
  }

  // The top
  FixRender(el, false, 0);
  // ITS
  auto i = el->BeginChildren();
  // TPC
  i++;
  // TRD+TOF
  i++;
  FixRender(*i, false, 0);
  auto j = (*i)->BeginChildren();
  FixRender(*j++); // BREF_1
  FixRender(*j++); // B076_1
  j++;
  j++;
  j++;
  j++;
  FixRender(*j++); // BSEGMO17_1
  FixRender(*j++); // BSEGMO0_1
  FixRender(*j++); // BSEGMO1_1
  j++;
  j++;
  j++;
  j++;
  j++;             // Skip modules 2 to 6
  FixRender(*j++); // BSEGMO7_1
  FixRender(*j++); // BSEGMO8_1
  FixRender(*j++); // BSEGMO9_1
  FixRender(*j++); // BSEGM10_1
  done = true;
}

void EventDisplay::PrintElement(TEveElement* el, Int_t lvl, Int_t max)
{
  if (max > 0 && lvl > max) {
    return;
  }
  printf("%2d ", lvl);
  if (el == nullptr) {
    Warning("PrintElement", "No element at level %d, %p", lvl, static_cast<void*>(el));
    return;
  }
  Printf("%*s%s - %d children", lvl, "", el->GetElementName(), el->NumChildren());
  lvl++;
  auto i = el->BeginChildren();
  while (i != el->EndChildren()) {
    PrintElement(*i, lvl, max);
    i++;
  }
}


} // namespace Utility
