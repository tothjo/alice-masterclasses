#ifndef ERENDERABLES_H_2HBZRN1A
#define ERENDERABLES_H_2HBZRN1A

#include <cstdint>
#include <gsl/gsl>

namespace Utility
{
enum class ERenderables : uint16_t {
  kIntersectionPoint = 1 << 0,
  kAxes = 1 << 1,
  kClusters = 1 << 2,
  kTracks = 1 << 3,
  kPrimaries = 1 << 4,
  kV0s = 1 << 5,
  kCascades = 1 << 6,
  kGeometry = 1 << 7,
  kAll =
    (kIntersectionPoint | kAxes | kClusters | kTracks | kV0s | kCascades | kPrimaries | kGeometry)
};

inline ERenderables& operator^=(ERenderables& lhs, const ERenderables rhs)
{
  lhs = static_cast<ERenderables>(static_cast<uint16_t>(lhs) ^ static_cast<uint16_t>(rhs));
  return lhs;
}

inline ERenderables operator|(const ERenderables lhs, const ERenderables rhs)
{
  return static_cast<ERenderables>(static_cast<uint16_t>(lhs) | static_cast<uint16_t>(rhs));
}

inline ERenderables operator&(const ERenderables lhs, const ERenderables rhs)
{
  return static_cast<ERenderables>(static_cast<uint16_t>(lhs) & static_cast<uint16_t>(rhs));
}

inline bool HasActivated(const ERenderables State, const ERenderables Bit)
{
  Expects(__builtin_popcount(static_cast<uint16_t>(Bit)) == 1);
  return (static_cast<uint16_t>(State) & static_cast<uint16_t>(Bit)) != 0u;
}

} // namespace Utility

#endif /* end of include guard: ERENDERABLES_H_2HBZRN1A */
