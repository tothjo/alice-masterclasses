#include "LogoButtons.h"

#include <GuiTypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFileDialog.h>
#include <TGFont.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TString.h>

#include "Utility/Exercise.h"
#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

namespace Utility
{
LogoButtons::LogoButtons(TGCompositeFrame* p, Exercise* e, Bool_t cheats)
  : TGVerticalFrame(p)
  , fExercise(e)
  , fLogo(nullptr)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
{
  // --- Some styling stuff ----------------------------------------
  gClient->GetColorByName("red", fHighlight);
  const TGFont* font = gClient->GetFont("-*-times-bold-r-*-*-16-*-*-*-*-*-*-*");
  FontStruct_t buttonFont = font->GetFontStruct();

  // --- Logo (doubling as cheat button) ---------------------------
  fLogo = new TGPictureButton(this, Logo());
  AddFrame(fLogo, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  if (e->CanCheat()) {
    fLogo->Connect("Clicked()", "Utility::LogoButtons", this, "ToggleCheat()");
    fLogo->AllowStayDown(true);
    fLogo->SetDown(cheats);

    ULong_t logoColor = TGFrame::GetDefaultFrameBackground();
    if (fLogo->IsDown()) {
      logoColor = fHighlight;
    }
    fLogo->ChangeBackground(logoColor);
  }

  // --- Print -----------------------------------------------------
  if (e->CanPrint()) {
    auto* pr = new TGTextButton(this, fTranslation.ButtonPrint());
    pr->SetToolTipText(fTranslation.TipPrint());
    pr->Connect("Clicked()", "Utility::LogoButtons", this, "PrintPdf()");
    this->AddFrame(pr, new TGLayoutHints(kLHintsExpandX));
  }

  // --- Save ------------------------------------------------------
  if (e->CanSave()) {
    auto* sv = new TGTextButton(this, fTranslation.ButtonExport());
    sv->SetToolTipText(fTranslation.TipExport());
    sv->Connect("Clicked()", "Utility::LogoButtons", this, "Export()");
    this->AddFrame(sv, new TGLayoutHints(kLHintsExpandX));
  }

  // --- Exit button -----------------------------------------------
  auto* ex = new TGTextButton(this, fTranslation.ButtonExit());
  ex->SetToolTipText(fTranslation.TipExit());
  ex->SetTextColor(fHighlight);
  ex->SetFont(buttonFont);
  ex->Connect("Clicked()", "Utility::Exercise", e, "Exit()");
  AddFrame(ex, new TGLayoutHints(kLHintsExpandX));

  // Add to the parent
  p->AddFrame(this, new TGLayoutHints(kLHintsExpandX));
}

/**
 * Toggle cheats enabled.  Calls Exercise::ToggleCheat(Bool_t)
 * with an argument of either true if the cheats are enabled, or
 * false if not.
 */
void LogoButtons::ToggleCheat()
{
  ULong_t logoColor = TGFrame::GetDefaultFrameBackground();
  if (fLogo->IsDown()) {
    logoColor = fHighlight;
  }
  fLogo->ChangeBackground(logoColor);
  fExercise->ToggleCheat(fLogo->IsDown());
  // gROOT->ProcessLine(Form("((Exercise*)%p)->ToggleCheat(%d)",
  // 			    fExercise, fLogo->IsDown()));
}

/**
 * @{
 * @name Export functions
 */

/**
 * Pop-up a dialog to select the output file
 *
 * @param types Types to enable in the selection
 * @param out   On return, the selected output name
 *
 * @return true if a file was selected
 */
Bool_t LogoButtons::SelectOutput(const char** types, TString& out)
{
  out = "";
  TGFileInfo info;
  info.fFileTypes = types;
  info.fMultipleSelection = false;
  new TGFileDialog(gClient->GetRoot(), this, kFDSave, &info);

  if (info.fFilename == nullptr) {
    return false;
  }

  // TODO Super clever, refactor
  out = info.fFilename;
  Int_t ei = info.fFileTypeIdx;
  const char* ext = &(types[ei + 1][1]);
  if (!out.EndsWith(ext)) {
    out.Append(ext);
  }

  return true;
}

void LogoButtons::PrintPdf()
{
  const char* types[] = { "PDF File", "*.pdf", nullptr, nullptr };
  TString out;
  if (!SelectOutput(types, out)) {
    return;
  }
  fExercise->PrintPdf(out);
}

void LogoButtons::Export()
{
  const char* types[] = { "ROOT File", "*.root", nullptr, nullptr };
  TString out;
  if (!SelectOutput(types, out)) {
    return;
  }
  fExercise->Export(out);
}
} // namespace Utility
