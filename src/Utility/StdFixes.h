#ifndef MAKEUNIQUE_H_YMWAH81R
#define MAKEUNIQUE_H_YMWAH81R

#include <memory>
#include <type_traits>

namespace std_fix
{
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
} // namespace std_fix

#endif /* end of include guard: MAKEUNIQUE_H_YMWAH81R */
