#include "Exercise.h"

#include "Utility/Utilities.h"

class TFile;

namespace Utility
{
TFile* Exercise::DataFile(const char* filename, const char* dir) const
{
  TString d(dir);
  return File((dir != nullptr ? d : DataDir()), filename);
}

} // namespace Utility
