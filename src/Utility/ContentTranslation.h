#ifndef MASTERCLASSCONTENTLANGUAGE_C_XNAO0PB7
#define MASTERCLASSCONTENTLANGUAGE_C_XNAO0PB7

#include <TString.h>
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep

namespace Utility
{
class TContentTranslation : public TLanguageProvider
{
 public:
  TContentTranslation(ESupportedLanguages lang)
    : TLanguageProvider(lang)
  {
  }

  LANG_KEY(Name)
  LANG_KEY(Description)

  TString Exercise(const TString& ExerciseKey) const { return GetText(ExerciseKey); }
};
} // namespace Utility

#endif /* end of include guard: MASTERCLASSCONTENTLANGUAGE_C_XNAO0PB7 */
