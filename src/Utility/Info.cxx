#include "Info.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TString.h>
#include <iostream>

#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

class TGPicture;
class TGWindow;

namespace Utility
{
namespace
{
const TGPicture* GetPicture(const TString& pic)
{
  TString Tmp = Form("%s_small.png", pic.Data());
  return Picture(Tmp);
}
} // namespace
/**
 * Constructor
 *
 * @param title  The title to show
 * @param names  Null terminated list of names to show
 * @param p      Parent
 * @param w      Width
 * @param h      Height
 */
InfoBox::InfoBox(const TString& title, const std::vector<TString>& names, const TGWindow* p,
                 UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h)
  , fDisplay(nullptr)
  , fWhat(names[0])
  , fTitle(title)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
{
  DontCallClose();

  auto* gf = new TGGroupFrame(this, title);
  auto* hf = new TGHorizontalFrame(gf, 250, 250);

  fDisplay = new TGPictureButton(hf, GetPicture(fWhat));
  fDisplay->Connect("Clicked()", "Utility::InfoBox", this, "MakeBigger()");

  hf->AddFrame(fDisplay, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  gf->AddFrame(hf);

  hf = new TGHorizontalFrame(gf);

  for (const auto& name : names) {
    auto* b = new TGTextButton(hf, name);
    const char* SlotCall = Form("Show(=\"%s\")", name.Data());
    std::cerr << "Creating slot for pictures: " << SlotCall << "\n";
    b->Connect("Clicked()", "Utility::InfoBox", this, SlotCall);
    hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  }

  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));

  hf = new TGHorizontalFrame(gf);
  auto* b = new TGTextButton(hf, fTranslation.ButtonClose());
  b->Connect("Clicked()", "Utility::InfoBox", this, "UnmapWindow()");
  hf->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));

  AddFrame(gf);

  SetWindowName(title);
  MapSubwindows();

  // Initialize the layout algorithm via Resize()
  Resize(GetDefaultSize());

  // Map main frame
  MapWindow();
}

void InfoBox::Show(const char* pic)
{
  std::cerr << "Pic path begin: " << pic << "\n";
  fWhat = pic;
  std::cerr << "Pic path now: " << pic << "\n";
  TString tmp = pic;
  tmp.ReplaceAll("+", "");
  std::cerr << "Pic path after replacement: " << tmp << "\n";
  fDisplay->SetPicture(GetPicture(tmp));
  gClient->NeedRedraw(this);
}
} // namespace Utility
