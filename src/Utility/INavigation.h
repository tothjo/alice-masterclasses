/**
 * @file   Navigation.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 09:46:40 2017
 *
 * @brief  Event, etc. navigation
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
#ifndef ALICENAVIGATION_C
#define ALICENAVIGATION_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TString.h>
#include <utility>
#include <vector>

#include "Utility/EventDisplay.h"
#include "Utility/GUITranslation.h"
#include "Utility/Info.h"
#include "Utility/Instructions.h"
#include "Utility/LogoButtons.h"
#include "Utility/NavigationElements.h"
#include "Utility/Utilities.h"

class TBuffer;
class TClass;
class TGButton;
class TMemberInspector;

namespace Utility
{
class EventDisplay;
class LogoButtons;
struct TGUIEnglish;
} // namespace Utility

namespace Utility
{
class TEventAnalyseGUI;
class TEventVisualization;
/// Forward declaration because of circular dependency.
/// FIXME Remove the VSDReader connections
class VSDReader;

//====================================================================
/**
 * Event and display navigation, encyclopidia, printing, saving, and
 * ending the application.
 *
 * @ingroup alice_masterclass_base_eventdisplay
 */
class INavigation : public TGCompositeFrame
{
 protected:
  // Access to the TaskButtons must be allowed for now.
  friend class TEventAnalyseGUI;
  const TGUIEnglish& fTranslation;

 private:
  // Buttons to allow cheating
  LogoButtons* fLogoButton;

  // Button to show Instructions.
  internal::TInstructionsFrame* fInstructionButtonFrame;

  // All GUI Element that make up the Events control, like event switching
  // and displaying the current state.
  internal::TEventsFrame* fEventsFrame;

 protected:
  /// Trigger all display buttons to load elements from the beginning on.
  virtual void InitialLoad() = 0;

 public:
  /**
   * Constructor
   *
   * @param p         Parent window
   * @param IsRaa     Flag to control creation of Navigation elements depending on Class
   * @param exercise  Pointer to exercise
   * @param cheat     True if cheats are already on
   */
  INavigation(EventDisplay* exercise, Bool_t cheat);
  ~INavigation() override { Cleanup(); }

  /// @{
  /// @name Configuration to connect to the other components

  // Signal Slots extracted from Constructor.
  virtual void SetupSignalSlots(TEventAnalyseGUI* AnalysisGUI, VSDReader* Reader);
  /// @}

  /// @{
  /// @name Signals and Slots that control the event to analyse.

  /// Signals that are emitted for navigation purposes.
  void NextEvent();     //*SIGNAL*
  void PreviousEvent(); //*SIGNAL*

  /// Slots that are attached to the navigation buttons, create interface to GUI
  /// with propagating the button presses.
  void SignalNextEvent() { Emit("NextEvent()"); }
  void SignalPreviousEvent() { Emit("PreviousEvent()"); }

  /** Set the current event number
   * @param num Number of event
   * @param max Maximum number of event
   */
  virtual void SetEventNumber(Int_t Num, Int_t Max);

  /** Set the current event type
   * @param type The type
   */
  void SetEventType(const TString& type) { fEventsFrame->fLabelDataset->SetText(type); }

  /** Set the number of events analysed
   * @param count The count
   */
  void SetEventsAnalysed(Int_t count);
  /// @}

  /// @{
  /// @name Slots that display additional information

  /// Show the detector information.
  void DetectorInfo();
  /// @}

  ClassDefOverride(INavigation, 0);
};

/// General helper function to setup the navigation buttons.
void SetupNavigationButton(TGButton* Button, const TString& ToolTip, Bool_t Toggle = false);
} // namespace Utility

#endif
