#include "Details.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TString.h>

#include "Utility/Utilities.h"

class TGWindow;

namespace Utility
{
Details::Details(TString what, const TString& title, const TGWindow* p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h)
{
  DontCallClose();

  // Special case
  if (what.EqualTo("TRD+TOF")) {
    what = "TRDTOF";
  }

  auto* gf = new TGGroupFrame(this, title);
  auto* hf = new TGHorizontalFrame(gf, 250, 250);
  auto* pic = Picture(Form("%s.png", what.Data()));
  auto* b1 = new TGPictureButton(hf, pic);
  SetWindowName(Form("%s - %s", title.Data(), what.Data()));

  b1->Connect("Clicked()", "Utility::Details", this, "UnmapWindow()");

  hf->AddFrame(b1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  gf->AddFrame(hf);

  AddFrame(gf);
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

} // namespace Utility
