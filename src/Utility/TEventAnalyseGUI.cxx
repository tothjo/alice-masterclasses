#include "TEventAnalyseGUI.h"

#include <RQ_OBJECT.h>
#include <TEveBrowser.h>
#include <TEveEventManager.h>
#include <TEveGeoShape.h>
#include <TEveManager.h>
#include <TEveSelection.h>
#include <TEveWindowManager.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLRnrCtx.h>
#include <TGLViewer.h>
#include <TGMsgBox.h>
#include <TGTab.h>
#include <TRootBrowser.h>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "Raa/part1/TNavigation.h"
#include "Utility/GUITranslation.h"
#include "Utility/Instructions.h"
#include "Utility/LanguageProvider.h"

// Bad coupling. To connect the Signal/Slots that are triggered
// while loading data in VSDReader the current form requires a dynamic_cast
// to every possible masterclass. The knowledge what to connect to what
// is encoded here (SetupSignalSlots).
// The necessary Refactoring is splitting up VSDReader in TrackLoader, V0Reader
// ... and then inject that into the GUI (similar as the Navigation).
#include "Raa/part1/EventDisplay.h"
#include "Strangeness/part1/EventDisplay.h"

class TEveElement;

namespace Utility
{
TEventAnalyseGUI::TEventAnalyseGUI(const TString& geomFileName)
  : fEveManager(TEveManager::Create(kTRUE, "FV"))
  , fEveBrowser(fEveManager->GetBrowser())
  , fEveEventManager(new TEveEventManager("Event", "ALICE VSD Event"))
  , fActiveRenderables(static_cast<Utility::ERenderables>(0))
  , fEventVisualization(fEveManager, fEveEventManager, fVSDData.GetVSD())
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
  , fCurrentEventIdx(0)
  , fTotalEventCount(-1)
  , fFinishedEvents(0)
{
  ConnectDataAndRenderables(fVSDData, fEventVisualization);
  fEveManager->Redraw3D(kTRUE);
  fEventVisualization.LoadDetectorGeometry(geomFileName);

  // --- Change the backround and use outlines ---------------------
  std::cerr << "Changing BG\n";
  ChangeBackgroundColor();
  fEveManager->GetDefaultGLViewer()->SetStyle(TGLRnrCtx::kOutline);

  std::cerr << "EveEventManager creation\n";

  // --- Add our event ---------------------------------------------
  fEveManager->AddEvent(fEveEventManager);

  std::cerr << "Creating the Eve Browser\n";
  // Get the fEveBrowser and selection tool, and connect to handler
  fEveBrowser->GetMainFrame()->Resize(1100, 870);

  std::cerr << "Remove some fEveBrowser tabs\n";
  // Remove unwanted tabs
  TGTab* right = fEveBrowser->GetTabRight();
  TGTab* left = fEveBrowser->GetTabLeft();
  TGTab* bottom = fEveBrowser->GetTabBottom();
  if (right->GetTabTab(3) != nullptr) {
    right->RemoveTab(3);
  }
  if (right->GetTabTab(2) != nullptr) {
    right->RemoveTab(2);
  }
  if (left->GetTabTab(1) != nullptr) {
    left->RemoveTab(1);
  }
  if (left->GetTabTab(0) != nullptr) {
    left->RemoveTab(0);
  }
  if (bottom->GetTabTab(0) != nullptr) {
    bottom->RemoveTab(0);
  }
}

void TEventAnalyseGUI::Setup(std::unique_ptr<INavigation> Navigation,
                             std::unique_ptr<EventDisplay> exercise, const TString& title,
                             const TString& dataFileName, Bool_t cheat)
{
  fExercise = std::move(exercise);
  fNavigation = std::move(Navigation);

  fEveBrowser->GetMainFrame()->SetWindowName(Form("ALICE MasterClass - %s", title.Data()));

  std::cerr << "Load exercise\n";
  // Create exercise interface
  fExercise->Setup(fEveBrowser, cheat);

  // Create navigation interface
  // This part is a little bit non-obvious and I did not understand all of it ;)
  // 1. Somehow makes the left Tab "active"
  // 2. Ensure, that the input navigation bar is not a standalone window,
  //    but will live inside the navigation tab.
  // 3. Add the Tab, and remove a prior existing one.
  // 4. Make the windows visible.
  std::cerr << "Add Navigation\n";
  fEveBrowser->StartEmbedding(TRootBrowser::kLeft);
  gsl::not_null<TGTab*> LeftTab = fEveBrowser->GetTabLeft();
  fNavigation->ReparentWindow(LeftTab);
  LeftTab->AddTab("Navigation", fNavigation.get());
  LeftTab->RemoveTab(0);
  LeftTab->MapSubwindows();
  fEveBrowser->StopEmbedding(fTranslation.Navigation());

  OpenInstructions();

  // Initialize the VSDData
  std::cerr << "Load data\n";
  // TODO Extract VSDData too and inject it like Exercise
  fVSDData.Setup(fExercise->DataFile(dataFileName));
  fTotalEventCount = fVSDData.GetNumberOfEvents();
  if (fTotalEventCount <= 0)
    throw std::runtime_error("Could not load any events!");

  // Select the student tabtDataAndRenderables(fVSDData, fEventVisualization);
  std::cerr << "Select tab\n";
  fEveBrowser->SetTab(TRootBrowser::kLeft, 0);
  fEveBrowser->GetTabRight()->SetTab(1);

  // Do not show more
  std::cerr << "Finalize\n";
  fEveManager->GetWindowManager()->HideAllEveDecorations();

  // Load Initial data into view
  SwitchToEvent(0);
}

void TEventAnalyseGUI::SetupSignalSlots()
{
  std::cerr << "Setting up selection Signal\n";
  gsl::not_null<TEveSelection*> selection = fEveManager->GetSelection();
  selection->Connect("SelectionAdded(TEveElement*)", "Utility::TEventVisualization",
                     &fEventVisualization, "DispatchMouseSelection(TEveElement*)");

  std::cerr << "Setting up Event navigation events\n";
  fNavigation->SetupSignalSlots(this, &fVSDData);
  fNavigation->Connect("PreviousEvent()", "Utility::TEventAnalyseGUI", this,
                       "ChoosePreviousEvent()");
  fNavigation->Connect("NextEvent()", "Utility::TEventAnalyseGUI", this, "ChooseNextEvent()");

  // Signal Slots that handle the connection between loading data and the
  // analysis layer of the classes.
  // FIXME Refactor, This must be done outside the GUI, and injected.
  // Requires split up of VSDReader into multiple classes, then it is typebased.
  if (auto* RaaExercise = dynamic_cast<Raa::EventDisplay*>(fExercise.get())) {
    fVSDData.Connect("LoadedTrack(TEveTrack*)", "Raa::EventDisplay", RaaExercise,
                     "TrackLoaded(TEveTrack*)");
    fVSDData.Connect("FinishedTracks()", "Raa::EventDisplay", RaaExercise, "FinishedTracks()");

    // GUI Selection must be passed to the analysis.
    fEventVisualization.Connect("SelectedESDTrack(TEveTrack*)", "Raa::EventDisplay", RaaExercise,
                                "TrackSelected(TEveTrack*)");

    fEventVisualization.Connect("SelectedClustersITS()", "Utility::TEventAnalyseGUI", this,
                                "ShowDetectorDetails(=\"ITS\")");
    fEventVisualization.Connect("SelectedClustersTPC()", "Utility::TEventAnalyseGUI", this,
                                "ShowDetectorDetails(=\"TPC\")");
    fEventVisualization.Connect("SelectedClustersTRDTOF()", "Utility::TEventAnalyseGUI", this,
                                "ShowDetectorDetails(=\"TRD+TOF\")");
  } else if (auto* StrangeExercise = dynamic_cast<Strangeness::EventDisplay*>(fExercise.get())) {
    fVSDData.Connect("ClearingV0s()", "Strangeness::EventDisplay", StrangeExercise, "ClearV0s()");
    fVSDData.Connect("LoadedV0Negative(TEveTrack*)", "Strangeness::EventDisplay", StrangeExercise,
                     "V0NegativeLoaded(TEveTrack*)");
    fVSDData.Connect("LoadedV0Positive(TEveTrack*)", "Strangeness::EventDisplay", StrangeExercise,
                     "V0PositiveLoaded(TEveTrack*)");
    fVSDData.Connect("LoadedV0()", "Strangeness::EventDisplay", StrangeExercise, "V0Loaded()");
    // fVSDData.Connect("FinishedV0s()", "Strangeness::EventDisplay", StrangeExercise,
    // "FinishedV0s()");

    fVSDData.Connect("ClearingCascades()", "Strangeness::EventDisplay", StrangeExercise,
                     "ClearCascades()");
    fVSDData.Connect("LoadedCascadeNegative(TEveTrack*)", "Strangeness::EventDisplay",
                     StrangeExercise, "CascadeNegativeLoaded(TEveTrack*)");
    fVSDData.Connect("LoadedCascadePositive(TEveTrack*)", "Strangeness::EventDisplay",
                     StrangeExercise, "CascadePositiveLoaded(TEveTrack*)");
    fVSDData.Connect("LoadedCascadeBachelor(TEveTrack*)", "Strangeness::EventDisplay",
                     StrangeExercise, "CascadeBachelorLoaded(TEveTrack*)");
    fVSDData.Connect("LoadedCascade()", "Strangeness::EventDisplay", StrangeExercise,
                     "CascadeLoaded()");
    fVSDData.Connect("FinishedCascades()", "Strangeness::EventDisplay", StrangeExercise,
                     "FinishedCascades()");

    // GUI Selection must be passed to the analysis.
    fEventVisualization.Connect("SelectedV0Track(TEveTrack*)", "Strangeness::EventDisplay",
                                StrangeExercise, "TrackSelected(TEveTrack*)");
    fEventVisualization.Connect("SelectedV0PointingLine(TEveTrack*)", "Strangeness::EventDisplay",
                                StrangeExercise, "TrackSelected(TEveTrack*)");
    fEventVisualization.Connect("SelectedCascadeTrack(TEveTrack*)", "Strangeness::EventDisplay",
                                StrangeExercise, "TrackSelected(TEveTrack*)");
  } else {
    throw std::runtime_error("Expected a known Exercise for Data Analysis");
  }
}

void TEventAnalyseGUI::ChoosePreviousEvent()
{
  fCurrentEventIdx = (fCurrentEventIdx == 0 ? fTotalEventCount : fCurrentEventIdx) - 1;
  SwitchToEvent(fCurrentEventIdx);
}

void TEventAnalyseGUI::ChooseNextEvent()
{
  fCurrentEventIdx = (fCurrentEventIdx + 1) % fTotalEventCount;
  SwitchToEvent(fCurrentEventIdx);
}

void TEventAnalyseGUI::FinishCurrentEvent()
{
  fExercise->EventDone();
  fFinishedEvents++;
  fNavigation->SetEventsAnalysed(fFinishedEvents);
  ChooseNextEvent();
}

void TEventAnalyseGUI::Auto()
{
  // Copied from VSDReader, right now this is a dummy to get the code compiling.
  Int_t ret = 0;
  new TGMsgBox(gClient->GetRoot(), fEveBrowser, fTranslation.QuestionAutomaticAnalysis(),
               fTranslation.QuestionAutomaticAnalysisMsg(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  if (ret != kMBYes) {
    return;
  }

  // StatusGuard sg(fTranslation.GuardAutoMode(), 2);
  for (Int_t i = 0; i < fTotalEventCount; i++) {
    fExercise->AutoEvent();
    FinishCurrentEvent();
  }
}
void TEventAnalyseGUI::OpenInstructions()
{
  new Instructions(gClient->GetRoot(), 750, 500, fExercise->Instructions());
}

void TEventAnalyseGUI::ToggleRenderable(ERenderables ElementID)
{
  if (ElementID == ERenderables::kAll) {
    ToggleRenderable(ERenderables::kIntersectionPoint);
    ToggleRenderable(ERenderables::kAxes);
    ToggleRenderable(ERenderables::kClusters);
    ToggleRenderable(ERenderables::kTracks);
    /* Explicitly not Primaries, because that is more a configuration then
     * a renderable! */
    ToggleRenderable(ERenderables::kV0s);
    ToggleRenderable(ERenderables::kCascades);
    ToggleRenderable(ERenderables::kGeometry);
  } else {
    // Flip the bit in ActiveRenderables that is defined by ElementID.
    static_assert(std::is_same<typename std::underlying_type<ERenderables>::type, uint16_t>::value,
                  "Popcount will be executed on uint16_t");
    Expects(__builtin_popcount(static_cast<uint16_t>(ElementID)) == 1);
    fActiveRenderables ^= ElementID;

    ConfigureState();
    VisualizeState();
  }
}

void TEventAnalyseGUI::ShowDetectorDetails(const char* Detector)
{
  Expects(strncmp(Detector, "ITS", 4) == 0 || strncmp(Detector, "TPC", 4) == 0 ||
          strncmp(Detector, "TRD+TOF", 9) == 0);
  new Details(Detector, "ALICE", gClient->GetRoot(), 100, 100);
}

void TEventAnalyseGUI::DispatchTaskChoice(Int_t NTask)
{
  gsl::not_null<Raa::EventDisplay*> Exercise(dynamic_cast<Raa::EventDisplay*>(fExercise.get()));
  gsl::not_null<Raa::TNavigation*> Navi(dynamic_cast<Raa::TNavigation*>(fNavigation.get()));
  auto NewTask = gsl::narrow_cast<Raa::EActiveTask>(NTask);
  Raa::EActiveTask OldTask = Exercise->GetCurrentTask();

  if (!Exercise->TryChooseTask(NewTask))
    Navi->SetTask(OldTask);
}

void TEventAnalyseGUI::SwitchToEvent(Int_t EventIdx)
{
  if (EventIdx < 0 || EventIdx >= fTotalEventCount)
    throw std::runtime_error("Choosen Event Idx is out of bounds!");

  // This part must happen first, because `NewEvent` updates the magnetic
  // field which influences the visual output below.
  std::cerr << "Update Navigation view\n";
  TString type = fExercise->NewEvent(EventIdx);
  fNavigation->SetEventType(type);
  fNavigation->SetEventNumber(EventIdx + 1, fTotalEventCount);

  // TODO The Visualization should register on a signal after the data is updated
  // and do all the manual work itself internally.
  std::cerr << "Clear vis\n";
  fEventVisualization.HideAll();
  fEventVisualization.DestroyEventRPhi();
  fEventVisualization.DestroyEventRhoZ();

  std::cerr << "Load Event Data\n";
  fVSDData.LoadEvent(EventIdx);

  ConfigureState();
  VisualizeState();
}

Bool_t TEventAnalyseGUI::IsActive(ERenderables Element)
{
  return HasActivated(fActiveRenderables, Element);
}

void TEventAnalyseGUI::ConfigureState()
{
  if (IsActive(ERenderables::kIntersectionPoint))
    fEventVisualization.ShowIntersectionPoint();
  else
    fEventVisualization.HideIntersectionPoint();

  if (IsActive(ERenderables::kAxes))
    fEventVisualization.ShowAxes();
  else
    fEventVisualization.HideAxes();

  if (IsActive(ERenderables::kClusters))
    fEventVisualization.ShowClusters();
  else
    fEventVisualization.HideClusters();

  if (IsActive(ERenderables::kTracks)) {
    fVSDData.LoadTracks(fExercise->GetMagneticField(), IsActive(ERenderables::kPrimaries));
    fEventVisualization.ShowTracks();
  } else
    fEventVisualization.HideTracks();

  if (IsActive(ERenderables::kV0s)) {
    fVSDData.LoadV0s(fExercise->GetMagneticField());
    fEventVisualization.ShowV0s();
  } else
    fEventVisualization.HideV0s();

  if (IsActive(ERenderables::kCascades)) {
    fVSDData.LoadCascades(fExercise->GetMagneticField());
    fEventVisualization.ShowCascades();
  } else
    fEventVisualization.HideCascades();

  if (IsActive(ERenderables::kGeometry))
    fEventVisualization.ShowDetector();
  else
    fEventVisualization.HideDetector();
}

void TEventAnalyseGUI::VisualizeState()
{
  std::cerr << "Fix Geometry of Detector up\n";
  fExercise->FixGeometry(fEventVisualization.GetDetectorGeometry());

  std::cerr << "Redisplay vis\n";
  // Update the sideviews.
  fEventVisualization.ImportEventRPhi(fEveEventManager);
  fEventVisualization.ImportEventRhoZ(fEveEventManager);

  // Trigger changes
  fEveEventManager->ElementChanged(kTRUE, kTRUE);
}

} // namespace Utility
