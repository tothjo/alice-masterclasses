#ifndef ERENDERELEMENTS_H_UNUNYGYI
#define ERENDERELEMENTS_H_UNUNYGYI

#include <TString.h>
#include <gsl/gsl>
#include <stdexcept>

namespace Utility
{
/// This enum encodes all possible discrete elements that can be rendered.
/// It is usefull to avoid typos between creation and testing if an element
/// is of this kind.
enum class ERenderElements {
  kClustersITS,
  kClustersTPC,
  kClustersTRD,
  kClustersTOF,

  kESDTrack,
  kESDTracks,

  kV0TrackNegative,
  kV0TrackPositive,
  kV0PointingLine,
  kV0NegativeTracks,
  kV0PositiveTracks,
  kV0s,

  kCascadeTrackNegative,
  kCascadeTrackPositive,
  kCascadeTrackBachelor,
  kCascadeLine1,
  kCascadeLine2,
  kCascadeBachelorTracks,
  kCascadePositiveTracks,
  kCascadeNegativeTracks,
  kCascadeV0s,

  kAxisX,
  kAxisY,
  kAxisZ,

  kIP,
  kIPLine,
};

inline TString RenderElementToString(ERenderElements E)
{
  switch (E) {
    case ERenderElements::kClustersITS:
      return "ITS";
    case ERenderElements::kClustersTPC:
      return "TPC";
    case ERenderElements::kClustersTRD:
      return "TRD";
    case ERenderElements::kClustersTOF:
      return "TOF";

    case ERenderElements::kESDTrack:
      return "ESD_Track";
    case ERenderElements::kESDTracks:
      return "ESD Tracks";

    case ERenderElements::kV0TrackNegative:
      return "V0_Track_Neg";
    case ERenderElements::kV0TrackPositive:
      return "V0_Track_Pos";
    case ERenderElements::kV0PointingLine:
      return "V0_Pointing_Line";
    case ERenderElements::kV0NegativeTracks:
      return "V0 Negative tracks";
    case ERenderElements::kV0PositiveTracks:
      return "V0 Positive tracks";
    case ERenderElements::kV0s:
      return "V0s";

    case ERenderElements::kCascadeTrackNegative:
      return "Cascade_Track_Neg";
    case ERenderElements::kCascadeTrackPositive:
      return "Cascade_Track_Pos";
    case ERenderElements::kCascadeTrackBachelor:
      return "Cascade_Track_Bachelor_Track";
    case ERenderElements::kCascadeLine1:
      return "Cascade_Line_1";
    case ERenderElements::kCascadeLine2:
      return "Cascade_Line_2";
    case ERenderElements::kCascadeBachelorTracks:
      return "Cascade bachelor tracks";
    case ERenderElements::kCascadePositiveTracks:
      return "Positive cascade tracks";
    case ERenderElements::kCascadeNegativeTracks:
      return "Negative cascade tracks";
    case ERenderElements::kCascadeV0s:
      return "Cascade V0s";

    case ERenderElements::kAxisX:
      return "GuideX";
    case ERenderElements::kAxisY:
      return "GuideY";
    case ERenderElements::kAxisZ:
      return "GuideZ";

    case ERenderElements::kIP:
      return "IP";
    case ERenderElements::kIPLine:
      return "IP Line";
  }
  Ensures(false && "Unreachable point!");
  return "";
}

inline ERenderElements StringToRenderElement(const TString& S)
{
  if (S == "ITS")
    return ERenderElements::kClustersITS;
  if (S == "TPC")
    return ERenderElements::kClustersTPC;
  if (S == "TRD")
    return ERenderElements::kClustersTRD;
  if (S == "TOF")
    return ERenderElements::kClustersTOF;

  if (S == "ESD_Track")
    return ERenderElements::kESDTrack;
  if (S == "ESD Tracks")
    return ERenderElements::kESDTracks;

  if (S == "V0_Track_Neg")
    return ERenderElements::kV0TrackNegative;
  if (S == "V0_Track_Pos")
    return ERenderElements::kV0TrackPositive;
  if (S == "V0_Pointing_Line")
    return ERenderElements::kV0PointingLine;
  if (S == "V0 Negative tracks")
    return ERenderElements::kV0NegativeTracks;
  if (S == "V0 Positive tracks")
    return ERenderElements::kV0PositiveTracks;
  if (S == "V0s")
    return ERenderElements::kV0s;

  if (S == "Cascade_Track_Neg")
    return ERenderElements::kCascadeTrackNegative;
  if (S == "Cascade_Track_Pos")
    return ERenderElements::kCascadeTrackPositive;
  if (S == "Cascade_Track_Bachelor_Track")
    return ERenderElements::kCascadeTrackBachelor;
  if (S == "Cascade_Line_1")
    return ERenderElements::kCascadeLine1;
  if (S == "Cascade_Line_2")
    return ERenderElements::kCascadeLine2;
  if (S == "Cascade bachelor tracks")
    return ERenderElements::kCascadeBachelorTracks;
  if (S == "Positive cascade tracks")
    return ERenderElements::kCascadePositiveTracks;
  if (S == "Negative cascade tracks")
    return ERenderElements::kCascadeNegativeTracks;
  if (S == "Cascade V0s")
    return ERenderElements::kCascadeV0s;

  if (S == "GuideX")
    return ERenderElements::kAxisX;
  if (S == "GuideY")
    return ERenderElements::kAxisY;
  if (S == "GuideZ")
    return ERenderElements::kAxisZ;

  if (S == "IP")
    return ERenderElements::kIP;
  if (S == "IP Line")
    return ERenderElements::kIPLine;

  throw std::runtime_error("Unknown Tag '" + S + "' for a RenderElement!");
}

} // namespace Utility

#endif /* end of include guard: ERENDERELEMENTS_H_UNUNYGYI */
