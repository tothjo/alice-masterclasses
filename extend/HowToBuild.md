Build Instructions
==================

Development builds
------------------

It is assumed that you cloned the repository **recursive** already.
If not, then do

```bash
$ git submodule init
> Submodule Catch2
> Submodule gsl-lite ...
```

to obtain the submodules necessary to build the MasterClass.
You need `cmake`, a not too outdated `gcc` or `clang` and `make`.

```bash
$ pwd
> <BLAA>/alice-masterclass
$ mkdir build && cd build
$ # Optional, maybe system root would work too!
$ alienv enter <AliRoot-Version6>
$ cmake .. -DCMAKE_BUILD_TYPE=MinSizeRel -DROOTSYS=$ROOTSYS
> CMake must be happy
$ make -j4
> Build output
$ ls src/
> ... MasterClass.x ...
```

- The executable lives in `build/src/MasterClass.x`
- For a runnable version see the next section. Because some data needs to be
  loaded dynamically it is required to have an installation of the program
  that will setup a directory that can be used to run the MasterClass.

Package builds
--------------

To create package that will actually contain all required data and execute
nicely there is a helper script in `deployment/make_package.py`.

Note: This script downloads a known functioning version of `ROOT`.

Example usage:

```
$ deployment/make_package.py --force --keep-files --type archive \
                             --out-directory deploy_ubuntu
> Build log
> Install log
> Packaging log
```

This call will create a `deploy_ubuntu.tar.gz` package that can be uploaded.
It creates the directory `deploy_ubuntu` as well that contains the runnable
MasterClass. Execute `AppRun` to start the MasterClass.

To just build the code for development you can skip the packaging with the
`--skip-packing` option. You will end up only with the build dir.

AppImage
--------

[AppImage](https://appimage.org/) are a nice way to contain all dependencies
in a compressed image that can run on __every__ linux distribution.

This would be the prefered way of shipping the MasterClasses but does not fully
function right now. The problem is, that `ROOT6` has a lot of dependencies that
need to be packed into the image. This requires some more work.

A second issue is the structure of `Raa Part2` which tries to write files in
the execution directory. That is not possible in `AppImages` as they are read
only.

Limitations
-----------

Right now the `make_package.py` only does it's job properly for Ubuntu 18.04.
It is extendable to other distributions as well.
