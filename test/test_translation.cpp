#include "catch2/catch.hpp"

#include <TSystem.h>
#include "EntryPoint/GUITranslation.h"
#include "Jpsi/ClassContent.h"
#include "Jpsi/GUITranslation.h"
#include "Raa/ClassContent.h"
#include "Raa/ContentGUITranslation.h"
#include "Strangeness/ClassContent.h"
#include "Strangeness/GUITranslation.h"
#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace
{
// Every instantiation demonstrates, that there are not incorect keys used.
// It does not show, that all keys are registered!
template <typename LangFactory>
void TestAllGUILanguages()
{
  using Utility::ESupportedLanguages;
  REQUIRE_NOTHROW(LangFactory::create(ESupportedLanguages::English));
  REQUIRE_NOTHROW(LangFactory::create(ESupportedLanguages::German));
  REQUIRE_THROWS_AS(LangFactory::create(ESupportedLanguages::NLanguages), std::runtime_error);
}

// Test that all languages are properly created for the ClassContent.
template <typename LangFactory>
void TestAllContentLanguages(LangFactory FactoryFunction)
{
  using Utility::ESupportedLanguages;
  REQUIRE_NOTHROW(FactoryFunction(ESupportedLanguages::English));
  REQUIRE_NOTHROW(FactoryFunction(ESupportedLanguages::German));
  REQUIRE_THROWS_AS(FactoryFunction(ESupportedLanguages::NLanguages), std::runtime_error);
}
} // namespace

TEST_CASE("Translation Utilities GetLanguageName", "[translations]")
{
  using Utility::ESupportedLanguages;
  using Utility::GetLanguageName;

  WHEN("A correct language value is provided")
  {
    REQUIRE(GetLanguageName(ESupportedLanguages::English) == "English");
    REQUIRE(GetLanguageName(ESupportedLanguages::German) == "German");
  }

  WHEN("A inccorect language value is provided, like NLanguages")
  {
    REQUIRE_THROWS_AS(GetLanguageName(ESupportedLanguages::NLanguages), std::runtime_error);
  }
}

TEST_CASE("Translation Utilites GetLanguageNameShort", "[translations]")
{
  using Utility::ESupportedLanguages;
  using Utility::GetLanguageNameShort;

  WHEN("A correct language value is provided")
  {
    REQUIRE(GetLanguageNameShort(ESupportedLanguages::English) == "en");
    REQUIRE(GetLanguageNameShort(ESupportedLanguages::German) == "de");
  }

  WHEN("A inccorect language value is provided, like NLanguages")
  {
    REQUIRE_THROWS_AS(GetLanguageNameShort(ESupportedLanguages::NLanguages), std::runtime_error);
  }
}

TEST_CASE("Translation Utilites LanguageFromEnv", "[translations]")
{
  using Utility::ESupportedLanguages;
  using Utility::LanguageFromEnv;

  WHEN("No Language is set in the Environment")
  {
    REQUIRE(LanguageFromEnv() == ESupportedLanguages::English);
  }

  WHEN("A Language is set via the environment variable correctly")
  {
    gSystem->Setenv("ALICE_MASTERCLASS_LANG", "de");
    REQUIRE(LanguageFromEnv() == ESupportedLanguages::German);
    gSystem->Setenv("ALICE_MASTERCLASS_LANG", "");
  }

  WHEN("A language is set via the environment, but incorrectly")
  {
    gSystem->Setenv("ALICE_MASTERCLASS_LANG", "rubbish");
    REQUIRE(LanguageFromEnv() == ESupportedLanguages::English);
    gSystem->Setenv("ALICE_MASTERCLASS_LANG", "");
  }
}

TEST_CASE("Basic usage of LanguageProvider", "[translations]")
{
  using Utility::ESupportedLanguages;
  using Utility::TLanguageProvider;

  TLanguageProvider P(ESupportedLanguages::English);
  REQUIRE(P.GetLanguage() == ESupportedLanguages::English);
}

// Only check 2 translations, it is assumed that all other translations
// do work as well. This is to reduce duplication an ease of change of single
// translation strings.
TEST_CASE("Entrypoint Translations", "[translations]")
{
  using EntryPoint::TEntryPointLanguage;
  using EntryPoint::TGUIEnglish;
  using Utility::ESupportedLanguages;
  using Utility::Translation;

  SECTION("Translation Completeness") { TestAllGUILanguages<TEntryPointLanguage>(); }

  WHEN("Language is English")
  {
    auto L = TEntryPointLanguage::create(ESupportedLanguages::English);
    REQUIRE(L->GUIChooseClass() == "Select master class ...");
  }

  WHEN("Language is German (some translation)")
  {
    auto L = TEntryPointLanguage::create(ESupportedLanguages::German);
    REQUIRE(L->GUIChooseClass() == "Waehle Meisterklasse ...");
  }
}

TEST_CASE("Jpsi Classcontent Translations", "[translations]")
{
  using Jpsi::TClassContent;
  using Jpsi::TContentEnglish;
  using Utility::ESupportedLanguages;

  SECTION("Translation Completeness") { TestAllContentLanguages(Jpsi::LanguageFactory); }

  SECTION("Class Content")
  {
    WHEN("Language is English")
    {
      TClassContent C(ESupportedLanguages::English);
      REQUIRE(C.GetName() == "Jpsi Masterclass");
    }

    WHEN("Language is German")
    {
      TClassContent C(ESupportedLanguages::German);
      REQUIRE(C.GetName() == "J/psi Master Class");
    }
  }
}

TEST_CASE("Jpsi GUI Translations", "[translations]")
{
  using Jpsi::TGUIEnglish;
  using Jpsi::TJPsiLanguage;
  using Utility::ESupportedLanguages;
  using Utility::Translation;

  SECTION("Translation Completeness") { TestAllGUILanguages<TJPsiLanguage>(); }

  WHEN("Language is English")
  {
    auto L = TJPsiLanguage::create(ESupportedLanguages::English);
    REQUIRE(L->MultiView() == "Multi View");
  }

  WHEN("Language is German (some translation)")
  {
    auto L = TJPsiLanguage::create(ESupportedLanguages::German);
    REQUIRE(L->MultiView() == "Mehrfachansicht");
  }
}

TEST_CASE("Raa Classcontent Translation", "[translations]")
{
  using Raa::LanguageFactory;
  using Raa::TClassContent;
  using Raa::TContentEnglish;
  using Utility::ESupportedLanguages;

  SECTION("Translation Completeness") { TestAllContentLanguages(LanguageFactory); }

  SECTION("Class Content")
  {
    WHEN("Language is English")
    {
      TClassContent C(ESupportedLanguages::English);
      REQUIRE(C.GetName() == "Nuclear Modification Factor");
    }

    WHEN("Language is German")
    {
      TClassContent C(ESupportedLanguages::German);
      REQUIRE(C.GetName() == "Nuklearer Formveraenderungsfaktor");
    }
  }
}

TEST_CASE("Raa GUI Translation", "[translations]")
{
  using Raa::ContentLanguage;
  using Raa::TGUIEnglish;
  using Raa::TGUIGerman;
  using Utility::ESupportedLanguages;
  using Utility::Translation;

  SECTION("Translation Completeness") { TestAllGUILanguages<Raa::ContentLanguage>(); }

  WHEN("Language is English")
  {
    auto L = ContentLanguage::create(ESupportedLanguages::English);
    REQUIRE(L->All() == "All");
  }

  WHEN("Language is German (some translation)")
  {
    auto L = ContentLanguage::create(ESupportedLanguages::German);
    REQUIRE(L->All() == "Alle");
  }
}

TEST_CASE("Strangeness Classcontent Translation", "[translations]")
{
  using Strangeness::LanguageFactory;
  using Strangeness::TClassContent;
  using Utility::ESupportedLanguages;

  SECTION("Translation Completeness") { TestAllContentLanguages(LanguageFactory); }

  SECTION("Class Content")
  {
    WHEN("Language is English")
    {
      TClassContent C(ESupportedLanguages::English);
      REQUIRE(C.GetName() == "Strange Particle Production");
    }

    WHEN("Language is German")
    {
      TClassContent C(ESupportedLanguages::German);
      REQUIRE(C.GetName() == "Strangepartikel Produktion");
    }
  }
}

TEST_CASE("Strangeness GUI Translation", "[translations]")
{
  using Strangeness::ClassLanguage;
  using Strangeness::TGUIEnglish;
  using Utility::ESupportedLanguages;
  using Utility::Translation;

  SECTION("Translation Completeness") { TestAllGUILanguages<ClassLanguage>(); }

  WHEN("Language is English")
  {
    auto L = ClassLanguage::create(ESupportedLanguages::English);
    REQUIRE(L->All() == "all");
  }

  WHEN("Language is German (some translation)")
  {
    gSystem->Setenv("ALICE_MASTERCLASS_LANG", "de");
    auto L = ClassLanguage::create(ESupportedLanguages::German);
    REQUIRE(L->All() == "alle");
  }
}

TEST_CASE("EventDisplay GUI Translation", "[translations]")
{
  using Utility::ESupportedLanguages;
  using Utility::TGUIEnglish;
  using Utility::Translation;
  using Utility::TUtilityLanguage;

  SECTION("Translation Completeness") { TestAllGUILanguages<TUtilityLanguage>(); }

  WHEN("Language is English")
  {
    auto L = TUtilityLanguage::create(ESupportedLanguages::English);
    REQUIRE(L->AutoAnalyseTitle() == "Auto analyse");
  }

  WHEN("Language is German (some translation)")
  {
    auto L = TUtilityLanguage::create(ESupportedLanguages::German);
    REQUIRE(L->AutoAnalyseTitle() == "Automatisch analysieren");
  }
}
