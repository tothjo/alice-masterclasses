#include "catch2/catch.hpp"

#include "Utility/ERenderElements.h"

TEST_CASE("RenderElements Enum to String", "")
{
  using Utility::ERenderElements;
  using Utility::RenderElementToString;

  REQUIRE(RenderElementToString(ERenderElements::kClustersITS) == "ITS");
  REQUIRE(RenderElementToString(ERenderElements::kClustersTPC) == "TPC");
  REQUIRE(RenderElementToString(ERenderElements::kClustersTRD) == "TRD");
  REQUIRE(RenderElementToString(ERenderElements::kClustersTOF) == "TOF");

  REQUIRE(RenderElementToString(ERenderElements::kESDTrack) == "ESD_Track");
  REQUIRE(RenderElementToString(ERenderElements::kESDTracks) == "ESD Tracks");

  REQUIRE(RenderElementToString(ERenderElements::kV0TrackNegative) == "V0_Track_Neg");
  REQUIRE(RenderElementToString(ERenderElements::kV0TrackPositive) == "V0_Track_Pos");
  REQUIRE(RenderElementToString(ERenderElements::kV0PointingLine) == "V0_Pointing_Line");
  REQUIRE(RenderElementToString(ERenderElements::kV0NegativeTracks) == "V0 Negative tracks");
  REQUIRE(RenderElementToString(ERenderElements::kV0PositiveTracks) == "V0 Positive tracks");
  REQUIRE(RenderElementToString(ERenderElements::kV0s) == "V0s");

  REQUIRE(RenderElementToString(ERenderElements::kCascadeTrackNegative) == "Cascade_Track_Neg");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeTrackPositive) == "Cascade_Track_Pos");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeTrackBachelor) ==
          "Cascade_Track_Bachelor_Track");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeLine1) == "Cascade_Line_1");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeLine2) == "Cascade_Line_2");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeBachelorTracks) ==
          "Cascade bachelor tracks");
  REQUIRE(RenderElementToString(ERenderElements::kCascadePositiveTracks) ==
          "Positive cascade tracks");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeNegativeTracks) ==
          "Negative cascade tracks");
  REQUIRE(RenderElementToString(ERenderElements::kCascadeV0s) == "Cascade V0s");

  REQUIRE(RenderElementToString(ERenderElements::kAxisX) == "GuideX");
  REQUIRE(RenderElementToString(ERenderElements::kAxisY) == "GuideY");
  REQUIRE(RenderElementToString(ERenderElements::kAxisZ) == "GuideZ");

  REQUIRE(RenderElementToString(ERenderElements::kIP) == "IP");
  REQUIRE(RenderElementToString(ERenderElements::kIPLine) == "IP Line");
}

TEST_CASE("RenderElements String to Enum", "")
{
  using Utility::ERenderElements;
  using Utility::StringToRenderElement;

  WHEN("Input is incorrect")
  {
    REQUIRE_THROWS_AS(StringToRenderElement("Rubbish"), std::runtime_error);
  }

  WHEN("Input makes sense")
  {
    REQUIRE(StringToRenderElement("ITS") == ERenderElements::kClustersITS);
    REQUIRE(StringToRenderElement("TPC") == ERenderElements::kClustersTPC);
    REQUIRE(StringToRenderElement("TRD") == ERenderElements::kClustersTRD);
    REQUIRE(StringToRenderElement("TOF") == ERenderElements::kClustersTOF);

    REQUIRE(StringToRenderElement("ESD_Track") == ERenderElements::kESDTrack);
    REQUIRE(StringToRenderElement("ESD Tracks") == ERenderElements::kESDTracks);

    REQUIRE(StringToRenderElement("V0_Track_Neg") == ERenderElements::kV0TrackNegative);
    REQUIRE(StringToRenderElement("V0_Track_Pos") == ERenderElements::kV0TrackPositive);
    REQUIRE(StringToRenderElement("V0_Pointing_Line") == ERenderElements::kV0PointingLine);
    REQUIRE(StringToRenderElement("V0 Negative tracks") == ERenderElements::kV0NegativeTracks);
    REQUIRE(StringToRenderElement("V0 Positive tracks") == ERenderElements::kV0PositiveTracks);
    REQUIRE(StringToRenderElement("V0s") == ERenderElements::kV0s);

    REQUIRE(StringToRenderElement("Cascade_Track_Neg") == ERenderElements::kCascadeTrackNegative);
    REQUIRE(StringToRenderElement("Cascade_Track_Pos") == ERenderElements::kCascadeTrackPositive);
    REQUIRE(StringToRenderElement("Cascade_Track_Bachelor_Track") ==
            ERenderElements::kCascadeTrackBachelor);
    REQUIRE(StringToRenderElement("Cascade_Line_1") == ERenderElements::kCascadeLine1);
    REQUIRE(StringToRenderElement("Cascade_Line_2") == ERenderElements::kCascadeLine2);
    REQUIRE(StringToRenderElement("Cascade bachelor tracks") ==
            ERenderElements::kCascadeBachelorTracks);
    REQUIRE(StringToRenderElement("Positive cascade tracks") ==
            ERenderElements::kCascadePositiveTracks);
    REQUIRE(StringToRenderElement("Negative cascade tracks") ==
            ERenderElements::kCascadeNegativeTracks);
    REQUIRE(StringToRenderElement("Cascade V0s") == ERenderElements::kCascadeV0s);

    REQUIRE(StringToRenderElement("GuideX") == ERenderElements::kAxisX);
    REQUIRE(StringToRenderElement("GuideY") == ERenderElements::kAxisY);
    REQUIRE(StringToRenderElement("GuideZ") == ERenderElements::kAxisZ);

    REQUIRE(StringToRenderElement("IP") == ERenderElements::kIP);
    REQUIRE(StringToRenderElement("IP Line") == ERenderElements::kIPLine);
  }
}
