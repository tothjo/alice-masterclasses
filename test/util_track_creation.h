#ifndef UTIL_TRACK_CREATION_H_KQRCVLVZ
#define UTIL_TRACK_CREATION_H_KQRCVLVZ

#include <TEveTrack.h>
#include <gsl/gsl>

#include "Raa/part1/TrackClassification.h"
#include "Utility/HelperPrimaryTracks.h"

namespace internal
{
inline TEveTrack CreatePrimary()
{
  TEveTrack t_primary;
  Utility::MarkPrimary(&t_primary);

  Ensures(Raa::IsPrimary(&t_primary));
  return t_primary;
}
inline TEveTrack CreateSecondary()
{
  TEveTrack t;
  Ensures(Raa::IsSecondary(&t));
  return t;
}
inline TEveTrack CreatePrimaryCharged()
{
  TEveTrack t_primary = CreatePrimary();
  t_primary.SetCharge(1);
  Ensures(Raa::IsPrimaryPositive(&t_primary));
  return t_primary;
}

inline TEveTrack CreatePrimaryHighPt(Double_t Pt = 1.)
{
  TEveRecTrackD underlying_track;
  const TEveVectorD Exactly1GeV(Pt, 0., 0.);
  underlying_track.fP = 2. * Exactly1GeV;

  TEveTrack t_primary(&underlying_track);
  Utility::MarkPrimary(&t_primary);

  Ensures(Raa::IsPrimary(&t_primary));

  return t_primary;
}
} // namespace internal

#endif /* end of include guard: UTIL_TRACK_CREATION_H_KQRCVLVZ */
