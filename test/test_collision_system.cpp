#include "catch2/catch.hpp"

#include "Raa/part1/ECollisionSystem.h"

TEST_CASE("Collision System Classification Proton Proton", "")
{
  using Raa::ECollisionSystem;
  using Raa::IsProtonProton;
  REQUIRE(IsProtonProton(ECollisionSystem::kpp2_76TeV));
  REQUIRE(IsProtonProton(ECollisionSystem::kpp7TeV));

  REQUIRE_FALSE(IsProtonProton(ECollisionSystem::kPbPbCentral));
  REQUIRE_FALSE(IsProtonProton(ECollisionSystem::kPbPbSemiCentral));
  REQUIRE_FALSE(IsProtonProton(ECollisionSystem::kPbPbPeripheral));
}
TEST_CASE("Collision System Classification Lead Lead", "")
{
  using Raa::ECollisionSystem;
  using Raa::IsLeadLead;
  REQUIRE_FALSE(IsLeadLead(ECollisionSystem::kpp2_76TeV));
  REQUIRE_FALSE(IsLeadLead(ECollisionSystem::kpp7TeV));

  REQUIRE(IsLeadLead(ECollisionSystem::kPbPbCentral));
  REQUIRE(IsLeadLead(ECollisionSystem::kPbPbSemiCentral));
  REQUIRE(IsLeadLead(ECollisionSystem::kPbPbPeripheral));
}
