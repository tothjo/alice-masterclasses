# Copied from https://github.com/mfasDa/ROOT6tools

############################################################################
# Various helpers making life easier when working with ROOT6               #
# Copyright (C) 2015  Markus Fasel, Lawrence Berkeley National Laboratory  #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
############################################################################

project(MasterClasses CXX)
cmake_minimum_required(VERSION 3.0)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

find_package(ROOT 6.10.00 REQUIRED)

include(CheckCXXCompilerFlag)

option(USE_ASAN "USE_ASAN" OFF)
if (USE_ASAN)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address")
endif (USE_ASAN)

option(USE_TSAN "USE_TSAN" OFF)
if (USE_TSAN)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=thread")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=thread")
endif (USE_TSAN)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIE")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")

CHECK_CXX_COMPILER_FLAG("--std=c++11" COMPILER_SUPPORTS_CXX11)
if(COMPILER_SUPPORTS_CXX11)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++11")
else()
  message(FATAL_ERROR "C++11 support required.")
endif()

option(TESTING "ACTIVATE UNITTESTING" ON)
if (TESTING)
  enable_testing()
  add_subdirectory(lib/Catch2)
  set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/lib/Catch2/contrib" ${CMAKE_MODULE_PATH})

  add_subdirectory(test)
endif (TESTING)

# ROOT dictionaries and maps
include(ROOTHelper)

# Treat ROOT as a System library
include_directories(SYSTEM ${ROOT_INCLUDE_DIR})
link_directories(SYSTEM ${ROOT_LIBDIR})

include_directories("${CMAKE_SOURCE_DIR}/src")
include_directories("${CMAKE_SOURCE_DIR}/translation")

# Add GSL for general programming support
set(GSL_INCLUDE_DIR lib/gsl-lite/include)
add_library(gsl INTERFACE)
include_directories(gsl INTERFACE ${GSL_INCLUDE_DIR})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Dgsl_CONFIG_CONTRACT_VIOLATION_THROWS")

add_subdirectory(src)

install(DIRECTORY data doc macros vsdData DESTINATION share)
install(FILES AppRun AliceMasterClass.desktop icon.png DESTINATION .)
