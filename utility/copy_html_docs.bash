#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <lang_shortcut>"
  echo "Example: $0 de"
  echo ""
  echo "Run this command from base directory of the repository!"
  exit 1
fi

lang="$1"

# Check if the script is executed from the correct directory. Existence of
# `.git` directory is used as heuristic.
if [ ! -d .git ]; then
  echo "You did not executed the script from the repository base-directory!"
  echo
  echo "Usage: $0 <lang_shortcut>"
  echo "Example: $0 de"
  exit 1
fi

echo "Copying Base Docs"
cp translation/EntryPoint/Description.{en,$lang}.html

echo "Copying Raa Docs"
cp translation/Raa/Counter.{en,$lang}.html
cp translation/Raa/Dataset.{en,$lang}.html
cp translation/Raa/Description.{en,$lang}.html
cp translation/Raa/EventDisplay.{en,$lang}.html
cp translation/Raa/LargeStat.{en,$lang}.html
cp translation/Raa/ReadValues.{en,$lang}.html
cp translation/Raa/Tasks.{en,$lang}.html

echo "Copying Strangeness Docs"
cp translation/Strangeness/Calculator.{en,$lang}.html
cp translation/Strangeness/Description.{en,$lang}.html
cp translation/Strangeness/EventDisplay.{en,$lang}.html
cp translation/Strangeness/Fitting.{en,$lang}.html
