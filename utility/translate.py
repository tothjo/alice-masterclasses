#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Build a small script that can request translation from google translate.
Workhorse is `request_translation.js`, the rest is just cleaning.
"""
import argparse
import subprocess
import re
import sys


def clean_trans(text: str):
    """Clean the translation output. phantomjs throws some TypeErrors:"""
    first_err = text.find("TypeError")

    if first_err == -1:
        return text
    else:
        return text[:first_err].strip()


def retrieve_trans(text: str, in_lang: str, out_lang: str):
    """Call helper script to retrieve the translation of `text`."""

    response = ""

    # Try at most 5 times to request the translation. It might not work always.
    for trial in range(5):
        print("Requesting next Translation")
        out = subprocess.check_output(
            ["phantomjs", "utility/request_translation.js", in_lang, out_lang, text])

        cleaned = clean_trans(out.decode("utf-8"))
        if len(cleaned) > 0:
            return cleaned
    raise RuntimeError("Could not retrieve valid translation from Google Translate")


def trans_file(file_name: str, in_lang: str, out_lang: str):
    """Translate the 'RegisterText' calls stored in `file_name`
    and output the contents in `file_name`.`out_lang`
    """
    translated = []
    with open(file_name, 'r') as f:
        # Match 2 groups: Key, Value
        # Key = Name of the Element to translate
        # Value = actual text snippet to translate
        pattern = re.compile(r'^\s*RegisterText\("(.*)", "(.*)"\)')
        for line in f:
            matched = pattern.match(line)

            # print(matched.groups())

            # e.g. the 'ReadHtml("..")' ones do not match!
            if not matched:
                translated.append("// FIXME: {}\n".format(line.strip()))
                continue

            text_key = matched.group(1)
            orig_text = matched.group(2)
            trans = retrieve_trans(orig_text, in_lang, out_lang)

            translated.append(
                "// CHECK: RegisterText(\"{}\", \"{}\");\n".format(
                    text_key, trans))

    return translated


def ascii_transformer(text):
    """Replace all umlaute with there equivalent."""
    # German Umlaute
    text = text.replace("ä", "ae")
    text = text.replace("Ä", "Ae")
    text = text.replace("ö", "oe")
    text = text.replace("Ö", "Oe")
    text = text.replace("ü", "ue")
    text = text.replace("Ü", "Ue")
    text = text.replace("ß", "ss")

    # Danish special chars
    text = text.replace("å", "a")
    text = text.replace("Å", "A")
    text = text.replace("æ", "ae")
    text = text.replace("Æ", "Ae")
    text = text.replace("é", "e")
    text = text.replace("É", "E")
    text = text.replace("ø", "o")
    text = text.replace("Ø", "O")

    # French special characters
    text = text.replace("ù", "u")
    text = text.replace("Ù", "U")
    text = text.replace("û", "u")
    text = text.replace("Û", "U")
    text = text.replace("ü", "ue")
    text = text.replace("Ü", "ue")
    text = text.replace("ÿ", "y")
    text = text.replace("Ÿ", "Y")
    text = text.replace("’", "'")
    text = text.replace("“", "\"")
    text = text.replace("”", "\"")
    text = text.replace("«", "<")
    text = text.replace("»", ">")
    text = text.replace("à", "a")
    text = text.replace("À", "A")
    text = text.replace("â", "a")
    text = text.replace("Â", "A")
    text = text.replace("æ", "ae")
    text = text.replace("Æ", "Ae")
    text = text.replace("ç", "c")
    text = text.replace("Ç", "C")
    text = text.replace("é", "e")
    text = text.replace("É", "E")
    text = text.replace("è", "e")
    text = text.replace("È", "E")
    text = text.replace("ê", "e")
    text = text.replace("Ê", "E")
    text = text.replace("ë", "e")
    text = text.replace("Ë", "E")
    text = text.replace("ï", "i")
    text = text.replace("Ï", "I")
    text = text.replace("î", "i")
    text = text.replace("Î", "I")
    text = text.replace("ô", "o")
    text = text.replace("Ô", "O")
    text = text.replace("œ", "oe")
    text = text.replace("Œ", "Oe")

    return text


def encode_as_ascii(snippets: list):
    """Encode the whole text so it can be used in the ROOT GUI classes.
    That means only ascii is allowed."""
    return map(ascii_transformer, snippets)


def main():
    """
    Request the translation for text snippets and return them.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input-file",
        help="File that contains 'RegisterText' calls (only!)",
        type=str)
    parser.add_argument(
        "-s",
        "--source-lang",
        help="Shortcut for source language (e.g. 'en' for English",
        type=str,
        default="en")
    parser.add_argument(
        "-t",
        "--target-lang",
        help="Shortcut for target language (e.g. 'de' for German)")
    args = parser.parse_args()

    print("Working on {}".format(args.input_file))
    assert args.input_file.endswith(args.source_lang)

    # Do the translation
    print("Translate them")
    translation = trans_file(args.input_file, args.source_lang,
                             args.target_lang)
    print("Encode them")
    ascii_coded = encode_as_ascii(translation)

    print("Write them")
    basename_file = args.input_file[:-(len(args.source_lang) + 1)]
    with open("{}.{}".format(basename_file, args.target_lang), 'w') as out:
        out.writelines(ascii_coded)


if __name__ == "__main__":
    main()
