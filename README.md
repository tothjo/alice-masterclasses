# ALICE Master Classes

## Notes for Developers

Please take a look in the `extend` directory as it contains documents
about the code internals. Feel free the browse the source code as well.

## User-space

This package contains the three available Master Classes from ALICE.

- __Nuclear Modification Factor__

In this master class the students will measure the nuclear
modification factor, which expresses the change in particle
production in heavy-ion collisions as compared to proton-proton
collisions.

- __Strangeness Enhancement__

In this master class the students will measure the strangeness
enhancement in heavy-ion collisions as compare to proton-proton
collisions.

- __J/Psi__

This master class is developed by GSI and integrated in the common framework.

## Longer descriptions of the master classes

Description of the individual classes which include explanation of the physics
as well.

- `doc/Raa/Documentation2013.pdf` Description (including physics motivation)
  of the Nuclear Modification master class
- `doc/Strangeness/Documentation.pdf` Description (including physics motivation)
  of the Strangeness Enhancement master class
- `doc/Jpsi/MasterClassManual.pdf` Description (including physics motivation)
  of the J/Psi master class, currently only in German.

## Installation

### Packages

The MasterClasses are shipped with packages which run at the moment best on
Ubuntu 18.04. Get a VM with it and download the `.tar.gz` package.
Unpack that and start the `AppRun` script.

### Build from Sources

The project is managed at the
[CERN GitLab instance](https://gitlab.cern.ch/tothjo/alice-masterclasses)
build instructions are in `extend/HowToBuild.md`.

## Usage

### Class selection

This will bring up a GUI where you can select your Master Class and
the specific exercise.

![Start GUI](doc/Utility/ClassSelector.png)

*Note*: The above image does not show recent additions to this interface.

You can also choose the language used in the descriptions through
out the master classes. Note also, that some descriptions may not be available
in the chosen language, and your favorite language may not be an option.
In that case, please contribute to this project by adding a translation.
See `extend/AddLanguage.md` for instructions on adding a new language.

## Distribution

Creating the distributions can be done via `GitLab CI` pipelines.
There is a last optional stage that will create the distributions. This
stage must be triggered manually.
